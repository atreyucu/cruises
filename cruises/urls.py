"""cruises URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from backend.models import CruiseLine, TravelDates
from frontend import urls as frontend_urls
from django.contrib.sitemaps.views import sitemap
from django.conf import settings
from frontend.sitemaps import DeparturePortSitemap, DestinationPortSitemap, DestinationSubregionSitemap, \
    CruiseLineSitemap, StaticViewSitemap, CruiseLineShipSitemap, CruiseLineDestinationSitemap, \
    CruiseLineDepartureSitemap, CustomViewSitemap


def getCruisesLines():
    cruiseslines = CruiseLine.objects.all()
    items = []
    for cl in cruiseslines:
        if TravelDates.objects.get_by_cruise_line_slug(cl.slug).count() != 0:
            items.append(cl)
    return items

def getCruiselineDestination():
    items = getCruisesLines()
    array = {}
    for cruiseline in items:
        key = 'cl-destination{cl}'.format(cl=cruiseline.id)
        array[key] = CruiseLineDestinationSitemap(cruiseline)
    return array

def getCruiselineDeparture():
    items = getCruisesLines()
    array = {}
    for cruiseline in items:
        key = 'cl-departure{cl}'.format(cl=cruiseline.id)
        array[key] = CruiseLineDepartureSitemap(cruiseline)
    return array

def getCruiselineShip():
    items = getCruisesLines()
    array = {}
    for cruiseline in items:
        key = 'cl-ship{cl}'.format(cl=cruiseline.id)
        array[key] = CruiseLineShipSitemap(cruiseline)
    return array

sitemaps = {
    'static': StaticViewSitemap,
    'custom': CustomViewSitemap,
    'departure_port': DeparturePortSitemap,
    'destination_port': DestinationPortSitemap,
    'subregion': DestinationSubregionSitemap,
    'cruiseline': CruiseLineSitemap,
}
# tmp = sitemaps.copy()
# tmp.update(getCruiselineDestination())
# sitemaps = tmp.copy()
# sitemaps.update(getCruiselineDeparture())
sitemaps.update(getCruiselineDeparture())
sitemaps.update(getCruiselineDestination())
sitemaps.update(getCruiselineShip())

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include(frontend_urls)),
    url(r'media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    url(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps,'SSL':True}, name='django.contrib.sitemaps.views.sitemap')
    #url(r'^robots.txt$', lambda r: HttpResponse("User-agent: *\nDisallow: /", content_type="text/plain"))
] #+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


