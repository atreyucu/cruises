import os
import sys
import site

# Add the site-packages of the chosen virtualenv to work with
site.addsitedir('/home/ubuntu/.virtualenvs/cruises/local/lib/python2.7/site-packages')

# Add the app's directory to the PYTHONPATH
sys.path.append('/home/ubuntu/sites/cruises/')
#sys.path.append('/home/ridosbey/cruises/cruises/')

#os.environ.setdefault("DJANGO_SETTINGS_MODULE", "probar_env_apache.settings"

os.environ['DJANGO_SETTINGS_MODULE'] = 'cruises.settings'

# Activate your virtual env
activate_env=os.path.expanduser("/home/ubuntu/.virtualenvs/cruises/bin/activate_this.py")
execfile(activate_env, dict(__file__=activate_env))

# This application object is used by any WSGI server configured to use this
# file. This includes Django's development server, if the WSGI_APPLICATION
# setting points here.
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

# Apply WSGI middleware here.
# from helloworld.wsgi import HelloWorldApplication
# application = HelloWorldApplication(application)
