/**
 * Created by ernesto on 8/26/15.
 */
$body = $("body");

$(document).on({
    ajaxStart: function() { $body.addClass("loading");  },
    ajaxStop: function() { $body.removeClass("loading"); }
});

$(".view-prices").on("click", function (e) {
    e.preventDefault();

    obj = $(this);

    if(obj.parents(".sail-offer").find(".prices-form-wrapper").is(':visible'))
    {
        obj.parents(".sail-offer").removeClass('dropup');
        obj.html('View All Sailings & Pricing '+'<span class="caret"></span>');
        obj.parents(".sail-offer").find(".prices-form-wrapper").stop(true, true).slideToggle();
    }
    else{
        $.get('/all-pricing/', {
                    't_unique_id':$(this).attr('unique_id')
                            },
            function(data){
                obj.parents(".sail-offer").find(".prices-form-wrapper").html(data);
                obj.parents(".sail-offer").find(".prices-form-wrapper").stop(true, true).slideToggle();
            }
        );
        obj.parents(".sail-offer").addClass('dropup');
        obj.html('Hide Pricing '+'<span class="caret"></span>');
    }

});

$("#cruise-line-filter").change(function(e){

    var obj = $('#'+e.currentTarget.id+' option:selected');
    $.get('/ship-by-cruise/',
            {
                cruise_line_id: $('#'+e.currentTarget.id+' option:selected').attr('cruiseid')
            },
            function(data)
            {
                $('#ship-filter').html('<option value="0">');
                for(key in data)
                {
                    var option = $('<option>')
                    option.attr('value', data[key].name);
                    option.html(data[key].name);
                    $('#ship-filter').append(option);
                }
                $('#ship-filter').trigger("chosen:updated");
            },
        'json'
    )
});

$(".filter-form").submit(function(e) {
        ($(this).find('select').each(function(){
            var option_select = $(this).children('option:selected').attr('value');
            if(option_select == '0')
            {
                $(this).attr('disabled', 'disabled')
            }

        }));
    });

$(".menu_contact").click(function(){
    var height = $(document).height();
    height = height + 'px';
    $("html, body").animate({scrollTop: height}, speed = 0);
});

$(".follow_us").click(function(){
    var height = $(document).height()-750;
    height = height + 'px';
    console.log(height)
    $("html, body").animate({scrollTop: height}, speed = 0);
    //$(location).attr('href',  $(location).attr('href')+'#footer-socials');
});

$(".get-deals-button").click(function () {
    var input = $(".get-deals-button").parents('form').find(".input-mail-address")
    var email = input.val();
    var floatBtn = $(".get-deals-button").parents('.floating-btn')
    if (email_validation(email)) {
        $.post('/add_subscriber/', {email: email, 'csrfmiddlewaretoken':$('input[name=csrfmiddlewaretoken]').val()},
            function (data) {
                if (data.success == 1) {
                    floatBtn.removeClass("open");
                    input.val('');
                    $('#menu_error_label').hide()
                } else
                    $('#menu_error_label').show()
            }, 'json'
        )
    }

});

$("#header-deal-submit").click(function () {
    var input = $("#header-deal-submit").parents('form').find(".input-mail-address")
    var email = input.val();
    if (email_validation(email)) {
        $.post('/add_subscriber/', {email: email, 'csrfmiddlewaretoken':$('input[name=csrfmiddlewaretoken]').val()},
            function (data) {
                if (data.success == 1) {
                    $("#header-get").slideToggle();
                    input.val('');
                    $('#menu_error_label').hide()
                } else
                    $('#menu_error_label').show()
            }, 'json'
        )
    }

});



function email_validation(email_value)
{
    var filter = /[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
    if(filter.test(email_value))
    {
        return true;
    }
    else
    {
        return false;
    }
}

$(document).ready(function(){

    if (window.matchMedia('(max-width: 960px)').matches) {
        $('.search-button').html('search')
    } else {
        $('.search-button').html('search your cruise')
    }

    loc = $(location).attr('href');
    if (window.matchMedia('(max-width: 540px)').matches && loc.search('/search/') > 0) {
        $('.blue-section').css('margin-top', '100px');
    }

    $("#footer-deal-submit").click(function(){
        var input = $("#footer-deal-submit").parents('form').find(".input-mail-address")
        var email = input.val();
        if(email_validation(email))
        {
            $.post('/add_subscriber/',
                            {
                                email: email,
                                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
                            },
                function(data)
                {
                    if(data.success == 1)
                    {
                        $("#footer-get").fadeToggle();
                        input.val('');
                        $("#menu_error_label_bottom").hide();
                    }
                    else{
                        $("#menu_error_label_bottom").show();
                    }

                }
            )
        }

    });

    $("#deal-submit").click(function () {
        var email = $("#get-deals").val();
        //console.log(email);
        if (email_validation(email)) {
            $.post('/add_subscriber/', {
                    email: email,
                    'csrfmiddlewaretoken': $('input[name=csrfmiddlewaretoken]').val()
                },
                function (data) {
                    if (data.success == 1) {
                        $("#get-deals").val('');
                        $('label[for=get-deals]').html('Get weekly cruise deals')
                    } else {
                        $('label[for=get-deals]').html('This email already exist!')
                    }
                }
            )
        }

    })

    $("#accordion").on('show.bs.collapse', function() {
        $("#accordion").find('.collapse.in').collapse('hide');
    });

})
