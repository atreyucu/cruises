# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0008_auto_20150729_1744'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='additionalsailingdates',
            name='travel',
        ),
        migrations.AlterField(
            model_name='traveldates',
            name='travel',
            field=models.ForeignKey(related_name='travel_dates', to_field=b'unique_id', blank=True, to='backend.Travel', null=True),
        ),
        migrations.DeleteModel(
            name='AdditionalSailingDates',
        ),
    ]
