# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0013_port_is_depart'),
    ]

    operations = [
        migrations.AddField(
            model_name='filterdate',
            name='year_month_date',
            field=models.CharField(max_length=7, null=True),
        ),
    ]
