# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0002_auto_20150716_1231'),
    ]

    operations = [
        migrations.AlterField(
            model_name='travel',
            name='unique_id',
            field=models.CharField(max_length=255, unique=True, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='traveldates',
            name='travel',
            field=models.ForeignKey(to_field=b'unique_id', blank=True, to='backend.Travel', null=True),
        ),
    ]
