# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0017_auto_20150901_1415'),
    ]

    operations = [
        migrations.AlterField(
            model_name='filterdate',
            name='year_month_date',
            field=models.DateField(null=True),
        ),
    ]
