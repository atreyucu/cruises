# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0029_auto_20150914_1806'),
    ]

    operations = [
        migrations.AddField(
            model_name='traveldates',
            name='destination_port',
            field=models.ForeignKey(related_name='destination_port', on_delete=django.db.models.deletion.SET_NULL, to='backend.Port', null=True),
        ),
        migrations.AlterField(
            model_name='port',
            name='country',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, to='backend.Country', null=True),
        ),
    ]
