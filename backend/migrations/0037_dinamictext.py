# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('backend', '0036_auto_20151012_0308'),
    ]

    operations = [
        migrations.CreateModel(
            name='DinamicText',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('object_id', models.PositiveIntegerField()),
                ('first_text', models.TextField()),
                ('second_text', models.TextField()),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
                ('cruise_line1', models.ForeignKey(related_name='+', to='backend.CruiseLine')),
                ('cruise_line2', models.ForeignKey(related_name='+', to='backend.CruiseLine')),
                ('cruise_line3', models.ForeignKey(related_name='+', to='backend.CruiseLine')),
                ('cruise_line4', models.ForeignKey(related_name='+', to='backend.CruiseLine')),
                ('cruise_line5', models.ForeignKey(related_name='+', to='backend.CruiseLine')),
            ],
        ),
    ]
