# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0034_subregion_is_active_destination'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='subregion',
            options={'ordering': ['name'], 'verbose_name': 'SubRegion', 'verbose_name_plural': 'SubRegions'},
        ),
        migrations.AlterModelOptions(
            name='subscriptions',
            options={'verbose_name': 'Subscriptions', 'verbose_name_plural': 'Subscriptions'},
        ),
        migrations.AddField(
            model_name='subscriptions',
            name='exported',
            field=models.BooleanField(default=1, verbose_name=b'Exported'),
        ),
        migrations.AlterField(
            model_name='subscriptions',
            name='email',
            field=models.EmailField(unique=True, max_length=255),
        ),
    ]
