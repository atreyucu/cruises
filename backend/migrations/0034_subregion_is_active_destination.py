# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0033_auto_20151001_1455'),
    ]

    operations = [
        migrations.AddField(
            model_name='subregion',
            name='is_active_destination',
            field=models.BooleanField(default=1, verbose_name=b'Active for destination'),
        ),
    ]
