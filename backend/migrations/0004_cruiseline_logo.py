# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0003_auto_20150716_1249'),
    ]

    operations = [
        migrations.AddField(
            model_name='cruiseline',
            name='logo',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
