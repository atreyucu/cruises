# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0015_filterlength'),
    ]

    operations = [
        migrations.AlterField(
            model_name='region',
            name='name',
            field=models.CharField(max_length=200, unique=True, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='subregion',
            name='name',
            field=models.CharField(unique=True, max_length=200),
        ),
    ]
