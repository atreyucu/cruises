# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0006_auto_20150728_1956'),
    ]

    operations = [
        migrations.CreateModel(
            name='Port',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='TravelDatesPort',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('day_number', models.IntegerField(null=True, blank=True)),
                ('date', models.DateField(null=True, blank=True)),
                ('depart_time', models.TimeField(null=True, blank=True)),
                ('return_time', models.TimeField(null=True, blank=True)),
                ('port', models.ForeignKey(blank=True, to='backend.Port', null=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='traveldates',
            name='all_port',
        ),
        migrations.AddField(
            model_name='traveldatesport',
            name='travel_date',
            field=models.ForeignKey(blank=True, to='backend.TravelDates', null=True),
        ),
        migrations.AddField(
            model_name='traveldates',
            name='visited_ports',
            field=models.ManyToManyField(to='backend.Port', through='backend.TravelDatesPort'),
        ),
    ]
