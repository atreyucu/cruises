# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0020_auto_20150909_1511'),
    ]

    operations = [
        migrations.AlterField(
            model_name='traveldates',
            name='balcony_price',
            field=models.FloatField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='traveldates',
            name='inside_price',
            field=models.FloatField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='traveldates',
            name='ocean_price',
            field=models.FloatField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='traveldates',
            name='suite_price',
            field=models.FloatField(null=True, blank=True),
        ),
    ]
