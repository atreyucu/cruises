# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0011_auto_20150807_1417'),
    ]

    operations = [
        migrations.CreateModel(
            name='FilterDate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pages_number', models.IntegerField(default=0)),
                ('month', models.CharField(max_length=50)),
                ('year', models.CharField(max_length=50)),
            ],
        ),
    ]
