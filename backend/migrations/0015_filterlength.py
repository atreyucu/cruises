# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0014_filterdate_year_month_date'),
    ]

    operations = [
        migrations.CreateModel(
            name='FilterLength',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('length_value', models.CharField(max_length=10)),
                ('length_text', models.CharField(max_length=50)),
            ],
        ),
    ]
