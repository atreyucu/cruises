# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0012_filterdate'),
    ]

    operations = [
        migrations.AddField(
            model_name='port',
            name='is_depart',
            field=models.BooleanField(default=0),
        ),
    ]
