# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0026_ship_slug'),
    ]

    operations = [
        migrations.AddField(
            model_name='port',
            name='slug',
            field=models.SlugField(null=True),
        ),
    ]
