# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0031_auto_20150929_1730'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='country',
            options={'ordering': ['name'], 'verbose_name_plural': 'countries'},
        ),
        migrations.AlterModelOptions(
            name='port',
            options={'verbose_name_plural': 'ports'},
        ),
        migrations.RemoveField(
            model_name='port',
            name='is_depart',
        ),
        migrations.AlterField(
            model_name='country',
            name='is_active_depart',
            field=models.BooleanField(default=1, verbose_name=b'Active for departure'),
        ),
        migrations.AlterField(
            model_name='country',
            name='is_active_destination',
            field=models.BooleanField(default=1, verbose_name=b'Active for destination'),
        ),
    ]
