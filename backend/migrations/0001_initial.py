# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AdditionalSailingDates',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField(null=True, blank=True)),
                ('url', models.URLField(null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='CruiseLine',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, null=True, blank=True)),
                ('url', models.URLField(null=True, blank=True)),
                ('description', models.TextField(null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Region',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, null=True, blank=True)),
                ('description', models.TextField(null=True, blank=True)),
                ('url', models.URLField(null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Ship',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, null=True, blank=True)),
                ('url', models.URLField(null=True, blank=True)),
                ('description', models.TextField(null=True, blank=True)),
                ('big_logo_src', models.CharField(max_length=200, null=True, blank=True)),
                ('small_logo_src', models.CharField(max_length=200, null=True, blank=True)),
                ('cruise_line', models.ForeignKey(on_delete=b'CASCADE', to='backend.CruiseLine', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='SubRegion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('description', models.TextField(null=True, blank=True)),
                ('url', models.URLField(null=True, blank=True)),
                ('region', models.ForeignKey(on_delete=b'CASCADE', to='backend.Region', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Travel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('unique_id', models.CharField(max_length=255, null=True, blank=True)),
                ('name', models.CharField(max_length=200, null=True, blank=True)),
                ('url', models.URLField(null=True, blank=True)),
                ('nights', models.IntegerField(default=0)),
                ('destination', models.ForeignKey(blank=True, to='backend.SubRegion', null=True)),
                ('ship', models.ForeignKey(to='backend.Ship')),
            ],
        ),
        migrations.CreateModel(
            name='TravelDates',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('depart_date', models.CharField(max_length=200, null=True, blank=True)),
                ('return_date', models.CharField(max_length=200, null=True, blank=True)),
                ('inside_price', models.CharField(max_length=200, null=True, blank=True)),
                ('ocean_price', models.CharField(max_length=200, null=True, blank=True)),
                ('balcony_price', models.CharField(max_length=200, null=True, blank=True)),
                ('suite_price', models.CharField(max_length=200, null=True, blank=True)),
                ('departure_port', models.CharField(max_length=255, null=True, blank=True)),
                ('all_port', models.CharField(max_length=255, null=True, blank=True)),
                ('travel', models.ForeignKey(blank=True, to='backend.Travel', null=True)),
            ],
        ),
        migrations.AddField(
            model_name='additionalsailingdates',
            name='travel',
            field=models.ForeignKey(to='backend.Travel'),
        ),
    ]
