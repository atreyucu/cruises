# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0038_auto_20151016_2158'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dinamictext',
            name='second_text',
            field=models.TextField(null=True),
        ),
    ]
