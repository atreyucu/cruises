# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0021_auto_20150910_1505'),
    ]

    operations = [
        migrations.AddField(
            model_name='traveldates',
            name='slug',
            field=models.SlugField(null=True, blank=True),
        ),
    ]
