# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0023_remove_traveldates_slug'),
    ]

    operations = [
        migrations.AddField(
            model_name='cruiseline',
            name='slug',
            field=models.SlugField(null=True),
        ),
    ]
