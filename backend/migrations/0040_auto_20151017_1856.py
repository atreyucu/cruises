# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0039_auto_20151017_1621'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dinamictext',
            name='cruise_line1',
            field=models.PositiveIntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='dinamictext',
            name='cruise_line2',
            field=models.PositiveIntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='dinamictext',
            name='cruise_line3',
            field=models.PositiveIntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='dinamictext',
            name='cruise_line4',
            field=models.PositiveIntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='dinamictext',
            name='cruise_line5',
            field=models.PositiveIntegerField(null=True),
        ),
    ]
