# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0007_auto_20150729_1302'),
    ]

    operations = [
        migrations.RenameField(
            model_name='traveldatesport',
            old_name='depart_time',
            new_name='arrive_time',
        ),
        migrations.AlterField(
            model_name='traveldates',
            name='id',
            field=models.AutoField(serialize=False, primary_key=True),
        ),
    ]
