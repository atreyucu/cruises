# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0025_auto_20150911_1622'),
    ]

    operations = [
        migrations.AddField(
            model_name='ship',
            name='slug',
            field=models.SlugField(null=True),
        ),
    ]
