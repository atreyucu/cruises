# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0005_auto_20150717_1257'),
    ]

    operations = [
        migrations.AlterField(
            model_name='traveldates',
            name='all_port',
            field=models.TextField(null=True, blank=True),
        ),
    ]
