# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0024_cruiseline_slug'),
    ]

    operations = [
        migrations.AddField(
            model_name='region',
            name='slug',
            field=models.SlugField(null=True),
        ),
        migrations.AddField(
            model_name='subregion',
            name='slug',
            field=models.SlugField(null=True),
        ),
    ]
