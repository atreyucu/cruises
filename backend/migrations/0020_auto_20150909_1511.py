# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0019_subscriptions'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subscriptions',
            name='email',
            field=models.EmailField(max_length=255),
        ),
    ]
