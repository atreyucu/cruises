# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0016_auto_20150831_1925'),
    ]

    operations = [
        migrations.AlterField(
            model_name='filterdate',
            name='year_month_date',
            field=models.DateField(max_length=7, null=True),
        ),
    ]
