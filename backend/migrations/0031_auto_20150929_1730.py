# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0030_auto_20150915_1540'),
    ]

    operations = [
        migrations.RenameField(
            model_name='country',
            old_name='is_active',
            new_name='is_active_depart',
        ),
        migrations.AddField(
            model_name='country',
            name='is_active_destination',
            field=models.BooleanField(default=1),
        ),
        migrations.AddField(
            model_name='port',
            name='is_active_depart',
            field=models.BooleanField(default=1),
        ),
        migrations.AddField(
            model_name='port',
            name='is_active_destination',
            field=models.BooleanField(default=1),
        ),
    ]
