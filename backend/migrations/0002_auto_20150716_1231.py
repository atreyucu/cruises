# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='travel',
            name='url',
        ),
        migrations.AddField(
            model_name='traveldates',
            name='url',
            field=models.URLField(null=True, blank=True),
        ),
    ]
