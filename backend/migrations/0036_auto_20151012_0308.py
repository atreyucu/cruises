# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0035_auto_20151012_0218'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subscriptions',
            name='exported',
            field=models.BooleanField(default=0, verbose_name=b'Exported'),
        ),
    ]
