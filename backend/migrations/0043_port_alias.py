# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0042_dinamictext_slug'),
    ]

    operations = [
        migrations.AddField(
            model_name='port',
            name='alias',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
