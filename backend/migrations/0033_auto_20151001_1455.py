# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0032_auto_20151001_1402'),
    ]

    operations = [
        migrations.AlterField(
            model_name='port',
            name='is_active_depart',
            field=models.BooleanField(default=1, verbose_name=b'Active for departure'),
        ),
        migrations.AlterField(
            model_name='port',
            name='is_active_destination',
            field=models.BooleanField(default=1, verbose_name=b'Active for destination'),
        ),
    ]
