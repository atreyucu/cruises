# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0037_dinamictext'),
    ]

    operations = [
        migrations.AddField(
            model_name='dinamictext',
            name='tag',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='dinamictext',
            name='cruise_line1',
            field=models.ForeignKey(related_name='+', to='backend.CruiseLine', null=True),
        ),
        migrations.AlterField(
            model_name='dinamictext',
            name='cruise_line2',
            field=models.ForeignKey(related_name='+', to='backend.CruiseLine', null=True),
        ),
        migrations.AlterField(
            model_name='dinamictext',
            name='cruise_line3',
            field=models.ForeignKey(related_name='+', to='backend.CruiseLine', null=True),
        ),
        migrations.AlterField(
            model_name='dinamictext',
            name='cruise_line4',
            field=models.ForeignKey(related_name='+', to='backend.CruiseLine', null=True),
        ),
        migrations.AlterField(
            model_name='dinamictext',
            name='cruise_line5',
            field=models.ForeignKey(related_name='+', to='backend.CruiseLine', null=True),
        ),
    ]
