# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0041_auto_20151017_1910'),
    ]

    operations = [
        migrations.AddField(
            model_name='dinamictext',
            name='slug',
            field=models.CharField(max_length=200, null=True),
        ),
    ]
