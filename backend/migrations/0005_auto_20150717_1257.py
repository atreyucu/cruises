# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0004_cruiseline_logo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='traveldates',
            name='depart_date',
            field=models.DateField(max_length=200, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='traveldates',
            name='return_date',
            field=models.DateField(max_length=200, null=True, blank=True),
        ),
    ]
