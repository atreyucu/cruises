from backend.scraper.settings import LOG_FOLDER

from twisted.python.log import FileLogObserver
from scrapy import signals



class SpiderLog(object):

    @classmethod
    def from_crawler(cls, crawler):
        obj = cls()
        crawler.signals.connect(obj.setup_logfile, signal=signals.spider_opened)
        return obj

    def setup_logfile(self, spider):
        logfile = '%sproduction_%s.log' %(LOG_FOLDER, spider.name)
        fl = FileLogObserver(open(logfile, 'w+'))
        fl.start()