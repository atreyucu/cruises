# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/topics/item-pipeline.html
from django.forms.models import model_to_dict
from scrapy.exceptions import DropItem
from backend.models import CruiseLine, Ship, Region, RegionItem, SubRegionItem, SubRegion, CruiseLineItem, ShipItem, TravelItem, Travel, TravelDates, TravelDatesItem, TravelDatesPortItem, Port, TravelDatesPort, PageNumberItem, PageNumber, FilterDateItem, FilterDate, FilterLength, FilterLengthItem, Country
import logging
from frontend.cron import generate_cruiseline_text, generate_subregion_text, generate_ship_text, \
    generate_port_text_departure, generate_port_text_destination


class TutorialPipeline(object):
    def process_item(self, item, spider):
        return item

class CruiseLinePipeLine(object):
    def process_item(self, item, spider):
        if (spider.name == 'cruise_line_spider' or spider.name == 'other_cruise_line_spider') and isinstance(item, CruiseLineItem):
            #print 'pasando por el CRUISE LINE pipe line'
            try:
                cruise_line_item = CruiseLine.objects.get(name = item['name'])
                cruise_line_item.url = item['url']
                cruise_line_item.description = item['description']
                cruise_line_item.logo = item['logo']
                cruise_line_item.save()
                logging.log(logging.INFO, 'Update item Cruise line with name "%s". Double Item'%(item['name']))
            except CruiseLine.DoesNotExist:
                item.save()
                cruiseline = CruiseLine.objects.get(name = item['name'])
                generate_cruiseline_text(cruiseline)
                logging.log(logging.INFO, 'Item saved')
            #print 'terminando CRUISE LINE pipe line'
        return item

class CruiseShipPipeLine(object):
    def process_item(self, item, spider):
        if (spider.name == 'cruise_line_spider' or spider.name == 'cruise_spider' or spider.name == 'other_cruise_line_spider') and isinstance(item, ShipItem):
            #print 'pasando por el cruise ship pipe line'
            cruise_name = item['cruise_line']
            cruise_line = CruiseLine.objects.get(name = cruise_name)

            try:
                ship_name = item['name']
                cruise_ship = Ship.objects.get(name=ship_name)
                cruise_ship.description = item['description']
                if item['big_logo_src'].__contains__('http://www.cruiseplanners.com'):
                    cruise_ship.big_logo_src = item['big_logo_src'].replace('http://www.cruiseplanners.com', 'https://cruiseplannersnet.com')
                else:
                    cruise_ship.big_logo_src = item['big_logo_src']
                if item.get('small_logo_src', None) != None :
                    cruise_ship.small_logo_src = item['small_logo_src']
                cruise_ship.save()
                logging.log(logging.INFO, 'Updated item Ship with name "%s". Double Item'%(ship_name))
            except Ship.DoesNotExist:
                item['cruise_line'] = cruise_line
                if item['big_logo_src'].__contains__('http://www.cruiseplanners.com'):
                    item['big_logo_src'] = item['big_logo_src'].replace('http://www.cruiseplanners.com', 'https://cruiseplannersnet.com')
                else:
                    item['big_logo_src'] = item['big_logo_src']
                item.save()
                cruise_ship = Ship.objects.get(name=ship_name)
                generate_ship_text(cruise_ship)
                logging.log(logging.INFO, 'Saved item Ship with name "%s".'%(ship_name))
        return item

class RegionPipeLine(object):
    def process_item(self, item, spider):
        if spider.name == "destination_spider" and isinstance(item, RegionItem):
            #print 'pasando por el Region pipe line'
            try:
                region = Region.objects.get(name = item['name'])
                region.description = item['description']
                region.url = item['url']
                region.save()
                logging.log(logging.INFO, 'Saved item Region with name "%s".'%(item['name']))
            except Region.DoesNotExist:
                item.save()
                logging.log(logging.INFO, 'Updated item Region with name "%s". Double Item'%(item['name']))
        return item

class SubRegionPipeLine(object):
    def process_item(self, item, spider):
        if spider.name == "destination_spider" and isinstance(item, SubRegionItem):
            #print 'pasando por el Region pipe line'
            region = Region.objects.get(name = item['region'])
            try:
                subregion = SubRegion.objects.get(name = item['name'])
                subregion.description = item['description']
                subregion.url = item['url']
                subregion.save()
                logging.log(logging.INFO, 'Saved item SubRegion with name "%s".'%(item['name']))
            except SubRegion.DoesNotExist:
                item['region'] = region
                item['alias'] = item['name']
                item.save()
                subregion = SubRegion.objects.get(name = item['name'])
                generate_subregion_text(subregion)
                logging.log(logging.INFO, 'Updated item SuRegion with name "%s". Double Item'%(item['name']))
        return item

class TravelPipeLine(object):
    def process_item(self, item, spider):
        if spider.name == "travel_spider" and item is not None and isinstance(item, TravelItem):
            #print "Pasando por pipe line TRAVEL"
            try:
                travel_obj = Travel.objects.get(unique_id = item['unique_id'])
            except Travel.DoesNotExist:
                travel_obj = Travel()
                travel_obj.unique_id = item['unique_id']
                travel_obj.name = item['name']
                #travel_obj.url = item['url']
                nights = int(item['nights'])
                travel_obj.nights = nights
                travel_name = item['name']

                #extrayendo destination del travel name
                travel_name_arr = travel_name.split("%s-Night "%(nights))
                if len(travel_name_arr) > 1:
                    subregion_region = travel_name_arr[1]
                    destination_parts = subregion_region.split(' (')
                    if len(destination_parts) > 1:
                        region_name = destination_parts[0]
                        subregion_name = destination_parts[1]
                        if subregion_name[-1] == ')':
                            subregion_name = subregion_name[:-1]
                    else:
                        subregion_name = subregion_region.strip()
                        region_name = subregion_region.strip()
                    try:
                        subregion_obj = SubRegion.objects.get(name = subregion_name)
                    except SubRegion.DoesNotExist:
                        try:
                            region_obj = Region.objects.get(name = region_name)
                        except Region.DoesNotExist:
                            region_obj = Region(name = region_name)
                            region_obj.save()
                        subregion_obj = SubRegion()
                        subregion_obj.name = subregion_name
                        subregion_obj.alias = subregion_name
                        subregion_obj.region = region_obj
                        subregion_obj.save()
                        generate_subregion_text(subregion_obj)
                        logging.log(logging.WARNING, "New destination(sub region) object with name '%s' was added in the TRAVEL scraper process."%(subregion_name))
                travel_obj.destination = subregion_obj

                try:
                    ship = Ship.objects.get(name = item['ship'])
                except Ship.DoesNotExist:
                    raise DropItem("Dropped item TRAVEL, SHIP object with name '%s' does not exists."%(item['ship']))
                if item['ship_small_logo_src'] != None or item['ship_small_logo_src'] != '':
                    ship.small_logo_src = item['ship_small_logo_src']
                    ship.save()
                travel_obj.ship = ship
                travel_obj.save()
                logging.log(logging.INFO, 'TRAVEL with name %s was saved.'%(item['name']))
        return item

def extract_date(str_date):
    str_date_arr = str_date.split(' ')
    if len(str_date_arr) == 2:
        week_day = str_date_arr[0]
        date_str = str_date_arr[1]
        str_date_arr = date_str.split('/')
        if len(str_date_arr) == 3:
            month = str_date_arr[0]
            day = str_date_arr[1]
            year = str_date_arr[2]
            from datetime import datetime
            return '%s%s-%s-%s'%(str(datetime.now().year)[:2] ,year, month, day)
    return str_date


class TravelDatesPipeLine(object):

    def __get_pricing(self, price_str_param):
        price_str = price_str_param
        if price_str != None:
            if price_str == 'Call For Pricing':
                price_str = None
            else:
                price_str = price_str.replace('$', '')
                price_str = price_str.replace(',', '')
                try:
                    price_str = float(price_str)
                except Exception, e:
                    price_str = None
        return price_str

    def process_item(self, item, spider):
        if (spider.name == "travel_spider" or spider.name == 'travel_price_spider') and item is not None and isinstance(item, TravelDatesItem):
            #print "Pasando por pipe line TRAVEL DATES"
            try:
                travel = Travel.objects.get(unique_id = item['travel'])
                logging.log(logging.INFO,"Exists travel in TravelDates")
            except Travel.DoesNotExist:
                raise DropItem('TravelDate is dropped because TRAVEL with UNIQUE_ID = "%s" does not exists'%(item['travel']))

            try:
                travel_date_obj = TravelDates.objects.get(url = item['url'])
            except TravelDates.DoesNotExist:
                travel_date_obj = TravelDates()
                travel_date_obj.travel = travel
                travel_date_obj.url = item['url']
                travel_date_obj.return_date = extract_date(item['return_date'])
                travel_date_obj.depart_date = extract_date(item['depart_date'])
                travel_date_obj.departure_port = item['departure_port']

            travel_date_obj.balcony_price = self.__get_pricing(item['balcony_price'])
            travel_date_obj.ocean_price = self.__get_pricing(item['ocean_price'])
            travel_date_obj.suite_price = self.__get_pricing(item['suite_price'])
            travel_date_obj.inside_price = self.__get_pricing(item['inside_price'])
            import re
            match = re.search(r'\d+\%\d+\%\d+%3B\w+', item['url'])
            if match is None:
                travel_date_obj.save()
            #travel_date_obj.visited_ports.all().delete()
            logging.log(logging.INFO, 'TRAVEL DATE with url %s was updated or saved.'%(item['url']))
        return item

class TravelDatePortPipeLine(object):
    def process_item(self, item, spider):

        import django
        django.setup()

        if (spider.name == 'travel_spider' or spider.name == 'travel_price_spider') and isinstance(item, TravelDatesPortItem):
            #print 'Pasando por TRAVEL DATE PORT PIPE LINE'
            try:
                port = Port.objects.get(name=item['port_name'])
            except Port.DoesNotExist:
                port = Port(name=item['port_name'],alias=item['port_name'])
                country_name = item['country_name']
                if country_name != None:
                    try:
                        country = Country.objects.get(name = country_name)
                    except Country.DoesNotExist:
                        country = Country()
                        country.name = country_name
                        country.save()
                    except Country.MultipleObjectsReturned:
                        country = Country.objects.filter(name = country_name)[0]

                port.country = country

                port.save()
                generate_port_text_departure(port)
                generate_port_text_destination(port)

            #if item['day_number'] == '1' or item['day_number'] == 1:
            #    port.is_depart = True

            try:
                travel_date = TravelDates.objects.get(url=item['travel_date_url'])
                if item['day_number'] == '1' or item['day_number'] == 1:
                    travel_date.departure_port = port.name
                if item['is_destination_port']:
                    travel_date.destination_port = port
                    travel_date.save()
            except TravelDates.DoesNotExist:
                raise DropItem('Dropped Item TRAVEL DATE PORT with Travel date url "%s". Travel dates does not exists'%(item['travel_date_url']))

            travel_date_port = TravelDatesPort.objects.filter(travel_date = travel_date).filter(day_number = item['day_number'])

            if len(travel_date_port) == 0:
                travel_date_port = TravelDatesPort()
                travel_date_port.port = port
                travel_date_port.travel_date = travel_date
                date = item['date']

                if item['arrive_time'] == '--':
                    item['arrive_time'] = None
                if item['return_time'] == '--':
                    item['return_time'] = None

                item['date'] = extract_date(date)
                travel_date_port.day_number = item['day_number']
                travel_date_port.date = item['date']
                travel_date_port.arrive_time = item['arrive_time']
                travel_date_port.return_time = item['return_time']
            #else:
            #    travel_date_port = travel_date_port[0]
            #    travel_date_port.port = port

            #item.save()

                travel_date_port.save()
            logging.log(logging.INFO, 'TRAVEL DATE PORT day %s of TRAVEL DATE WITH URL %s was updated or saved.'%(item['day_number'], travel_date.url))
        return item

class PageNumberPipeLine(object):

    def process_item(self, item, spider):
        if isinstance(item, PageNumberItem):
            page_number = PageNumber.objects.all()
            if len(page_number) > 0:
                page_number = page_number[0]
                page_number.last_page_number = item['last_page_number']
                page_number.save()
            else:
                page_number = item
                page_number.save()
        return item

class FilterDatePipeLine(object):
    def process_item(self, item, spider):
        if isinstance(item, FilterDateItem):
            try:
                filter_date_obj = FilterDate.objects.get(month = item['month'], year = item['year'])
            except FilterDate.DoesNotExist:
                filter_date_obj = FilterDate()
            filter_date_obj.year = item['year']
            filter_date_obj.month = item['month']
            filter_date_obj.pages_number = item['pages_number']
            filter_date_obj.year_month_date = item['year_month_date'] + '-01'
            filter_date_obj.save()
        return item

class FilterLengthPipeLine(object):
    def process_item(self, item, spider):
        if isinstance(item, FilterLengthItem):
            try:
                filter_length_obj = FilterLength.objects.get(length_value = item['length_value'])
            except FilterLength.DoesNotExist:
                filter_length_obj = FilterLength()
            filter_length_obj.length_value = item['length_value']
            filter_length_obj.length_text = item['length_text']
            filter_length_obj.save()
        return item
