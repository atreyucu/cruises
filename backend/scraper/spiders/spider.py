from scrapy.loader import ItemLoader
from scrapy.spiders import Spider
#from scrapy.selector import Selector
import logging
from scrapy.http import Request, Response
from scrapy import Selector, FormRequest
from backend.models import ShipItem, CruiseLineItem, RegionItem, SubRegionItem, TravelItem, TravelDatesItem, TravelDatesPortItem, PageNumberItem, \
    PageNumber, FilterDate, FilterDateItem, TravelDates, Travel, FilterLengthItem
import re

from django.template import defaultfilters

START_PAGE_NUMBER = 1
END_PAGE_NUMBER = 51
PAGINATION_URL = '/page:{page}/'
FILTER_DATE_URL = 'month:{month}/year:{year}'

BASE_URL = 'http://www.redhotcruiseplanners.com'
BASE_URL_SEARCH = BASE_URL + '/searches/cruise'
BASE_URL_CRUISE_LINE = BASE_URL +'/cruiseLines/view/'
SCRAPER_TRAVEL_ADDITIONAL_DATES = False

class DateFilterSpider(Spider):
    name = 'filter_date_spider'
    start_urls = [
        BASE_URL_SEARCH,
    ]

    def get_page_number(self, response):
        item = response.meta['item']
        xs = Selector(response)
        page_number_xpath = '//div[@class="paging_header_left"]/text()'
        try:
            page_number_object = xs.xpath(page_number_xpath).extract()[0].strip()
            page_number_arr = page_number_object.split(' of ')
            if len(page_number_arr) == 2:
                page_number = page_number_arr[1]

            item['pages_number'] = page_number
        except IndexError:
            page_number = 0
            item['pages_number'] = page_number
        yield item


    def parse(self, response):
        xs = Selector(response)
        date_filter_xpath = '//select[@id="SearchSailingDateMonthHome"]//option'
        date_filter_obj = xs.xpath(date_filter_xpath)
        for obj in date_filter_obj:
            obj_text = obj.xpath('text()').extract()
            print obj_text
            if len(obj_text) > 0:
                obj_text = obj_text[0]
                print obj_text
                obj_value = obj.xpath('@value').extract()[0].strip()
                #obj = obj.strip()
                arr = obj_text.split(' - ')
                month = arr[0]
                year = arr[1]

                filter_item = FilterDateItem()
                filter_item['year'] = year
                filter_item['month'] = month
                filter_item['year_month_date'] = obj_value
                filter_url = FILTER_DATE_URL.format(month=month, year=year)
                yield Request(BASE_URL_SEARCH + '/' + filter_url, callback=self.get_page_number, meta={
                                'item': filter_item}
                                )


class LengthFilterSpider(Spider):
    name = 'filter_length_spider'
    start_urls = [
        BASE_URL_SEARCH,
    ]

    def parse(self, response):
        xs = Selector(response)
        date_filter_xpath = '//select[@id="SearchLengthHome"]//option'
        date_filter_obj = xs.xpath(date_filter_xpath)
        for obj in date_filter_obj:
            obj_text = obj.xpath('text()').extract()
            if len(obj_text) > 0:
                obj_text = obj_text[0]
                obj_value = obj.xpath('@value').extract()[0].strip()
                filter_item = FilterLengthItem()
                filter_item['length_value'] = obj_value
                filter_item['length_text'] = obj_text
                yield filter_item

class TravelSpider(Spider):
    name = "travel_spider"
    start_urls = [
        BASE_URL_SEARCH,
    ]
    def __init__(self):
        start_url = self.start_urls[0]

        #page_number_obj = PageNumber.objects.all()
        filter_date_objs = FilterDate.objects.order_by('year')
        urls = []
        for f_date in filter_date_objs:
            if f_date.pages_number != 0:
                pages_number = f_date.pages_number + 1
                for page in range(1, pages_number):
                    url = BASE_URL_SEARCH + PAGINATION_URL.format(page=page) + FILTER_DATE_URL.format(month = f_date.month, year = f_date.year)
                    urls.append(url)

        self.start_urls = urls

    def get_price(self, xs, price_type):

        if price_type == 1:
            price = xs.xpath('//span[@id="inside_price_id"]/text()').extract()
        elif price_type == 2:
            price = xs.xpath('//span[@id="oceanview_price_id"]/text()').extract()
        elif price_type == 3:
            price = xs.xpath('//span[@id="balcony_price_id"]/text()').extract()
        elif price_type == 4:
            price = xs.xpath('//span[@id="suite_price_id"]/text()').extract()

        price_xpath_base = '//span[@id="cruiseInfo"]/table[position()=2]/tr[position()=4]/td/table/tr'

        price_base_obj = xs.xpath(price_xpath_base)


        if len(price) > 0:
            price = price[0]
        else:
            price = price_base_obj.xpath('td[position()=%s]/text()'%(price_type)).extract()
            exists = False
            for i_p in price:
                if i_p.__contains__('$'):
                    price = i_p.strip()
                    exists = True
            if not exists:
                price_obj = price_base_obj.xpath('td[position()=%s]/span'%(price_type))
                for p_obj in price_obj:
                    price = p_obj.xpath('text()').extract()
                    if len(price) > 0:
                        if price[0].__contains__('$'):
                            price = price[0].strip()
        return price

    def parse_details_page(self, response):
        xs = Selector(response)
        base_xpath = '//section[@class="itinerary_details_container"]'
        try:
            base_obj = xs.xpath(base_xpath)[0]
            depart_port = base_obj.xpath('div[position()=2]/div[position()=2]/text()').extract()[0].strip()
            depart_date = base_obj.xpath('div[position()=2]/div[position()=4]/text()').extract()[0].strip()
            return_date = base_obj.xpath('div[position()=2]/div[position()=6]/text()').extract()[0].strip()
            nights = base_obj.xpath('div[position()=2]/div[position()=8]/text()').extract()[0].strip()
        except Exception, e:
            logging.log(logging.ERROR, 'XPath does not exists. Error message: ' + e.message)

        inside_price = self.get_price(xs, 1)
        ocean_price = self.get_price(xs, 2)
        balcony_price = self.get_price(xs, 3)
        suite_price = self.get_price(xs, 4)

        travel_name_xpath = '//span[@id="cruiseInfo"]/table[position()=1]/tr/td[position()=1]/text()'

        try:
            travel_name = xs.xpath(travel_name_xpath).extract()[0].strip()

            ship_name = base_obj.xpath('div[position()=2]/div[position()=10]/text()').extract()[0].strip()
            cruise_line_name = base_obj.xpath('div[position()=2]/div[position()=12]/text()').extract()[0].strip()
        except Exception, e:
            logging.log(logging.ERROR, 'XPath does not exists. Error message: ' + e.message)

        #travel_item = response.meta['item']
        travel_item = TravelItem()
        travel_item['name'] = travel_name
        travel_item['ship'] = ship_name
        if response.meta.has_key('ship'):
            travel_item['ship_small_logo_src'] = response.meta['ship']['small_logo_src']
        travel_item['nights'] = nights
        travel_name_temp = travel_name
        travel_item['unique_id'] = travel_name_temp + '-' + cruise_line_name + '-' + travel_item['ship']

        travel_date_item = TravelDatesItem()
        travel_date_item['url'] = response.url
        travel_date_item['departure_port'] = depart_port
        travel_date_item['depart_date'] = depart_date
        travel_date_item['return_date'] = return_date

        travel_date_item['inside_price'] = inside_price
        travel_date_item['ocean_price'] = ocean_price
        travel_date_item['balcony_price'] = balcony_price
        travel_date_item['suite_price'] = suite_price
        #travel_date_item['all_port'] = response.meta['travel_date_data']['all_port']

        ports_days_xpath = '//span[@id="cruiseInfo"]/table[position()=4]/tr/td/table/tr[position()>1]'
        ports_days_obj = xs.xpath(ports_days_xpath)
        if len(ports_days_obj) == 0:
            ports_days_xpath = '//span[@id="cruiseInfo"]/table[position()=3]/tr/td/table/tr[position()>1]'
            ports_days_obj = xs.xpath(ports_days_xpath)



        ports_data_scraped = []
        for pd_obj in ports_days_obj:
            if len(pd_obj.xpath('td')) == 5:
                try:
                    day = pd_obj.xpath('td[position()=1]/span/text()').extract()[0].strip()
                    date = pd_obj.xpath('td[position()=2]/span/text()').extract()[0].strip()
                    port = pd_obj.xpath('td[position()=3]/a/text()').extract()
                    if len(port) == 1:
                        port = port[0].strip()
                        if port != 'At Sea':
                            travel_date_port_item = TravelDatesPortItem()
                            port_arr = port.split(',')
                            if len(port_arr) == 2:
                                port = port_arr[0].strip()
                                country_name = port_arr[1].strip()

                            elif len(port_arr) == 3 and port_arr[2].strip() != 'The':
                                port = port_arr[0]
                                country_name = port_arr[2]

                            elif len(port_arr) == 3 and port_arr[2].strip() == 'The':
                                port = port_arr[0]
                                country_name = port_arr[1]
                            elif len(port_arr) == 1:
                                country_name = None
                                port = port_arr[0]

                            #match port name with SEA
                            match = re.search(r'\bsea\b|\bSea\b', port)
                            if match == None:
                                travel_date_port_item['port_name'] = port
                                if country_name != None:
                                    travel_date_port_item['country_name'] = country_name.strip()
                                else:
                                    travel_date_port_item['country_name'] = country_name

                                travel_date_port_item['day_number'] = day
                                travel_date_port_item['travel_date_url'] = travel_date_item['url']
                                travel_date_port_item['date'] = date
                                arrive_time = pd_obj.xpath('td[position()=4]/text()').extract()[0].strip()
                                return_time = pd_obj.xpath('td[position()=5]/text()').extract()[0].strip()
                                travel_date_port_item['arrive_time'] = arrive_time
                                travel_date_port_item['return_time'] = return_time
                                travel_date_port_item['is_destination_port'] = False

                                if str(len(ports_days_obj) - 1) == day:
                                    travel_date_port_item['is_destination_port'] = True

                                ports_data_scraped.append(travel_date_port_item)

                except Exception, e:
                    logging.log(logging.ERROR, 'XPath does not exists. Error message: ' + e.message + 'Travel url: ' + response.url)


        #for pds_item in ports_data_scraped:
        if len(ports_data_scraped) > 0:
            travel_item['unique_id'] += '-' + ports_data_scraped[0]['port_name'] + '-' + ports_data_scraped[-1]['port_name']

        travel_item['unique_id'] = defaultfilters.slugify(travel_item['unique_id'])

        travel_date_item['travel'] = travel_item['unique_id']
        print travel_item['unique_id']
        yield travel_item
        yield travel_date_item
        for pds_item in ports_data_scraped:
            yield pds_item
        ##print  ports_days_obj
        if response.meta.has_key('sailings'):
            sailings = response.meta['sailings']
            for sailing in sailings:
                yield Request(BASE_URL + sailing, callback=self.parse_details_page, meta={
                                    'sailings': [],
                })



    def parse(self, response, xs=None):
        #print  '-----------------------------'
        #print  str(self.state.get('items_count', 0)) + ' OF '+ str(len(self.start_urls))
        #print  '-----------------------------'
        hxs = Selector(response)
        items = hxs.xpath("//div[@class='search_result mobile-top-container bottom-line']")

        for i in items:
            try:
                cruise_line_name =     i.xpath("div/table/tr[position()=2]/td[position()=2]/div/a[position()=1]/text()").extract()[0]
                cruise_line_url = i.xpath("div/table/tr[position()=2]/td[position()=2]/div/a[position()=1]/@href").extract()[0]

                item_cruise_line = {'url': cruise_line_url, 'name': cruise_line_name}
                #yield Request(BASE_URL + cruise_line_url, callback=CruisesLinesSpider().parse_item_line, meta={'item':item_cruise_line})

                ship_small_logo = i.xpath("div/table/tr[position()=2]/td[position()=1]/img/@src").extract()[0]
                ship_name =     i.xpath("div/table/tr[position()=2]/td[position()=2]/div/a[position()=2]/text()").extract()[0]
                ship_url = i.xpath("div/table/tr[position()=2]/td[position()=2]/div/a[position()=2]/@href").extract()[0]
            except Exception, e:
                logging.log(logging.ERROR, 'XPath does not exists. Error message: ' + e.message)

            item_ship = ShipItem()
            item_ship['url']  = ship_url
            item_ship['name'] = ship_name
            item_ship['cruise_line'] = cruise_line_name
            item_ship['small_logo_src'] = ship_small_logo

            #yield Request(BASE_URL + ship_url, callback=CruisesLinesSpider().parse_item_ship, meta={'item':item_ship})

            try:
                travel_date_url = i.xpath("div/table/tr/td/h3[@style='padding: 0; margin: 0']/a/@href").extract()[0]
                ship_name = i.xpath("div/table/tr[position()=2]/td[position()=2]/div/a[position()=2]/text()").extract()[0].strip()
            except Exception, e:
                logging.log(logging.ERROR, 'XPath does not exists. Error message: ' + e.message)

            additional_dates_xpath_base = i.xpath("div/table/tr[position()=6]/td/table/tr")
            additional_dates_temp = []

            if SCRAPER_TRAVEL_ADDITIONAL_DATES:
                for add_dates in additional_dates_xpath_base:
                    year_text = add_dates.xpath('td[position()=1]/text()').extract()[0].strip()
                    year = year_text[:4]
                    dates_xpath = add_dates.xpath('td[position()=2]/a')
                    for d_xpath in dates_xpath:
                        add_url = d_xpath.xpath('@href').extract()[0].strip()
                        additional_dates_temp.append(add_url)

            yield Request(BASE_URL + travel_date_url, callback=self.parse_details_page, meta={
                                            'ship': {'small_logo_src': ship_small_logo},
                                            'sailings': additional_dates_temp,

            })

        self.state['items_count'] = self.state.get('items_count', 0) + 1


class TravelPriceSpider(Spider):
    name = 'travel_price_spider'
    start_urls = [
        BASE_URL_SEARCH,
        #'http://www.redhotcruiseplanners.com/sailing/8-nights-royal-caribbean-international-navigator-of-the-seas-3-19-16'
    ]


    def __init__(self):
       start_url = self.start_urls[0]

       travel_dates_q = TravelDates.objects.all()
       urls = []
       for td in travel_dates_q:
           urls.append(td.url)
       self.start_urls = urls

    def get_price(self, xs, price_type):

        if price_type == 1:
            price = xs.xpath('//span[@id="inside_price_id"]/text()').extract()
        elif price_type == 2:
            price = xs.xpath('//span[@id="oceanview_price_id"]/text()').extract()
        elif price_type == 3:
            price = xs.xpath('//span[@id="balcony_price_id"]/text()').extract()
        elif price_type == 4:
            price = xs.xpath('//span[@id="suite_price_id"]/text()').extract()

        price_xpath_base = '//span[@id="cruiseInfo"]/table[position()=2]/tr[position()=4]/td/table/tr'

        price_base_obj = xs.xpath(price_xpath_base)


        if len(price) > 0:
            price = price[0]
        else:
            price = price_base_obj.xpath('td[position()=%s]/text()'%(price_type)).extract()
            exists = False
            for i_p in price:
                if i_p.__contains__('$'):
                    price = i_p.strip()
                    exists = True
            if not exists:
                price_obj = price_base_obj.xpath('td[position()=%s]/span'%(price_type))
                for p_obj in price_obj:
                    price = p_obj.xpath('text()').extract()
                    if len(price) > 0:
                        if price[0].__contains__('$'):
                            price = price[0].strip()
        return price

    def parse(self, response, xs=None):
        xs = Selector(response)
        base_xpath = '//section[@class="itinerary_details_container"]'
        try:
            base_obj = xs.xpath(base_xpath)[0]
            depart_port = base_obj.xpath('div[position()=2]/div[position()=2]/text()').extract()[0].strip()
            depart_date = base_obj.xpath('div[position()=2]/div[position()=4]/text()').extract()[0].strip()
            return_date = base_obj.xpath('div[position()=2]/div[position()=6]/text()').extract()[0].strip()
            nights = base_obj.xpath('div[position()=2]/div[position()=8]/text()').extract()[0].strip()
        except Exception, e:
            logging.log(logging.ERROR, 'XPath does not exists. Error message: ' + e.message)

        inside_price = self.get_price(xs, 1)
        ocean_price = self.get_price(xs, 2)
        balcony_price = self.get_price(xs, 3)
        suite_price = self.get_price(xs, 4)

        travel_name_xpath = '//span[@id="cruiseInfo"]/table[position()=1]/tr/td[position()=1]/text()'

        try:
            travel_name = xs.xpath(travel_name_xpath).extract()[0].strip()

            ship_name = base_obj.xpath('div[position()=2]/div[position()=10]/text()').extract()[0].strip()
            cruise_line_name = base_obj.xpath('div[position()=2]/div[position()=12]/text()').extract()[0].strip()
        except Exception, e:
            logging.log(logging.ERROR, 'XPath does not exists. Error message: ' + e.message)

        #travel_item = response.meta['item']
        travel_item = TravelItem()
        travel_item['name'] = travel_name
        travel_item['ship'] = ship_name
        if response.meta.has_key('ship'):
            travel_item['ship_small_logo_src'] = response.meta['ship']['small_logo_src']
        travel_item['nights'] = nights
        travel_name_temp = travel_name
        travel_item['unique_id'] = travel_name_temp + '-' + cruise_line_name + '-' + travel_item['ship']

        travel_date_item = TravelDatesItem()
        travel_date_item['url'] = response.url
        travel_date_item['departure_port'] = depart_port
        travel_date_item['depart_date'] = depart_date
        travel_date_item['return_date'] = return_date

        travel_date_item['inside_price'] = inside_price
        travel_date_item['ocean_price'] = ocean_price
        travel_date_item['balcony_price'] = balcony_price
        travel_date_item['suite_price'] = suite_price
        #travel_date_item['all_port'] = response.meta['travel_date_data']['all_port']

        ports_days_xpath = '//span[@id="cruiseInfo"]/table[position()=4]/tr/td/table/tr[position()>1]'
        ports_days_obj = xs.xpath(ports_days_xpath)
        if len(ports_days_obj) == 0:
            ports_days_xpath = '//span[@id="cruiseInfo"]/table[position()=3]/tr/td/table/tr[position()>1]'
            ports_days_obj = xs.xpath(ports_days_xpath)



        ports_data_scraped = []
        for pd_obj in ports_days_obj:
            if len(pd_obj.xpath('td')) == 5:
                try:
                    day = pd_obj.xpath('td[position()=1]/span/text()').extract()[0].strip()
                    date = pd_obj.xpath('td[position()=2]/span/text()').extract()[0].strip()
                    port = pd_obj.xpath('td[position()=3]/a/text()').extract()
                    if len(port) == 1:
                        port = port[0].strip()
                        if port != 'At Sea':
                            travel_date_port_item = TravelDatesPortItem()
                            port_arr = port.split(',')
                            if len(port_arr) == 2:
                                port = port_arr[0].strip()
                                country_name = port_arr[1].strip()

                            elif len(port_arr) == 3 and port_arr[2].strip() != 'The':
                                port = port_arr[0]
                                country_name = port_arr[2]

                            elif len(port_arr) == 3 and port_arr[2].strip() == 'The':
                                port = port_arr[0]
                                country_name = port_arr[1]

                            #match port name with SEA
                            match = re.search(r'\bsea\b|\bSea\b', port)
                            if match == None:
                                travel_date_port_item['port_name'] = port
                                travel_date_port_item['country_name'] = country_name.strip()

                                travel_date_port_item['day_number'] = day
                                travel_date_port_item['travel_date_url'] = travel_date_item['url']
                                travel_date_port_item['date'] = date
                                arrive_time = pd_obj.xpath('td[position()=4]/text()').extract()[0].strip()
                                return_time = pd_obj.xpath('td[position()=5]/text()').extract()[0].strip()
                                travel_date_port_item['arrive_time'] = arrive_time
                                travel_date_port_item['return_time'] = return_time
                                travel_date_port_item['is_destination_port'] = False

                                if str(len(ports_days_obj) - 1) == day:
                                    travel_date_port_item['is_destination_port'] = True

                                ports_data_scraped.append(travel_date_port_item)

                except Exception, e:
                    logging.log(logging.ERROR, 'XPath does not exists. Error message: ' + e.message + "Travel: " + travel_date_item['url'])


        #for pds_item in ports_data_scraped:
        if len(ports_data_scraped) > 0:
            travel_item['unique_id'] += '-' + ports_data_scraped[0]['port_name'] + '-' + ports_data_scraped[-1]['port_name']

        travel_item['unique_id'] = defaultfilters.slugify(travel_item['unique_id'])

        travel_date_item['travel'] = travel_item['unique_id']
        print travel_item['unique_id']
        yield travel_item
        yield travel_date_item
        for pds_item in ports_data_scraped:
            yield pds_item
        ##print  ports_days_obj
        if response.meta.has_key('sailings'):
            sailings = response.meta['sailings']
            for sailing in sailings:
                yield Request(BASE_URL + sailing, callback=self.parse_details_page, meta={
                                    'sailings': [],
                })


class DestinationSpider(Spider):
    name = "destination_spider"
    start_urls = [
        "http://www.redhotcruiseplanners.com/destinations",
    ]

    def parse_subregions(self, response):
        xs = Selector(response)
        subregion_description = xs.xpath('//div[@class="contentBlock"]/p').extract()

        desc_temp = ''
        for d in subregion_description:
            desc_temp += d

        item = response.meta['item']
        item['description'] = desc_temp
        yield item

    def parse(self, response):
        xs = Selector(response)
        base_xpath = '//div[@id="destinations"]'
        base_objects = xs.xpath(base_xpath)

        for b_obj in base_objects:
            destination_item = RegionItem()
            name_xpath = 'h4/a/text()'
            url_xpath = 'h4/a/@href'
            description_xpath = 'p/text()'

            name = b_obj.xpath(name_xpath).extract()[0].strip()
            description_data = b_obj.xpath(description_xpath).extract()
            description = None
            if len(description_data) >= 1:
                description = ''
                for d in description_data:
                    description += d
            url = b_obj.xpath(url_xpath).extract()[0]

            destination_item['name'] = name
            destination_item['url'] = url
            destination_item['description'] = description

            yield destination_item

            subregions_xpath_base = 'div[@class="subregions-list-container"]/a'
            subregions_objects = b_obj.xpath(subregions_xpath_base)

            subregions_data_temp = []

            for sub_objects in subregions_objects:
                subregions_item = SubRegionItem()
                subregions_item['url'] = sub_objects.xpath('@href').extract()[0].strip()
                subregions_item['name'] = sub_objects.xpath('text()').extract()[0].strip()
                subregions_data_temp.append(subregions_item)

                subregions_item['region'] = destination_item['name']
                if subregions_item['url'].__contains__(BASE_URL) == False:
                    subregions_item['url'] = BASE_URL + subregions_item['url']
                yield Request(subregions_item['url'], callback=self.parse_subregions, meta={'item':subregions_item})

            #yield destination_item




class CruisesLinesSpider(Spider):
    name = "cruise_line_spider"

    start_urls = [
        "http://www.redhotcruiseplanners.com/cruiseLines",
    ]

    def parse_item_ship(self, response, xs=None):
        xs = Selector(response)
        base_obj = xs.xpath('//div[@class="contentBlock"]')
        image_src = base_obj.xpath('div/img/@src').extract()[0].strip()
        description = base_obj.xpath('div/p').extract()

        description_temp = ""
        for d in description:
            description_temp += d

        item = response.meta['item']
        item['big_logo_src'] = image_src
        item['description'] = description_temp
        yield item

    def parse_item_line(self, response, xs=None):
        xs = Selector(response)
        base_xpath = '//div[@class="contentBlock"]'
        description_xpath = 'div[position()=1]/p'
        base_obj = xs.xpath(base_xpath)
        logo = base_obj.xpath('div[position()=1]/img/@src').extract()[0]

        description_all = base_obj.xpath(description_xpath).extract()
        description_cruise_line = ''
        for d in description_all:
            description_cruise_line += d

        item_cruise_line = CruiseLineItem()
        item_cruise_line['name'] =  response.meta['item']['name']
        item_cruise_line['description'] = description_cruise_line
        item_cruise_line['url'] =  response.meta['item']['url']
        item_cruise_line['logo'] = logo
        yield item_cruise_line

        #ship_data = []
        ship_xpath = 'ul/li'
        ship_objs = base_obj.xpath(ship_xpath)


        for s_obj in ship_objs:
            ship_url = s_obj.xpath('div[@class="list_content"]/span/a/@href').extract()[0].strip()
            ship_name = s_obj.xpath('div[@class="list_content"]/span/a/text()').extract()[0].strip()
            ship_name = ship_name.encode('ascii', errors='ignore')
            ship_name = ship_name.strip()
            #ship_temp = {'name': ship_name, 'url': ship_url}
            #ship_data.append(ship_temp)

            item_ship = ShipItem()
            item_ship['url']  = ship_url
            item_ship['name'] = ship_name
            item_ship['cruise_line'] = item_cruise_line['name']
            if ship_url.__contains__(BASE_URL) == False:
                ship_url = BASE_URL+ship_url
            yield Request(ship_url, callback=self.parse_item_ship, meta={'item':item_ship})



    def parse(self, response):
        xs = Selector(response)
        base_objects = xs.xpath("//ul[@class='data_list mobile-floating-list ten-ninety-list']/li")
        for obj in base_objects:
            url_xpath = 'div[position()=2]/span/a/@href'
            url = obj.xpath(url_xpath).extract()
            name_xpath = 'div[position()=2]/span/a/text()'
            name = obj.xpath(name_xpath).extract()[0].strip()
            if(len(url) > 0):
                url_path = url[0].strip()
                if url_path.__contains__(BASE_URL) == False:
                    url_path = BASE_URL + url_path
                item = {'url': url_path, 'name': name}
                yield Request(url_path, callback=self.parse_item_line, meta={'item':item})

class OtherCruisesLinesSpider(Spider):
    name = "other_cruise_line_spider"

    start_urls = [
        "http://www.redhotcruiseplanners.com/searches/cruise",
    ]

    def parse(self, response):
        xs = Selector(response);
        base_objects = xs.xpath('//select[@id="SearchCruiseLineHome"]/option')

        for b_obj in base_objects:
            line_value_xpath = '@value'
            line_value = b_obj.xpath(line_value_xpath).extract()[0].strip()
            if line_value != '0' and line_value != 0:
                line_name_xpath = 'text()';
                line_name = b_obj.xpath(line_name_xpath).extract()[0].strip()

                line_url = BASE_URL_CRUISE_LINE + line_name
                item = {'url' : line_url, 'name': line_name}
                yield Request(line_url, callback=self.parse_item_line, meta={'item':item})

    def parse_item_ship(self, response, xs=None):
        xs = Selector(response)
        base_obj = xs.xpath('//div[@class="contentBlock"]')
        image_src = base_obj.xpath('div/img/@src').extract()[0]
        description = base_obj.xpath('div/p').extract()

        description_temp = ""
        for d in description:
            description_temp += d

        item = response.meta['item']
        item['big_logo_src'] = image_src
        item['description'] = description_temp
        yield item

    def parse_item_line(self, response, xs=None):
        xs = Selector(response)
        base_xpath = '//div[@class="contentBlock"]'
        description_xpath = 'div[position()=1]/p'
        base_obj = xs.xpath(base_xpath)
        logo = base_obj.xpath('div[position()=1]/img/@src').extract()[0]

        description_all = base_obj.xpath(description_xpath).extract()
        description_cruise_line = ''
        for d in description_all:
            description_cruise_line += d

        item_cruise_line = CruiseLineItem()
        item_cruise_line['name'] =  response.meta['item']['name']
        item_cruise_line['description'] = description_cruise_line
        item_cruise_line['url'] =  response.meta['item']['url']
        item_cruise_line['logo'] = logo
        yield item_cruise_line

        #ship_data = []
        ship_xpath = 'ul/li'
        ship_objs = base_obj.xpath(ship_xpath)


        for s_obj in ship_objs:
            ship_url = s_obj.xpath('div[@class="list_content"]/span/a/@href').extract()[0]
            ship_name = s_obj.xpath('div[@class="list_content"]/span/a/text()').extract()[0]

            item_ship = ShipItem()
            item_ship['url']  = ship_url
            ship_name = ship_name.encode('ascii', errors='ignore')
            ship_name = ship_name.strip()
            item_ship['name'] = ship_name
            item_ship['cruise_line'] = item_cruise_line['name']
            if ship_url.__contains__(BASE_URL) == False:
                ship_url = BASE_URL+ship_url
            yield Request(ship_url, callback=self.parse_item_ship, meta={'item':item_ship})



class test(Spider):
    name = "test"

    start_urls = [
        "http://cruises.dev/search/",
    ]

    def parse(self, response):
        xs = Selector(response);
        base_object = xs.xpath('//div')
        print 'test'+ '\n'