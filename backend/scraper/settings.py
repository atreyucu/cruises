# Scrapy settings for tutorial project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

#BOT_NAME = 'tutorial'

#NEWSPIDER_MODULE = 'tutorial.spiders'

#DOWNLOADER_MIDDLEWARES = {
#    'tutorial.middlewares.CustomDownloaderMiddleware': 650
#}

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'tutorial (+http://www.yourdomain.com)'
from datetime import datetime

USER_AGENT = "lm_54.235.85.214_rhc"

import os
PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

ROOT_DIR = os.path.abspath(os.path.join(PROJECT_ROOT,'../../'))

LOG_FOLDER = os.path.abspath(os.path.join(ROOT_DIR,'spiders_logs'))

LOG_FILE = os.path.abspath(os.path.join(LOG_FOLDER,'general_log_%s.log'%(str(datetime.now()))))

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "cruises.settings")

EXTENSIONS = {
    #'backend.scraper.extensions.SpiderLog': 100,
}

SPIDER_MODULES = ['backend.scraper',]

ITEM_PIPELINES = {
    'backend.scraper.pipelines.CruiseShipPipeLine': 300,
    'backend.scraper.pipelines.CruiseLinePipeLine': 310,

    'backend.scraper.pipelines.RegionPipeLine': 400,
    'backend.scraper.pipelines.SubRegionPipeLine': 410,

    'backend.scraper.pipelines.TravelPipeLine': 500,
    'backend.scraper.pipelines.TravelDatesPipeLine': 510,
    'backend.scraper.pipelines.TravelDatePortPipeLine': 520,

    'backend.scraper.pipelines.PageNumberPipeLine': 600,

    'backend.scraper.pipelines.FilterDatePipeLine': 700,
    'backend.scraper.pipelines.FilterLengthPipeLine': 710,


    #'mybot.pipelines.validate.StoreMyItem': 800,
}
DUPEFILTER_DEBUG = True
LOG_LEVEL = 'INFO' #CRITICAL, ERROR, WARNING, INFO, DEBUG
#DOWNLOAD_DELAY = 0.25
DOWNLOAD_TIMEOUT = 360 #180
DOWNLOAD_MAXSIZE = 0#1073741824