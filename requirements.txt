Django==1.8.3
django-cronjobs==0.2.3
django-floppyforms==1.5.2
django-pure-pagination==0.2.1
django-suit==0.2.15
django-suit-redactor==0.0.2
MySQL-python==1.2.5
Scrapy==1.0.1
scrapy-djangoitem==1.0.0

# django-maintenance-mode==0.14.0
# mysqlclient==1.4.6

