from django.apps.config import AppConfig


class FrontendConfig(AppConfig):
    name = 'frontend'

    def ready(self):
        import frontend.signal