import floppyforms as forms
from backend.models import CruiseLine
from cruises import settings


class SelectImage(forms.Select):
    template_name = settings.FLOPPY_FORMS_TEMPLATES+'select.html'

    def get_context(self, name, value, attrs=None):
        ctx = super(SelectImage, self).get_context(name, value, attrs)
        options = []
        cruises_lines = CruiseLine.objects.all()
        for cruiseline in cruises_lines:
            options.append((str(cruiseline.id), cruiseline.name, cruiseline.logo))
        ctx['lines'] = options

        return ctx

    def _format_value(self, value):
        if len(value) == 1 and value[0] is None:
            return []
        return set([str(v) for v in value])

    class Media:
        js = (
            '/static/jquery.ddslick.min.js',
            '/static/widgets.js'
        )
