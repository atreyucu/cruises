from django.http.response import HttpResponsePermanentRedirect
import cruises.settings as settings
from django.http import HttpResponseRedirect, HttpResponse

SSL = 'SSL'


def request_is_secure(request):
    if request.is_secure():
        return True

    # Handle forwarded SSL (used at Webfaction)
    if 'HTTP_X_FORWARDED_SSL' in request.META:
        return request.META['HTTP_X_FORWARDED_SSL'] == 'on'

    if 'HTTP_X_SSL_REQUEST' in request.META:
        return request.META['HTTP_X_SSL_REQUEST'] == '1'

    return False

class SSLRedirect:
    def process_request(self, request):
        if request_is_secure(request):
            request.IS_SECURE=True
        return None

    def process_view(self, request, view_func, view_args, view_kwargs):
        if SSL in view_kwargs:
            secure = view_kwargs[SSL]
            del view_kwargs[SSL]
        else:
            secure = False

        if settings.DEBUG or not settings.ENABLED_SSL:
            return None

        if getattr(settings, "TESTMODE", False):
            return None

        if request.path.find('robots.txt') != -1:
            return None

        if not secure == request_is_secure(request):
            return self._redirect(request, secure)


    def _redirect(self, request, secure):
        if settings.DEBUG and request.method == 'POST':
            raise RuntimeError(
            """Django can't perform a SSL redirect while maintaining POST data.
                Please structure your views so that redirects only occur during GETs.""")

        protocol = secure and "https" or "http"

        host_name = request.META['HTTP_HOST']
        #host_name = host_name.replace('www.','')

        newurl = "%s://%s%s" % (protocol,host_name,request.get_full_path())

        #return HttpResponseRedirect(newurl) #HTML status code 302
        return HttpResponsePermanentRedirect(newurl) #HTML status code 301