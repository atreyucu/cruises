from django.contrib.sitemaps import Sitemap
from django.db.models.aggregates import Count
from backend.models import Port, SubRegion, CruiseLine, Ship, Travel, TravelDates
from django.contrib import sitemaps
from django.core.urlresolvers import reverse


class DeparturePortSitemap(Sitemap):
    changefreq = "daily"
    priority = 0.8
    protocol = 'https'

    def location(self, obj):
        return '/cruise-departures/{slug}/'.format(slug=obj.slug)

    def items(self):
        ports = Port.objects.get_departure_port()
        items = []
        for p in ports:
            if p.traveldates_set.filter(traveldatesport__day_number = 1).count() != 0:
               items.append(p)
        return items


class DestinationPortSitemap(Sitemap):
    changefreq = "daily"
    priority = 0.8
    protocol = 'https'

    def location(self, obj):
        return '/cruise-destinations/port/{slug}/'.format(slug=obj.slug)

    def items(self):
        ports = Port.objects.get_destination_port()
        items = []
        for p in ports:
            if p.traveldates_set.count() != 0:
                items.append(p)
        return items


class DestinationSubregionSitemap(Sitemap):
    changefreq = "daily"
    priority = 0.8
    protocol = 'https'

    def location(self, obj):
        return '/cruise-destinations/subregion/{slug}/'.format(slug=obj.slug)

    def items(self):
        subregions = SubRegion.objects.get_subregions_active()
        items = []
        for subregion in subregions:
            if TravelDates.objects.get_by_region_id(subregion.id).count() != 0:
                items.append(subregion)
        return items


class CruiseLineSitemap(Sitemap):
    changefreq = "daily"
    priority = 0.8
    protocol = 'https'

    def location(self, obj):
        return '/cruiselines/{slug}/'.format(slug=obj.slug)

    def items(self):
        cruiseslines = CruiseLine.objects.all()
        items = []
        for cl in cruiseslines:
            if TravelDates.objects.get_by_cruise_line_slug(cl.slug).count() != 0:
                items.append(cl)

        return items


class CruiseLineDestinationSitemap(Sitemap):
    cruiseline = CruiseLine()
    def __init__(self, obj):
        self.cruiseline = obj

    changefreq = "daily"
    priority = 0.8
    protocol = 'https'

    def location(self, obj):
        return '/cruiselines/{cruiseline}/subregion/{subregion}/'.format(cruiseline=self.cruiseline.slug,subregion=obj.slug)

    def items(self):
        ships = self.cruiseline.ship_set.all()
        travels = Travel.objects.values('destination_id').filter(ship__in = ships).order_by('destination__name').annotate(count = Count('destination_id'))
        items = []
        for travel in travels:
            subregion = SubRegion.objects.get(pk = travel['destination_id'])
            if TravelDates.objects.get_by_cruise_line_slug(self.cruiseline.slug).get_by_subregion_slug(subregion.slug).count() != 0:
                items.append(subregion)
        return items


class CruiseLineDepartureSitemap(Sitemap):
    cruiseline = CruiseLine()
    def __init__(self, obj):
        self.cruiseline = obj

    changefreq = "daily"
    priority = 0.8
    protocol = 'https'

    def location(self, obj):
        return '/cruiselines/{cruiseline}/departure/{port}/'.format(cruiseline=self.cruiseline.slug,port=obj.slug)

    def items(self):
        ships = self.cruiseline.ship_set.all()
        travels = TravelDates.objects.values('departure_port').filter(travel__ship_id__in = ships).filter(traveldatesport__day_number = 1).order_by('departure_port').annotate(count = Count('departure_port'))
        items = []
        for travel in travels:
            try:
                port = Port.objects.get_by_name(travel['departure_port'])
                if port.is_active_depart:
                    items.append(port)
            except Port.DoesNotExist:
                pass
        return items


class CruiseLineShipSitemap(Sitemap):
    cruiseline = CruiseLine()
    def __init__(self, obj):
        self.cruiseline = obj

    changefreq = "daily"
    priority = 0.8
    protocol = 'https'

    def location(self, obj):
        return '/cruiselines/{cruiseline}/ship/{ship}/'.format(cruiseline=obj.cruise_line.slug,ship=obj.slug)

    def items(self):
        items = []
        for ship in self.cruiseline.ship_set.all():
            if TravelDates.objects.get_by_ship_slug(ship.slug).count() != 0:
                items.append(ship)
        return items


class StaticViewSitemap(sitemaps.Sitemap):
    priority = 0.8
    changefreq = 'weekly'
    protocol = 'https'

    def items(self):
        return ['home', 'destinations', 'departures', 'cruise_lines']

    def location(self, item):
        return reverse(item)


class CustomViewSitemap(sitemaps.Sitemap):
    priority = 0.8
    changefreq = 'daily'
    protocol = 'https'

    def items(self):
        return ['romantic_danube']

    def location(self, item):
        return reverse(item)