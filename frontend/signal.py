from django.db.models.signals import post_delete, post_save
from django.dispatch.dispatcher import receiver
from cruises import settings
from frontend.models import BannerImage, CruiseDeal, TopDestination
import os.path
import shutil


@receiver(post_delete, sender=BannerImage)
@receiver(post_delete, sender=TopDestination)
def model_post_delete(sender, **kwargs):
    instance = kwargs['instance']
    static = settings.STATIC_ROOT
    if static == '':
        static = settings.MEDIA_ROOT
    path = '{static}{image}'.format(static=static,image=instance.image.__str__())
    if os.path.isfile(path):
        os.remove(path)
    media = settings.MEDIA_ROOT
    path = '{media}{image}'.format(media=media,image=instance.image.__str__())
    if os.path.isfile(path):
        os.remove(path)



@receiver(post_save, sender=BannerImage)
@receiver(post_save, sender=TopDestination)
def model_post_save(sender, **kwargs):
    instance = kwargs['instance']
    file = '{media}{image}'.format(media=settings.MEDIA_ROOT, image=instance.image.__str__())
    static = settings.STATIC_ROOT
    if static == '':
        static = settings.MEDIA_ROOT
    dir = '{static}/uploads'.format(static=static)

    shutil.copy(file, dir)
