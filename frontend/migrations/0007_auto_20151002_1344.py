# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.files.storage


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0006_auto_20151002_1340'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bannerimage',
            name='image',
            field=models.ImageField(storage=django.core.files.storage.FileSystemStorage(location=b'/home/ramdy/PycharmProjects/cruises/cruises/media/uploads'), upload_to=b''),
        ),
    ]
