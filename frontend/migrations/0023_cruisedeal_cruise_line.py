# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0036_auto_20151012_0308'),
        ('frontend', '0022_cruisedeal_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='cruisedeal',
            name='cruise_line',
            field=models.ForeignKey(on_delete=b'CASCADE', to='backend.CruiseLine', null=True),
        ),
    ]
