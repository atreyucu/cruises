# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0036_auto_20151012_0308'),
    ]

    operations = [
        migrations.CreateModel(
            name='BannerImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100, blank=True)),
                ('image', models.ImageField(help_text=b'The image dimension should be 1680x1050', upload_to=b'uploads')),
                ('order', models.PositiveIntegerField(null=True)),
            ],
        ),

    ]
