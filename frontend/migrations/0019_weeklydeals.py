# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0018_remove_cruisedeal_url'),
    ]

    operations = [
        migrations.CreateModel(
            name='WeeklyDeals',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.EmailField(unique=True, max_length=254)),
                ('exported', models.BooleanField(default=1, verbose_name=b'Exported')),
            ],
        ),
    ]
