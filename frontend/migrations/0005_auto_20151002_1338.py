# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.files.storage


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0004_topdestination'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bannerimage',
            name='image',
            field=models.ImageField(storage=django.core.files.storage.FileSystemStorage(location=b'/media/uploads'), upload_to=b''),
        ),
    ]
