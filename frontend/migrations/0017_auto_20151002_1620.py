# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0016_auto_20151002_1544'),
    ]

    operations = [
        migrations.AlterField(
            model_name='topdestination',
            name='image',
            field=models.ImageField(help_text=b'The image dimension should be 375x239', upload_to=b'uploads'),
        ),
    ]
