# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0021_remove_cruisedeal_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='cruisedeal',
            name='url',
            field=models.URLField(default=b''),
        ),
    ]
