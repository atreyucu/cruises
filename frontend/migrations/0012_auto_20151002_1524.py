# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0010_auto_20151002_1425'),
    ]

    operations = [
        migrations.AddField(
            model_name='topdestination',
            name='title',
            field=models.CharField(max_length=100, blank=True),
        ),
        migrations.AlterField(
            model_name='bannerimage',
            name='image',
            field=models.ImageField(help_text=b'Image dimension should be XxX', upload_to=b'uploads'),
        ),
    ]
