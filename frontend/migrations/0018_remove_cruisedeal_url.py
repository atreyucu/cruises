# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0017_auto_20151002_1620'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='cruisedeal',
            name='url',
        ),
    ]
