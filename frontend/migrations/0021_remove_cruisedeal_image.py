# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0020_delete_weeklydeals'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='cruisedeal',
            name='image',
        ),
    ]
