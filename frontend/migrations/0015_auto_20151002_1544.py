# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0014_auto_20151002_1527'),
    ]

    operations = [
        migrations.AlterField(
            model_name='topdestination',
            name='image',
            field=models.ImageField(help_text=b'The image width should be 256', upload_to=b'uploads'),
        ),
    ]
