# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0033_auto_20151001_1455'),
        ('frontend', '0003_auto_20151002_0344'),
    ]

    operations = [
        migrations.CreateModel(
            name='TopDestination',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(upload_to=b'uploads')),
                ('order', models.PositiveIntegerField()),
                ('destination', models.ForeignKey(blank=True, to='backend.SubRegion', null=True)),
            ],
        ),
    ]
