# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0015_auto_20151002_1544'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cruisedeal',
            name='image',
            field=models.ImageField(help_text=b'The image width should be 256', upload_to=b'uploads'),
        ),
    ]
