# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0023_cruisedeal_cruise_line'),
    ]

    operations = [
        migrations.AddField(
            model_name='cruisedeal',
            name='show_in_home',
            field=models.BooleanField(default=1, verbose_name=b'Show in Homepage'),
        ),
    ]
