# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0002_auto_20151002_0325'),
    ]

    operations = [
        migrations.CreateModel(
            name='CruiseDeal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(upload_to=b'uploads')),
                ('title', models.CharField(max_length=100)),
                ('description', models.TextField()),
                ('order', models.PositiveIntegerField()),
                ('expire_date', models.DateField()),
                ('url', models.URLField()),
            ],
        ),

    ]
