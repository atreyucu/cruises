# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0012_auto_20151002_1524'),
    ]

    operations = [
        migrations.AlterField(
            model_name='topdestination',
            name='title',
            field=models.CharField(max_length=100),
        ),
    ]
