from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index,{'SSL':True},name='home'),
    url(r'^cruise-destinations/$', views.destinations, {'SSL':True},name='destinations'),
    url(r'^cruise-departures/$', views.departures, {'SSL':True},name='departures'),
    url(r'^cruiselines/$', views.cruises_lines, {'SSL':True},name='cruise_lines'),

    url(r'^cruiselines/(?P<cruise_line_slug>[\w|\W]+)/ship/(?P<ship_slug>[\w|\W]+)/$', views.cruise_by_line_ship,{'SSL':True}),
    url(r'^cruiselines/(?P<cruise_line_slug>[\w|\W]+)/subregion/(?P<subregion>[\w|\W]+)/$', views.cruise_by_line_subregion,{'SSL':True}),
    url(r'^cruiselines/(?P<cruise_line_slug>[\w|\W]+)/departure/(?P<departure_port_slug>[\w|\W]+)/$', views.cruise_by_line_departure,{'SSL':True}),
    url(r'^cruiselines/(?P<slug>[\w|\W]+)/$', views.cruise_details,{'SSL':True}),

    url(r'^cruise-destinations/port/(?P<destination_port>[\w|\W]+)/$', views.cruise_by_destination_port,{'SSL':True}),
    url(r'^cruise-destinations/(?P<type>region|subregion)/(?P<destination>[\w|\W]+)/$', views.cruise_by_destination,{'SSL':True}),
    #url(r'^cruise-destinations/subregion/(?P<destination>[\w|\W]+)/$', views.cruise_by_destination),
    url(r'^cruise-departures/(?P<departure_port_slug>[\w|\W]+)/$', views.cruise_by_departure_port,{'SSL':True}),
    url(r'^cruise/(?P<cruiseid>[0-9]+)/$', views.cruise,{'SSL':True}),

    url(r'^search/$', views.search_cruises,{'SSL':True}),

    url(r'^search/(?:date/(?P<date>[0-9]{4}-[0-9]{2})/){0,1}(?:length/(?P<length>([0-9]{1,2}-[0-9]{1,2})|[0-9]{1,2}\+)/){0,1}(?:destination/(?P<destination>[\w|\W]+)/){0,1}(?:cruiseLine/(?P<cruise_line>[\w|\W]+)/){0,1}(?:ship/(?P<ship>[\w|\W]+)/){0,1}(?:departure-port/(?P<departure_port>[\w|\W]+)/){0,1}$', views.search_cruises,{'SSL':True}, name='search_cruises_name'),

    #url(r'^search/date:(?P<date>[\w|\W]+)/length:(?P<length>[\w|\W]+)/$', views.search_cruises, name='search_cruises'),
    #url(r'^summary/$', views.summary),

    url(r'^cruise-deals/$', views.cruise_deals,{'SSL':True}, name='cruise_deals'),

    url(r'^about-us/$', views.about_us,{'SSL':True}, name='about_us'),
    url(r'^contact-us/$', views.contact_us,{'SSL':True}, name='contact_us'),

    #----------ajax request------
    url(r'^all-pricing/$', views.travels_pricing,{'SSL':True}, name='travel_pricing'),
    url(r'^ship-by-cruise/$', views.get_ships_by_cruiseline,{'SSL':True}, name='ship_by_cruise'),
    url(r'^add_subscriber/$', views.subscribe,{'SSL':True}, name='add_subscriber'),
    url(r'^send_email/$', views.send_email,{'SSL':True},name='send_email'),

    url(r'^viking-romantic-danube/$', views.custom_article,{'SSL':True},name='romantic_danube'),
    #url(r'^testing/$', views.testing,{'SSL':True},name='testing'),
    #url(r'^testing2/$', views.testing2,{'SSL':True},name='testing2'),

    #url(r'^robots.txt$', views.robots,{'SSL':True},name='robots.txt')
    #url(r'^ports-by-country/(?P<country_id>[\d])/$', views.country_json, name='port_by_country'),





]