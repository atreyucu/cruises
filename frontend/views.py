import json
import urllib2
from django.core.mail import send_mail
from django.core.paginator import EmptyPage
from pure_pagination.paginator import PageNotAnInteger
from django.http.response import HttpResponse, Http404
from django.shortcuts import render_to_response, redirect
from cruises import settings
from cruises.settings import OBJECTS_PER_PAGE
# Create your views here.
from django.template.context import RequestContext
from backend.helpers import travel_obj_to_list, PaginatorCustom, get_daterange, _get_sort_result, BASE_IMAGES_URL, \
    travel_obj_to_list_custom, _remove_duplicates
from backend.models import Region, SubRegion, CruiseLine, TravelDates, Port, Ship, Subscriptions, Travel, Country, \
    DinamicText
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Min, Count
from frontend.models import CruiseDeal, TopDestination


def index(request):
    deals = CruiseDeal.objects.exclude(show_in_home=0).order_by('order')[:4]
    top_dest = TopDestination.objects.order_by('order')[:12]

    return render_to_response('frontend/index.html',{'deals': deals, 'top_dest': top_dest, 'base_image_url':BASE_IMAGES_URL},
                              context_instance = RequestContext(request))

def cruise_deals(request):
    try:
        page = request.GET.get('page', 1)
        page = int(page)
    except PageNotAnInteger:
        page = 1
    except ValueError:
        raise Http404

    cruises_deals = CruiseDeal.objects.order_by('order')
    paginator = PaginatorCustom(cruises_deals,OBJECTS_PER_PAGE, request=request)
    cruises_deals_page = paginator.page(page)

    return render_to_response('frontend/cruise_deals.html',
                              {'cruises_deals': cruises_deals,'deals_page': cruises_deals_page,'base_image_url':BASE_IMAGES_URL},
                              context_instance = RequestContext(request))

def about_us(request):
    return render_to_response('frontend/about_us.html', {}, context_instance = RequestContext(request))

def contact_us(request):
    return render_to_response('frontend/contact_us.html', {}, context_instance = RequestContext(request))

def destinations(request):

    regions_query = Region.objects.all()
    subregions_query = SubRegion.objects.get_subregions_active()

    regions_data = []
    list_temp = []
    #print regions_query
    for r in regions_query:
        #print TravelDates.objects.get_by_region_id(r.id).count()
        if TravelDates.objects.get_by_region_id(r.id).count() != 0:
            temp = {'id': r.id, 'name': r.name, 'slug': r.slug}
            list_temp.append(temp)
            if len(list_temp) == 3 or r == regions_query[len(regions_query) - 1]:
                regions_data.append(list_temp)
                list_temp = []

    subregions_data = []
    list_temp = []
    for sr in subregions_query:
         if TravelDates.objects.get_by_subregion_id(sr.id).count() != 0:
            temp = {'id': sr.id, 'name': sr.name, 'slug': sr.slug}
            subregions_data.append(temp)


    count_regions = len(subregions_data)
    part_1 = part_2 = part_3 = count_regions/3

    if count_regions%3 == 1:
        part_1 += 1
    elif count_regions%3 == 2:
        part_1 += 1
        part_2 += 1

    r_data_1 = subregions_data.__getslice__(0, part_1)
    r_data_2 = subregions_data.__getslice__(part_1, part_1 + part_2)
    r_data_3 = subregions_data.__getslice__(part_1 + part_2, count_regions + 1)


    from django.template import defaultfilters
    country_destinations = Country.objects.get_destination_countries()

    country_destinations_data = []

    for cd in country_destinations:
        #ports = cd.port_set.values('slug', 'id', 'name').order_by('name')
        ports = cd.port_set.get_destination_port()
        ports_add = []
        for p in ports:
            port_temp = {'slug': p.slug, 'name': p.name, "id": p.id}
            if p.traveldates_set.count() != 0:
                ports_add.append(port_temp)

        if len(ports_add) > 0:
            temp = {'country_name': cd.name, 'country_slug': defaultfilters.slugify(cd.name),'ports': ports_add}
            country_destinations_data.append(temp)

    count_ports = len(country_destinations_data)
    part_1 = part_2 = part_3 = count_ports/3

    if count_ports%3 == 1:
        part_1 += 1
    elif count_ports%3 == 2:
        part_1 += 1
        part_2 += 1

    port_data_1 = country_destinations_data.__getslice__(0, part_1)
    port_data_2 = country_destinations_data.__getslice__(part_1, part_1 + part_2)
    port_data_3 = country_destinations_data.__getslice__(part_1 + part_2, count_ports + 1)

    countries = Country.objects.values('id','name' ).all()
    return render_to_response('frontend/destinations.html',
                              {'regions': regions_data,
                               'subregions': [r_data_1, r_data_2, r_data_3],
                                'countries': countries,
                                'port_data_1': port_data_1,
                                'port_data_2': port_data_2,
                                'port_data_3': port_data_3,
                               #'destination_ports': destination_ports_data
                              },
                              context_instance = RequestContext(request))

def departures(request):

    departure_ports = Port.objects.get_departure_port()
    departure_ports_data = []
    list_temp = []
    for p in departure_ports:
        if p.traveldates_set.filter(traveldatesport__day_number = 1).count() != 0:
            name = p.name
            if p.country != None:
                name += ', ' + p.country.name
            temp = {'id': p.id, 'name':name, 'slug': p.slug}
            departure_ports_data.append(temp)

    count_d = len(departure_ports_data)
    part_1 = part_2 = part_3 = count_d/3

    if count_d%3 == 1:
        part_1 += 1
    elif count_d%3 == 2:
        part_1 += 1
        part_2 += 1

    port_data_1 = departure_ports_data.__getslice__(0, part_1)
    port_data_2 = departure_ports_data.__getslice__(part_1, part_1 + part_2)
    port_data_3 = departure_ports_data.__getslice__(part_1 + part_2, count_d + 1)

    return render_to_response('frontend/departures.html', {'ports': [port_data_1, port_data_2, port_data_3]}, context_instance = RequestContext(request))

def cruises_lines(request):
    cruise_lines_query = CruiseLine.objects.all()

    cruise_lines_data = []
    list_temp = []
    for cl in cruise_lines_query:
        if TravelDates.objects.get_by_cruise_line_slug(cl.slug).count() != 0:
            temp = {'id': cl.id, 'name': cl.name, 'slug': cl.slug}
            cruise_lines_data.append(temp)

    count_l = len(cruise_lines_data)
    part_1 = part_2 = part_3 = count_l/3

    if count_l%3 == 1:
        part_1 += 1
    elif count_l%3 == 2:
        part_1 += 1
        part_2 += 1

    l_data_1 = cruise_lines_data.__getslice__(0, part_1)
    l_data_2 = cruise_lines_data.__getslice__(part_1, part_1 + part_2)
    l_data_3 = cruise_lines_data.__getslice__(part_1 + part_2, count_l + 1)

    return render_to_response('frontend/cruises_lines.html',
                                    {'cruise_lines': [l_data_1, l_data_2, l_data_3]},
                                    context_instance = RequestContext(request))

def cruise_details(request, slug):

    try:
        cruise_line = CruiseLine.objects.get(slug = slug)
    except CruiseLine.DoesNotExist:
        raise Http404
    cruise_line_data = {'name': cruise_line.name, 'slug': cruise_line.slug}

    ships = Ship.objects.filter(cruise_line = cruise_line).order_by('name')

    ship_data = []

    for s in ships:
        ship_data.append({'slug': s.slug, 'name': s.name, 'small_logo': s.small_logo_src, 'big_logo': s.big_logo_src})

    destinations_data = []

    t_destinations = Travel.objects.values('destination_id').filter(ship__in = ships).order_by('destination__name').annotate(count = Count('destination_id'))

    for d in t_destinations:
        dest = SubRegion.objects.get(pk = d['destination_id'])
        if TravelDates.objects.get_by_cruise_line_slug(slug).get_by_subregion_slug(dest.slug).count() != 0:
            destinations_data.append({'name': dest.name, 'id': dest.id, 'slug': dest.slug})

    departure_port_data = []

    departure_port_query = TravelDates.objects.values('departure_port').filter(travel__ship_id__in = ships, traveldatesport__day_number = 1).order_by('departure_port').annotate(count = Count('departure_port'))

    for dp in departure_port_query:
        try:
            d_temp = Port.objects.get_by_name(dp['departure_port'])
            if d_temp.is_active_depart:
                name = d_temp.name
                if d_temp.country != None:
                    name += ', ' + d_temp.country.name
                departure_port_data.append({'name': name, 'slug': d_temp.slug})
        except Port.DoesNotExist:
            pass

    try:
        dinamic_text = cruise_line.dinamic_text.get(tag='cruiseline_lines')
    except DinamicText.MultipleObjectsReturned:
        dinamic_text = cruise_line.dinamic_text.filter(tag='cruiseline_lines')[0]
    except DinamicText.DoesNotExist:
        dinamic_text = DinamicText()

    params = {'cruise_line': cruise_line_data,'ships': ship_data,'destinations': destinations_data,'departures': departure_port_data,
              'dinamic_text': dinamic_text}

    return render_to_response('frontend/cruises_line_details.html',params,context_instance = RequestContext(request))

def cruise_by_line_subregion(request, cruise_line_slug, subregion):

    travel_dates = TravelDates.objects.get_by_cruise_line_slug(cruise_line_slug).get_by_subregion_slug(subregion)
    daterange = get_daterange(travel_dates)
    travel_dates = travel_dates.values('travel_id').annotate(count=Count('travel_id'))
    travel_dates = _get_sort_result(request, travel_dates)
    #travel_dates = travel_dates.values('travel_id').annotate(count=Count('travel_id'))
    travel_dates = _remove_duplicates(travel_dates)
    try:
        page = request.GET.get('page', 1)
        page = int(page)
    except PageNotAnInteger:
        page = 1
    except ValueError:
        raise Http404

    paginator = PaginatorCustom(travel_dates,OBJECTS_PER_PAGE, request=request)

    try:
        travel_dates_page = paginator.page(page)
        travel_dates_data = travel_obj_to_list(travel_dates_page.object_list)
    except Exception:
        paginator = PaginatorCustom(TravelDates.objects.none(),OBJECTS_PER_PAGE, request=request)
        travel_dates_page = paginator.page(1)
        travel_dates_data = {}

    #daterange = get_daterange(travel_dates_data)

    cruiseline = CruiseLine.objects.get(slug=cruise_line_slug)
    destination = SubRegion.objects.get(slug=subregion)

    title = '{cruiseline} to {destination} in {daterange}'.format(cruiseline=cruiseline.name,
                                                        destination=destination.alias, daterange=", ".join(daterange))
    meta = ['']*10
    meta[0] = "<meta property='og:title' content='{cruiseline} to {destination} in {daterange}'>".format(cruiseline=cruiseline.name,
                                                        destination=destination.alias, daterange=", ".join(daterange))
    meta[1] = "<meta property='og:description' content='View a list of cruises with {cruiseline} to {destination}. " \
            "See all upcoming cruises visiting {destination} with {cruiseline}.' > ".format(cruiseline=cruiseline.name,
                                                        destination=destination.alias)
    meta[2] = "<meta property='og:keywords' content='{cruiseline} to {destination} in {daterange}'>".format(cruiseline=cruiseline.name,
                                                        destination=destination.alias, daterange=", ".join(daterange))
    meta[3] = "<meta property='og:copyright' content='Copyright 2015, Red Hot Cruises'>"
    meta[4] = "<meta property='og:image' content='/static/logo.png'>"
    meta[5] = "<meta name='description' content='View a list of cruises with {cruiseline} to {destination}. " \
            "See all upcoming cruises visiting {destination} with {cruiseline}.'>".format(cruiseline=cruiseline.name,
                                                        destination=destination.alias)
    meta[6] = "<meta name='keywords' content='{cruiseline} to {destination} in {daterange}'>".format(cruiseline=cruiseline.name,
                                                        destination=destination.alias, daterange=", ".join(daterange))
    meta[7] = "<meta name='robots' content='index, follow'>"
    meta[8] = "<meta name='googlebot' content='index, follow'>"
    meta[9] = "<meta name='msnbot' content='index, follow'>"

    try:
       dinamic_text = destination.dinamic_text.get(tag='subregion_cruiseline',slug=cruiseline.slug)
    except DinamicText.MultipleObjectsReturned:
       dinamic_text = destination.dinamic_text.filter(tag='subregion_cruiseline').filter(slug=cruiseline.slug)[0]
    except DinamicText.DoesNotExist:
        dinamic_text = DinamicText()

    params = {'travels': travel_dates_data, 'pager': travel_dates_page, 'title': title, 'metadata': ''.join(meta),
              'has_text': True, 'text_title': 'Cruises to {destination} with {cruiseline}'.format(cruiseline=cruiseline.name,
                                                        destination=destination.alias), 'dinamic_text': dinamic_text}

    return render_to_response('frontend/search_result.html',params,context_instance = RequestContext(request))

def cruise_by_line_ship(request, cruise_line_slug, ship_slug):

    travel_dates = TravelDates.objects.get_by_ship_slug(ship_slug)
    daterange = get_daterange(travel_dates)
    travel_dates = travel_dates.values('travel_id').annotate(count=Count('travel_id'))
    travel_dates = _get_sort_result(request, travel_dates)
    #travel_dates = travel_dates.values('travel_id').annotate(count=Count('travel_id'))
    travel_dates = _remove_duplicates(travel_dates)

    try:
        page = request.GET.get('page', 1)
        page = int(page)
    except PageNotAnInteger:
        page = 1
    except ValueError:
        raise Http404

    paginator = PaginatorCustom(travel_dates,OBJECTS_PER_PAGE, request=request)

    try:
        travel_dates_page = paginator.page(page)
        travel_dates_data = travel_obj_to_list(travel_dates_page.object_list)
    except Exception:
        paginator = PaginatorCustom(TravelDates.objects.none(),OBJECTS_PER_PAGE, request=request)
        travel_dates_page = paginator.page(1)
        travel_dates_data = {}
    #daterange = get_daterange(travel_dates_data)

    try:
        cruiseline = CruiseLine.objects.get(slug=cruise_line_slug)
        ship = Ship.objects.get(slug=ship_slug)
    except Ship.DoesNotExist, CruiseLine.DoesNotExist:
        raise Http404
    except Ship.MultipleObjectsReturned:
        ship = Ship.objects.filter(slug=ship_slug)[0]
    except CruiseLine.MultipleObjectsReturned:
         cruiseline = CruiseLine.objects.filter(slug=cruise_line_slug)[0]

    title = '{ship} Cruises with {cruiseline} in {daterange}'.format(cruiseline=cruiseline.name,
                                                        ship=ship.name, daterange=", ".join(daterange))
    meta = ['']*10
    meta[0] = "<meta property='og:title' content='{ship} Cruises with {cruiseline} in {daterange}'>".format(cruiseline=cruiseline.name,
                                                        ship=ship.name, daterange=", ".join(daterange))
    meta[1] = "<meta property='og:description' content='View a list of {ship} cruises with {cruiseline}. See all " \
              "upcoming cruises aboard the {ship} with {cruiseline}.' > ".format(cruiseline=cruiseline.name, ship=ship.name)
    meta[2] = "<meta property='og:keywords' content='{ship} Cruises with {cruiseline} in {daterange}'>".format(cruiseline=cruiseline.name,
                                                        ship=ship.name, daterange=", ".join(daterange))
    meta[3] = "<meta property='og:copyright' content='Copyright 2015, Red Hot Cruises'>"
    meta[4] = "<meta property='og:image' content='/static/logo.png'>"
    meta[5] = "<meta name='description' content='View a list of {ship} cruises with {cruiseline}. See all upcoming " \
              "cruises aboard the {ship} with {cruiseline}.'>".format(cruiseline=cruiseline.name,ship=ship.name)
    meta[6] = "<meta name='keywords' content='{ship} Cruises with {cruiseline} in {daterange}'>".format(cruiseline=cruiseline.name,
                                                        ship=ship.name, daterange=", ".join(daterange))
    meta[7] = "<meta name='robots' content='index, follow'>"
    meta[8] = "<meta name='googlebot' content='index, follow'>"
    meta[9] = "<meta name='msnbot' content='index, follow'>"

    try:
       dinamic_text = ship.dinamic_text.get(tag='ship_cruiseline')
    except DinamicText.MultipleObjectsReturned:
       dinamic_text = ship.dinamic_text.filter(tag='ship_cruiseline')[0]
    except DinamicText.DoesNotExist:
        dinamic_text = DinamicText()

    params = {'travels': travel_dates_data, 'pager': travel_dates_page, 'title': title, 'metadata': ''.join(meta),
              'has_text': True, 'text_title': '{ship} Cruises with {cruiseline}'.format(cruiseline=cruiseline.name,
                                                        ship=ship.name), 'dinamic_text': dinamic_text}

    return render_to_response('frontend/search_result.html',params,context_instance = RequestContext(request))

def cruise_by_line_departure(request, cruise_line_slug, departure_port_slug):

    travel_dates = TravelDates.objects.get_by_cruise_line_slug(cruise_line_slug).\
        get_by_departure_port_slug(departure_port_slug)
    daterange = get_daterange(travel_dates)
    travel_dates = travel_dates.values('travel_id').annotate(count=Count('travel_id'))
    travel_dates = _get_sort_result(request, travel_dates)
    travel_dates = _remove_duplicates(travel_dates)

    try:
        page = request.GET.get('page', 1)
        page = int(page)
    except PageNotAnInteger:
        page = 1
    except ValueError:
        raise Http404

    paginator = PaginatorCustom(travel_dates,OBJECTS_PER_PAGE, request=request)

    try:
        travel_dates_page = paginator.page(page)
        travel_dates_data = travel_obj_to_list(travel_dates_page.object_list)
    except Exception:
        paginator = PaginatorCustom(TravelDates.objects.none(),OBJECTS_PER_PAGE, request=request)
        travel_dates_page = paginator.page(1)
        travel_dates_data = {}


    #daterange = get_daterange(travel_dates_data)

    cruiseline = CruiseLine.objects.get(slug=cruise_line_slug)
    port = Port.objects.get(slug=departure_port_slug)

    title = '{cruiseline} from {departure} in {daterange}'.format(cruiseline=cruiseline.name,
                                                        departure=port.alias, daterange=", ".join(daterange))
    meta = ['']*10
    meta[0] = "<meta property='og:title' content='{cruiseline} from {departure} in {daterange}'>".format(cruiseline=cruiseline.name,
                                                        departure=port.alias, daterange=", ".join(daterange))
    meta[1] = "<meta property='og:description' content='View a list of cruises with {cruiseline} from {departure}. " \
            "See all upcoming cruises visiting {departure} with {cruiseline}.' > ".format(cruiseline=cruiseline.name,
                                                        departure=port.alias)
    meta[2] = "<meta property='og:keywords' content='{cruiseline} from {departure} in {daterange}'>".format(cruiseline=cruiseline.name,
                                                        departure=port.alias, daterange=", ".join(daterange))
    meta[3] = "<meta property='og:copyright' content='Copyright 2015, Red Hot Cruises'>"
    meta[4] = "<meta property='og:image' content='/static/logo.png'>"
    meta[5] = "<meta name='description' content='View a list of cruises with {cruiseline} from {departure}. See all " \
              "upcoming cruises leaving from {departure} with {cruiseline}.'>".format(cruiseline=cruiseline.name,
                                                                                      departure=port.alias)
    meta[6] = "<meta name='keywords' content='{cruiseline} from {departure} in {daterange}'>".format(cruiseline=cruiseline.name,
                                                        departure=port.alias, daterange=", ".join(daterange))
    meta[7] = "<meta name='robots' content='index, follow'>"
    meta[8] = "<meta name='googlebot' content='index, follow'>"
    meta[9] = "<meta name='msnbot' content='index, follow'>"

    try:
        dinamic_text = port.dinamic_text.get(tag='port_cruiseline',slug=cruiseline.slug)
    except DinamicText.MultipleObjectsReturned:
       dinamic_text = port.dinamic_text.filter(tag='port_cruiseline').filter(slug=cruiseline.slug)[0]
    except DinamicText.DoesNotExist:
        dinamic_text = DinamicText()

    params = {'travels': travel_dates_data, 'pager': travel_dates_page, 'title': title, 'metadata': ''.join(meta),
              'has_text': True, 'text_title': 'Cruises from {departure} with {cruiseline}'.format(cruiseline=cruiseline.name,
                                                        departure=port.alias), 'dinamic_text': dinamic_text}

    return render_to_response('frontend/search_result.html',params,context_instance = RequestContext(request))

def cruise_by_destination(request, type, destination):

    if type == 'subregion':
        travel_dates = TravelDates.objects.get_by_subregion_slug(destination).order_by('depart_date')
        daterange = get_daterange(travel_dates)
        travel_dates = travel_dates.values('travel_id').annotate(count=Count('travel_id'))
    elif type == 'region':
        travel_dates = TravelDates.objects.get_by_region_slug(destination).order_by('depart_date')
        daterange = get_daterange(travel_dates)
        travel_dates = travel_dates.values('travel_id').annotate(count=Count('travel_id'))
    travel_dates = _get_sort_result(request, travel_dates)
    try:
        page = request.GET.get('page', 1)
        page = int(page)
    except PageNotAnInteger:
        page = 1
    except ValueError:
        raise Http404

    paginator = PaginatorCustom(travel_dates,OBJECTS_PER_PAGE, request=request)

    try:
        travel_dates_page = paginator.page(page)
        travel_dates_data = travel_obj_to_list(travel_dates_page.object_list)
    except Exception:
        paginator = PaginatorCustom(TravelDates.objects.none(),OBJECTS_PER_PAGE, request=request)
        travel_dates_page = paginator.page(1)
        travel_dates_data = {}

    subregion = SubRegion.objects.get(slug=destination)
    #daterange = get_daterange(travel_dates_data)

    title = 'Cruises to {destination} in {daterange}'.format(destination=subregion.alias, daterange=", ".join(daterange))
    meta = ['']*10
    meta[0] = "<meta property='og:title' content='Cruises to {destination} in {daterange}'>".format(destination=subregion.alias, daterange=", ".join(daterange))
    meta[1] = "<meta property='og:description' content='View a complete list of cruises to {destination}. See all " \
              "upcoming cruises visiting {destination} at Red Hot Cruises.' > ".format(destination=subregion.alias)
    meta[2] = "<meta property='og:keywords' content='Cruises to {destination} in {daterange}'>".format(destination=subregion.alias, daterange=", ".join(daterange))
    meta[3] = "<meta property='og:copyright' content='Copyright 2015, Red Hot Cruises'>"
    meta[4] = "<meta property='og:image' content='/static/logo.png'>"
    meta[5] = "<meta name='description' content='View a complete list of cruises to {destination}. See all upcoming " \
              "cruises visiting {destination} at Red Hot Cruises.'>".format(destination=subregion.alias)
    meta[6] = "<meta name='keywords' content='Cruises to {destination} in {daterange}'>".format(destination=subregion.alias,
                                                                                                daterange=", ".join(daterange))
    meta[7] = "<meta name='robots' content='index, follow'>"
    meta[8] = "<meta name='googlebot' content='index, follow'>"
    meta[9] = "<meta name='msnbot' content='index, follow'>"

    try:
        dinamic_text = subregion.dinamic_text.get(tag='subregion_destination')
    except DinamicText.MultipleObjectsReturned:
       dinamic_text = subregion.dinamic_text.filter(tag='subregion_destination')[0]
    except DinamicText.DoesNotExist:
        dinamic_text = DinamicText()

    params = {'travels': travel_dates_data, 'pager': travel_dates_page, 'title': title, 'metadata': ''.join(meta), 'has_text': True,
              'text_title': 'Cruises to {destination}'.format(destination=subregion.alias), 'dinamic_text': dinamic_text}

    return render_to_response('frontend/search_result.html',params, context_instance = RequestContext(request))

def cruise_by_destination_port(request, destination_port):

    travel_dates = TravelDates.objects.get_by_destination_port(destination_port).order_by('depart_date')
    daterange = get_daterange(travel_dates)
    travel_dates = travel_dates.values('travel_id').annotate(count=Count('travel_id'))
    travel_dates = _get_sort_result(request, travel_dates)
    try:
        page = request.GET.get('page', 1)
        page = int(page)
    except PageNotAnInteger:
        page = 1
    except ValueError:
        raise Http404

    paginator = PaginatorCustom(travel_dates,OBJECTS_PER_PAGE, request=request)

    try:
        travel_dates_page = paginator.page(page)
        travel_dates_data = travel_obj_to_list(travel_dates_page.object_list)
    except Exception:
        paginator = PaginatorCustom(TravelDates.objects.none(),OBJECTS_PER_PAGE, request=request)
        travel_dates_page = paginator.page(1)
        travel_dates_data = {}

    port = Port.objects.get(slug=destination_port)
    #daterange = get_daterange(TravelDates.objects.get_by_destination_port(destination_port).order_by('depart_date'))

    title = 'Cruises to {destination}, {country} in {daterange}'.format(destination=port.alias, country = port.country.name, daterange=", ".join(daterange))
    meta = ['']*10
    meta[0] = "<meta property='og:title' content='Cruises to {destination}, {country} in {daterange}'>".format(destination=port.alias, country=port.country.name,daterange=", ".join(daterange))
    meta[1] = "<meta property='og:description' content='View a complete list of cruises to {destination}, {country}. See all " \
              "upcoming cruises visiting {destination} at Red Hot Cruises.' > ".format(destination=port.alias, country = port.country.name)
    meta[2] = "<meta property='og:keywords' content='Cruises to {destination}, {country} in {daterange}'>".format(destination=port.alias, country = port.country.name, daterange=", ".join(daterange))
    meta[3] = "<meta property='og:copyright' content='Copyright 2015, Red Hot Cruises'>"
    meta[4] = "<meta property='og:image' content='/static/logo.png'>"
    meta[5] = "<meta name='description' content='View a complete list of cruises to {destination}, {country}. See all upcoming " \
              "cruises visiting {destination} at Red Hot Cruises.'>".format(destination=port.alias, country = port.country.name)
    meta[6] = "<meta name='keywords' content='Cruises to {destination}, {country} in {daterange}'>".format(destination=port.alias,
                                                                                                        country = port.country.name,
                                                                                                daterange=", ".join(daterange))
    meta[7] = "<meta name='robots' content='index, follow'>"
    meta[8] = "<meta name='googlebot' content='index, follow'>"
    meta[9] = "<meta name='msnbot' content='index, follow'>"

    try:
        dinamic_text = port.dinamic_text.get(tag='port_destination')
    except DinamicText.MultipleObjectsReturned:
       dinamic_text = port.dinamic_text.filter(tag='port_destination')[0]
    except DinamicText.DoesNotExist:
        dinamic_text = DinamicText()

    params = {'travels': travel_dates_data, 'pager': travel_dates_page, 'title': title, 'metadata': ''.join(meta),
              'has_text': True,'text_title': 'Cruises to {port}, {country}'.format(port=port.alias, country = port.country.name), 'dinamic_text': dinamic_text}

    return render_to_response('frontend/search_result.html',params,context_instance = RequestContext(request))

def cruise_by_departure_port(request, departure_port_slug):

    travel_dates = TravelDates.objects.get_by_departure_port_slug(departure_port_slug)
    daterange = get_daterange(travel_dates)
    travel_dates = travel_dates.values('travel_id').annotate(count=Count('travel_id'))
    travel_dates = _get_sort_result(request, travel_dates)
    travel_dates = _remove_duplicates(travel_dates)
    try:
        page = request.GET.get('page', 1)
        page = int(page)
    except PageNotAnInteger:
        page = 1
    except ValueError:
        raise Http404


    paginator = PaginatorCustom(travel_dates,OBJECTS_PER_PAGE, request=request)

    try:
        travel_dates_page = paginator.page(page)
        travel_dates_data = travel_obj_to_list(travel_dates_page.object_list)
    except Exception:
        paginator = PaginatorCustom(TravelDates.objects.none(),OBJECTS_PER_PAGE, request=request)
        travel_dates_page = paginator.page(1)
        travel_dates_data = {}

    port = Port.objects.get(slug=departure_port_slug)
    #daterange = get_daterange(travel_dates_data)

    canonical = '<link rel="canonical" href="{sheme}://redhotcruises.com{path}" />'.format(sheme=request.scheme,path=request.path)
    # if page != 1:
    #     if page == 2:
    #         canonical = '<link rel="next" href="{sheme}://redhotcruises.com{path}" />'.format(sheme=request.scheme,path=request.path)
    #     elif page == paginator.num_pages:
    #         canonical = '<link rel="prev" href="{sheme}://redhotcruises.com{path}"'.format(sheme=request.scheme,path=request.path)
    #     else:
    #         temp = ['']*2
    #         temp[0] = '<link rel="prev" href="{sheme}://redhotcruises.com{path}?page={page}" />'.format(sheme=request.scheme,path=request.path,page=page-1)
    #         temp[1] = '<link rel="next" href="{sheme}://redhotcruises.com{path}?page={page}" />'.format(sheme=request.scheme,path=request.path,page=page+1)
    #         canonical = ''.join(temp)

    title = 'Cruises departing from {departure} port in {daterange}'.format(departure=port.alias, country = port.country.name, daterange=", ".join(daterange))
    meta = ['']*10
    meta[0] = "<meta property='og:title' content='Cruises departing from {departure} port in {daterange}'>".format(departure=port.alias, daterange=", ".join(daterange))
    meta[1] = "<meta property='og:description' content='View a complete list of cruises departing from {departure} port. See all " \
              "upcoming cruises leaving the port of {departure} at Red Hot Cruises.' > ".format(departure=port.alias)
    meta[2] = "<meta property='og:keywords' content='Cruises from {departure}, {country} in {daterange}'>".format(departure=port.alias, country = port.country.name, daterange=", ".join(daterange))
    meta[3] = "<meta property='og:copyright' content='Copyright 2015, Red Hot Cruises'>"
    meta[4] = "<meta property='og:image' content='/static/logo.png'>"
    meta[5] = "<meta name='description' content='View a complete list of cruises from {departure}, {country}. See all upcoming " \
              "cruises leaving {departure} at Red Hot Cruises.'>".format(departure=port.alias, country = port.country.name)
    meta[6] = "<meta name='keywords' content='Cruises from {departure}, {country} in {daterange}'>".format(departure=port.alias, country = port.country.name,
                                                                                                daterange=", ".join(daterange))
    meta[7] = "<meta name='robots' content='index, follow'>"
    meta[8] = "<meta name='googlebot' content='index, follow'>"
    meta[9] = "<meta name='msnbot' content='index, follow'>"

    try:
        dinamic_text = port.dinamic_text.get(tag='port_departure')
    except DinamicText.MultipleObjectsReturned:
       dinamic_text = port.dinamic_text.filter(tag='port_departure')[0]
    except DinamicText.DoesNotExist:
        dinamic_text = DinamicText()

    params = {'travels': travel_dates_data, 'pager': travel_dates_page, 'title': title, 'metadata': ''.join(meta),
              'has_text': True,'text_title': 'Cruises departing from {port}, {country}'.format(port=port.alias, country = port.country.name),
              'dinamic_text': dinamic_text,'canonical':canonical}

    return render_to_response('frontend/search_result.html',params,context_instance = RequestContext(request))

def travels_pricing(request):
    if request.GET.has_key('t_unique_id'):
        travel_date_id = request.GET['t_unique_id']
        travel_dates_query = TravelDates.objects.filter(travel_id = travel_date_id).order_by('depart_date')
        inside_price_min = travel_dates_query.aggregate(Min('inside_price'))['inside_price__min']
        ocean_price_min = travel_dates_query.aggregate(Min('ocean_price'))['ocean_price__min']
        balcony_price_min = travel_dates_query.aggregate(Min('balcony_price'))['balcony_price__min']
        suite_price_min = travel_dates_query.aggregate(Min('suite_price'))['suite_price__min']

        travel_dates_data = []
        for td in travel_dates_query:
            travel_temp_dict = {
                'id': td.id,
                'depart_date': td.depart_date,
                'inside': td.inside_price,
                'oceanview': td.ocean_price,
                'balcony': td.balcony_price,
                'suite': td.suite_price,
                'is_inside_min_price': False,
                'is_ocean_min_price': False,
                'is_balcony_min_price': False,
                'is_suite_min_price': False,
                }
            if td.inside_price == inside_price_min and inside_price_min is not None:
                travel_temp_dict['is_inside_min_price'] = True

            if td.balcony_price == balcony_price_min and balcony_price_min is not None:
                travel_temp_dict['is_balcony_min_price'] = True

            if td.ocean_price == ocean_price_min and ocean_price_min is not None:
                travel_temp_dict['is_ocean_min_price'] = True

            if td.suite_price == suite_price_min and suite_price_min is not None:
                travel_temp_dict['is_suite_min_price'] = True


            travel_dates_data.append(travel_temp_dict)
        return render_to_response('frontend/pricing_dates.html',
                                  {'pricing': travel_dates_data},
                                   context_instance = RequestContext(request))

def get_ships_by_cruiseline(request):
    if request.GET.has_key('cruise_line_id'):
        cruise_line_id =  request.GET['cruise_line_id']
        ships_query = Ship.objects.filter(cruise_line_id = cruise_line_id)
        ships_arr = []
        for ship in ships_query:
            ships_arr.append({'id': ship.id, 'name': ship.name})
        json_result = json.dumps(ships_arr)

        return HttpResponse(json_result, content_type='application/json')
    else:
        return HttpResponse(json.dumps({}), content_type='application/json')



@csrf_exempt
def search_cruises(request, **kwargs):
    travel_dates = TravelDates.objects.all()
    if request.GET.has_key('date'):
        date =  request.GET['date'] + '-01'
        try:
            import datetime
            date = datetime.datetime.strptime(date, '%Y-%m-%d')
        except Exception, e:
            raise Http404
        travel_dates = travel_dates.get_by_date(date)
    elif len(request.GET) == 0 or (len(request.GET) == 1 and (request.GET.has_key('sort') or request.GET.has_key('page'))):
        #travel_dates = travel_dates.get_travels_current_month()
        pass

    if request.GET.has_key('length'):
        length = request.GET['length']
        length_arr = length.split('-')
        if len(length_arr) == 2:
            min_length = length_arr[0]
            max_length = length_arr[1]
            travel_dates = travel_dates.get_by_length_range(min_length, max_length)
        else:
            length_arr = length.split('+')
            min_length = length_arr[0]
            travel_dates = travel_dates.get_by_length_range(min_length)

    if request.GET.has_key('destination'):
        destination = request.GET['destination']
        travel_dates = travel_dates.get_by_subregion_alias(destination)

    if request.GET.has_key('cruiseLine'):
        cruise_line = request.GET['cruiseLine']
        travel_dates = travel_dates.get_by_cruise_line_name(cruise_line)

    if request.GET.has_key('ship'):
        ship = request.GET['ship']
        travel_dates = travel_dates.get_by_ship_name(ship)

    if request.GET.has_key('departure'):
        departure = request.GET['departure']
        travel_dates = travel_dates.get_by_departure_port_slug(departure)

    if request.GET.has_key('q'):
        from django.db.models import Q
        query_text = request.GET['q']
        travel_dates = travel_dates.filter(Q(travel__name__icontains = query_text)
        | Q(destination_port__name__icontains = query_text) | Q(travel__ship__name__icontains = query_text)
        | Q(travel__ship__cruise_line__name__icontains = query_text) | Q(departure_port__icontains = query_text))


    try:
        page = request.GET.get('page', 1)
        page = int(page)
    except PageNotAnInteger:
        page = 1
    except ValueError:
        raise Http404

    travel_dates = travel_dates.values('travel_id').annotate(count=Count('travel_id'))
    travel_dates = _get_sort_result(request, travel_dates)


    paginator = PaginatorCustom(travel_dates,OBJECTS_PER_PAGE, request=request)

    try:
        travel_dates_page = paginator.page(page)
        travel_dates_data = travel_obj_to_list(travel_dates_page.object_list)
    except Exception:
        paginator = PaginatorCustom(TravelDates.objects.none(),OBJECTS_PER_PAGE, request=request)
        travel_dates_page = paginator.page(1)
        travel_dates_data = {}

    title = 'Search for Cruises - Red Hot Cruises'
    meta = ['']*10
    meta[0] = """<meta property='og:title' content="Search for Cruises - Red Hot Cruises">"""
    meta[1] = """<meta property='og:description' content="Search for Cruises - Red Hot Cruises">"""
    meta[2] = """<meta property='og:keywords' content="Search for Cruises - Red Hot Cruises">"""
    meta[3] = "<meta property='og:copyright' content='Copyright 2015, Red Hot Cruises'>"
    meta[4] = "<meta property='og:image' content='/static/logo.png'>"
    meta[5] = """<meta name='description' content="Search for Cruises - Red Hot Cruises">"""
    meta[6] = """<meta name='keywords' content="Search for Cruises - Red Hot Cruises">"""
    meta[7] = """<meta name='robots' content="noindex, nofollow">"""
    meta[8] = "<meta name='googlebot' content='index, follow'>"
    meta[9] = "<meta name='msnbot' content='index, follow'>"

    params = {'travels': travel_dates_data, 'pager': travel_dates_page, 'title': title, 'metadata': ''.join(meta)}

    return render_to_response('frontend/search_result.html',params,context_instance = RequestContext(request))

def subscribe(request):
    if request.POST.has_key('email'):
        email = request.POST['email']
        try:
            Subscriptions.objects.get(email = email)
            data = json.dumps({'success': 0})
        except Subscriptions.DoesNotExist:
            Subscriptions.objects.create(email=email)
            data = json.dumps({'success': 1})
        return HttpResponse(data, content_type='application/json')
    else:
        raise Http404


def cruise(request, cruiseid):
    travel = TravelDates.objects.get(pk=cruiseid)
    from django.shortcuts import redirect
    return redirect(travel.url)

def send_email(request):

    if request.method == 'POST':
        name = request.POST['name']
        email = request.POST['email']
        comment = request.POST['comment']
        send_mail('Customer Answer', comment, email, [settings.EMAIL_HOST_USER],fail_silently=False)
    return redirect(request.META['HTTP_REFERER'])

def testing(request):
    import urllib2
    doc = urllib2.urlopen('http://127.0.0.1:8000/sitemap.xml')
    import xml.etree.ElementTree as ET
    root = ET.parse(doc).getroot()

    return HttpResponse(len(root))


def testing2(request):
    cruise_lines_query = CruiseLine.objects.all()

    cruise_lines_data = []
    for cl in cruise_lines_query:
        if TravelDates.objects.get_by_cruise_line_slug(cl.slug).count() != 0:
            cruise_lines_data.append(cl)

    total_ships = 0
    total_destinations = 0
    total_departures = 0
    total_cruiselines = len(cruise_lines_data)
    for cruise_line in cruise_lines_data:

        ships = Ship.objects.filter(cruise_line = cruise_line).order_by('name')

        ship_data = []

        for s in ships:
            ship_data.append({'slug': s.slug, 'name': s.name, 'small_logo': s.small_logo_src, 'big_logo': s.big_logo_src})
        total_ships += len(ship_data)
        destinations_data = []

        t_destinations = Travel.objects.values('destination_id').filter(ship__in = ships).order_by('destination__name').annotate(count = Count('destination_id'))

        for d in t_destinations:
            dest = SubRegion.objects.get(pk = d['destination_id'])
            if TravelDates.objects.get_by_cruise_line_slug(cruise_line.slug).get_by_subregion_slug(dest.slug).count() != 0:
                destinations_data.append({'name': dest.name, 'id': dest.id, 'slug': dest.slug})
        total_destinations += len(destinations_data)
        departure_port_data = []

        departure_port_query = TravelDates.objects.values('departure_port').filter(travel__ship_id__in = ships).filter(traveldatesport__day_number = 1).order_by('departure_port').annotate(count = Count('departure_port'))


        for dp in departure_port_query:
            try:
                d_temp = Port.objects.get_by_name(dp['departure_port'])
                if d_temp.is_active_depart:
                    name = d_temp.name
                    if d_temp.country != None:
                        name += ', ' + d_temp.country.name
                    departure_port_data.append({'name': name, 'slug': d_temp.slug})
            except Port.DoesNotExist:
                pass
        total_departures += len(departure_port_data)

    return HttpResponse('ships:'+str(total_ships)+' destinations:'+str(total_destinations)+' departures:'+str(total_departures)+' cruiseslines:'+str(total_cruiselines))



def custom_article(request, cruise_line_slug='viking-cruises', departure_port_slug='budapest-hungary'):

    filter_ports=[u'Budapest', u'Vienna', u'Krems', u'Passau', u'Regensburg', u'Nuremberg']
    travel_dates = TravelDates.objects.get_by_cruise_line_slug(cruise_line_slug).\
        get_by_departure_port_slug(departure_port_slug).filter(visited_ports__name__in = filter_ports)
    daterange = get_daterange(travel_dates)
    travel_dates = travel_dates.values('travel_id').annotate(count=Count('travel_id'))

    travel_dates = _get_sort_result(request, travel_dates)
    try:
        page = request.GET.get('page', 1)
        page = int(page)
    except PageNotAnInteger:
        page = 1
    except ValueError:
        raise Http404

    travel_dates_data = travel_obj_to_list_custom(travel_dates,filter_ports)

    # paginator = PaginatorCustom(travel_dates,OBJECTS_PER_PAGE, request=request)
    #
    # try:
    #     travel_dates_page = paginator.page(page)
    #     travel_dates_data = travel_obj_to_list(travel_dates_page.object_list)
    # except Exception:
    #     paginator = PaginatorCustom(TravelDates.objects.none(),OBJECTS_PER_PAGE, request=request)
    #     travel_dates_page = paginator.page(1)
    #     travel_dates_data = {}


    #daterange = get_daterange(travel_dates_data)

    cruiseline = CruiseLine.objects.get(slug=cruise_line_slug)
    port = Port.objects.get(slug=departure_port_slug)

    title = '"Romantic Danube" with Viking River Cruises'
    description = 'Embark on the "Romantic Danube" with Viking River Cruises commencing in March 2016. See all available ' \
                  'dates and ships for the "Romantic Danube" departing from Budapest.'
    meta = ['']*10
    meta[0] = "<meta property='og:title' content='{title}'>".format(title=title)
    meta[1] = "<meta property='og:description' content='{description}'> ".format(description=description)
    meta[2] = "<meta property='og:keywords' content='{cruiseline} from {departure} in {daterange}'>".format(cruiseline=cruiseline.name,
                                                        departure=port.alias, daterange=", ".join(daterange))
    meta[3] = "<meta property='og:copyright' content='Copyright 2015, Red Hot Cruises'>"
    meta[4] = "<meta property='og:image' content='/static/logo.png'>"
    meta[5] = "<meta name='description' content='{description}'>".format(description=description)
    meta[6] = "<meta name='keywords' content='{cruiseline} from {departure} in {daterange}'>".format(cruiseline=cruiseline.name,
                                                        departure=port.alias, daterange=", ".join(daterange))
    meta[7] = "<meta name='robots' content='index, follow'>"
    meta[8] = "<meta name='googlebot' content='index, follow'>"
    meta[9] = "<meta name='msnbot' content='index, follow'>"


    params = {'travels': travel_dates_data, 'title': title, 'metadata': ''.join(meta),
              'has_text': True, 'text_title': 'Cruises from {departure} with {cruiseline}'.format(cruiseline=cruiseline.name,
                                                        departure=port.alias)}

    return render_to_response('frontend/search_custom.html',params,context_instance = RequestContext(request))

