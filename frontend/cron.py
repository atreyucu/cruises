# -*- coding: latin-1 -*-
import random
import cronjobs
from django.db.models import Count
from backend.models import SubRegion, CruiseLine, DinamicText, Port, Travel, Ship, TravelDates

PHONE = '<a style="color: #fff" href="tel:8442354177">(844)235-4177</a>'
EMAIL = '<a style="color: #fff" href="mailto:info@redhotcruises.com">email us</a>'

#@cronjobs.register
def destination_text():
    print 'generating destination_text'
    option1 = ['has become', 'has always been', 'has proven to be', 'has developed into', 'has made itself into',
               'is known as', 'is definitely']
    option2 = ['top', 'highly sought-after', 'popular', 'must-see', 'featured', 'beloved']
    option3 = ['heading out', 'embarking', 'traveling', 'going']
    option4 = ['cruise', 'vacation', 'getaway']
    option5 = ['Featuring', 'Complete with', 'Bursting with']
    option6 = ['beautiful', 'spectacular', 'magnificent', 'stunning', 'exquisite']
    option7 = ['scenery', 'landscapes', 'views', 'ambiance']
    option8 = ['interesting', 'fascinating', 'captivating', 'inviting']
    option9 = ['charismatic', 'charming', 'delightful', 'lovable', 'enchanting']
    option10 = ['offers', 'provides', 'has', 'includes', 'contains']
    option11 = ['visitors', 'guests', 'newcomers', 'vacationers']
    option12 = ['pick from', 'choose from', 'arrange', 'plan', 'schedule']
    option13 = ['lots', 'a wide variety', 'an assortment', 'a wide range']
    option14 = ['trips', 'excursions', 'adventures', 'ventures']
    option15 = ['better', 'fully', 'further']
    option16 = ['see', 'witness', 'experience', 'enjoy', 'take part in', 'appreciate']
    option17 = ['Red Hot Cruises', 'RedHotCruises', 'RedHotCruises.com']
    option18 = ['offer', 'provide you with', 'have', 'allow you to choose from']
    option19 = ['a huge assortment', 'one of the largest selections', 'many types']
    option20 = ['aboard', 'with', 'through', 'via']
    option21 = ['world’s', 'industry’s']
    option22 = ['best', 'most beloved', 'most trusted', 'most popular', 'premier', 'most well-known', 'top',
                'most well-respected']
    option23 = ['companies', 'lines', 'providers', 'hosts', 'leaders']
    option49 = ['including', 'like', 'featuring']

    text1 = """
    {destination} {option1} a {option2} destination when {option3} on a {option4}. {option5} {option6} {option7}, {option8}
    cultures and {option9} people, {destination} {option10} something for everyone. With all cruises to {destination}, {option11}
    can {option12} {option13} of activities and shore {option14} to {option15} {option16} what {destination} has to offer.
    Here at {option17}, we {option18} {option19} of cruises to {destination} {option20} some of the {option21} {option22}
    cruise {option23} {option49}: """

    option24 = ['View','Check out','Browse','See']
    option48 = ['complete','full','entire','detailed']
    option25 = ['list','lineup','schedule','listing','catalog','calendar']
    option26 = ['available','possible','upcoming','forthcoming']
    #sort_by = ['date range,cruise line,length,ship,departure port']
    option27 = ['Once choosing','When you find','Once you see','When choosing','When selecting']
    option28 = ['perfect','ideal','best']
    option29 = ['view','check out','browse','see']
    option30 = ['more','additional','further']
    option31 = ['specifics','information']
    option32 = ['complete','full','entire','detailed']
    option33 = ['schedule','itinerary','agenda']
    option34 = ['ship','vessel','boat','yacht','sailing']
    option35 = ['a list of','the available','some featured']
    option36 = ['trips','excursions','adventures','ventures']
    option37 = ['Vacancy','Availability','Space']
    option38 = ['very','extremely','usually','often']
    option39 = ['purchase','book','buy','reserve','order']
    option40 = ['now','today','as soon as possible','quickly','without delay']
    option41 = ['ensure','lock in','grab']
    option42 = ['it’s too late','it sells out completely','they are gone','waiting too long']
    option43 = ['Give us a call','Call us','Get a hold of us','Contact us']
    option44 = ['speak','talk','consult']
    option45 = ['cruise','vacation']
    option46 = ['professional','specialist','expert','pro']
    option47 = ['guide you in the right direction','answer any questions you have','help you find what you’re looking for','find the perfect cruise for you']

    text2 = """
    {option24} our {option48} {option25} of all {option26} cruises to {destination} and sort by {sort_by}.
    {option27} the {option28} cruise, you can {option29} {option30} {option31} and the {option32} cruise {option33} as well
    as the {option34} details and {option35} shore {option36} in {destination}. {option37} is {option38} limited, so
    {option39} online with us {option40} to {option41} your {destination} cruise reservation before {option42}. {option43} at
    {phone} or {email} to {option44} with a {option45} {option46} and we will {option47}."""

    subregions = SubRegion.objects.all()
    for subregion in subregions:
        print 'generating subregions'
        try:
            dinamic_text = subregion.dinamic_text.get(tag='subregion_destination')
        except DinamicText.DoesNotExist:
            dinamic_text = DinamicText()
        except DinamicText.MultipleObjectsReturned:
            dinamic_text = subregion.dinamic_text.filter(tag='subregion_destination')[0]
        dinamic_text.tag = 'subregion_destination'
        dinamic_text.content_object = subregion
        dinamic_text.first_text = text1.format(destination=subregion.alias,
                                                    option1=option1[random.randint(0,len(option1)-1)],option2=option2[random.randint(0,len(option2)-1)],
                                                    option3=option3[random.randint(0,len(option3)-1)],option4=option4[random.randint(0,len(option4)-1)],
                                                    option5=option5[random.randint(0,len(option5)-1)],option6=option6[random.randint(0,len(option6)-1)],
                                                    option7=option7[random.randint(0,len(option7)-1)],option8=option8[random.randint(0,len(option8)-1)],
                                                    option9=option9[random.randint(0,len(option9)-1)],option10=option10[random.randint(0,len(option10)-1)],
                                                    option11=option11[random.randint(0,len(option11)-1)],option12=option12[random.randint(0,len(option12)-1)],
                                                    option13=option13[random.randint(0,len(option13)-1)],option14=option14[random.randint(0,len(option14)-1)],
                                                    option15=option15[random.randint(0,len(option15)-1)],option16=option16[random.randint(0,len(option16)-1)],
                                                    option17=option17[random.randint(0,len(option17)-1)],option18=option18[random.randint(0,len(option18)-1)],
                                                    option19=option19[random.randint(0,len(option19)-1)],option20=option20[random.randint(0,len(option20)-1)],
                                                    option21=option21[random.randint(0,len(option21)-1)],option22=option22[random.randint(0,len(option22)-1)],
                                                    option23=option23[random.randint(0,len(option23)-1)],option49=option49[random.randint(0,len(option49)-1)])
        dinamic_text.second_text = text2.format(destination=subregion.alias,
                                                option24=option24[random.randint(0,len(option24)-1)],option36=option36[random.randint(0,len(option36)-1)],
                                                option25=option25[random.randint(0,len(option25)-1)],option37=option37[random.randint(0,len(option37)-1)],
                                                option26=option26[random.randint(0,len(option26)-1)],option38=option38[random.randint(0,len(option38)-1)],
                                                option27=option27[random.randint(0,len(option27)-1)],option39=option39[random.randint(0,len(option39)-1)],
                                                option28=option28[random.randint(0,len(option28)-1)],option40=option40[random.randint(0,len(option40)-1)],
                                                option29=option29[random.randint(0,len(option29)-1)],option41=option41[random.randint(0,len(option41)-1)],
                                                option30=option30[random.randint(0,len(option30)-1)],option42=option42[random.randint(0,len(option42)-1)],
                                                option31=option31[random.randint(0,len(option31)-1)],option43=option43[random.randint(0,len(option43)-1)],
                                                option32=option32[random.randint(0,len(option32)-1)],option44=option44[random.randint(0,len(option44)-1)],
                                                option33=option33[random.randint(0,len(option33)-1)],option45=option45[random.randint(0,len(option45)-1)],
                                                option34=option34[random.randint(0,len(option34)-1)],option46=option46[random.randint(0,len(option46)-1)],
                                                option35=option35[random.randint(0,len(option35)-1)],option47=option47[random.randint(0,len(option47)-1)],
                                                sort_by=generate_random_sort(),option48=option48[random.randint(0,len(option48)-1)],
                                                email=EMAIL, phone=PHONE)

        cruiselines = subregion.getCruisesLines()
        list = generate_random_list(0, len(cruiselines))
        count = len(list) if (len(cruiselines) < 5) else 5
        for i in range(count):
            cruise_line = CruiseLine.objects.get(pk=cruiselines[list[i]])
            if i == 0:
                 dinamic_text.cruise_line1 = '<a style="color: #fff" href="/cruiselines/{cruiseline_slug}/subregion/' \
                 '{subregion}/">{name}</a>'.format(cruiseline_slug=cruise_line.slug,
                                        subregion=subregion.slug,name=cruise_line.name)
            elif i == 1:
                dinamic_text.cruise_line2 = '<a style="color: #fff" href="/cruiselines/{cruiseline_slug}/subregion/' \
                 '{subregion}/">{name}</a>'.format(cruiseline_slug=cruise_line.slug,
                                        subregion=subregion.slug,name=cruise_line.name)
            elif i == 2:
                dinamic_text.cruise_line3 = '<a style="color: #fff" href="/cruiselines/{cruiseline_slug}/subregion/' \
                 '{subregion}/">{name}</a>'.format(cruiseline_slug=cruise_line.slug,
                                        subregion=subregion.slug,name=cruise_line.name)
            elif i == 3:
                dinamic_text.cruise_line4 = '<a style="color: #fff" href="/cruiselines/{cruiseline_slug}/subregion/' \
                 '{subregion}/">{name}</a>'.format(cruiseline_slug=cruise_line.slug,
                                        subregion=subregion.slug,name=cruise_line.name)
            else:
                dinamic_text.cruise_line5 = '<a style="color: #fff" href="/cruiselines/{cruiseline_slug}/subregion/' \
                 '{subregion}/">{name}</a>'.format(cruiseline_slug=cruise_line.slug,
                                        subregion=subregion.slug,name=cruise_line.name)

        dinamic_text.save()



    ports = Port.objects.all()
    for port in ports:
        print 'generating ports destinations'
        try:
            dinamic_text = port.dinamic_text.get(tag='port_destination')
        except DinamicText.DoesNotExist:
            dinamic_text = DinamicText()
        except DinamicText.MultipleObjectsReturned:
            dinamic_text = port.dinamic_text.filter(tag='port_destination')[0]
        dinamic_text.tag = 'port_destination'
        dinamic_text.content_object = port
        dinamic_text.first_text = text1.format(destination=port.alias,
                                               option1=option1[random.randint(0, len(option1) - 1)],
                                               option2=option2[random.randint(0, len(option2) - 1)],
                                               option3=option3[random.randint(0, len(option3) - 1)],
                                               option4=option4[random.randint(0, len(option4) - 1)],
                                               option5=option5[random.randint(0, len(option5) - 1)],
                                               option6=option6[random.randint(0, len(option6) - 1)],
                                               option7=option7[random.randint(0, len(option7) - 1)],
                                               option8=option8[random.randint(0, len(option8) - 1)],
                                               option9=option9[random.randint(0, len(option9) - 1)],
                                               option10=option10[random.randint(0, len(option10) - 1)],
                                               option11=option11[random.randint(0, len(option11) - 1)],
                                               option12=option12[random.randint(0, len(option12) - 1)],
                                               option13=option13[random.randint(0, len(option13) - 1)],
                                               option14=option14[random.randint(0, len(option14) - 1)],
                                               option15=option15[random.randint(0, len(option15) - 1)],
                                               option16=option16[random.randint(0, len(option16) - 1)],
                                               option17=option17[random.randint(0, len(option17) - 1)],
                                               option18=option18[random.randint(0, len(option18) - 1)],
                                               option19=option19[random.randint(0, len(option19) - 1)],
                                               option20=option20[random.randint(0, len(option20) - 1)],
                                               option21=option21[random.randint(0, len(option21) - 1)],
                                               option22=option22[random.randint(0, len(option22) - 1)],
                                               option23=option23[random.randint(0, len(option23) - 1)],
                                               option49=option49[random.randint(0, len(option49) - 1)])
        dinamic_text.second_text = text2.format(destination=port.alias,
                                                option24=option24[random.randint(0, len(option24) - 1)],
                                                option36=option36[random.randint(0, len(option36) - 1)],
                                                option25=option25[random.randint(0, len(option25) - 1)],
                                                option37=option37[random.randint(0, len(option37) - 1)],
                                                option26=option26[random.randint(0, len(option26) - 1)],
                                                option38=option38[random.randint(0, len(option38) - 1)],
                                                option27=option27[random.randint(0, len(option27) - 1)],
                                                option39=option39[random.randint(0, len(option39) - 1)],
                                                option28=option28[random.randint(0, len(option28) - 1)],
                                                option40=option40[random.randint(0, len(option40) - 1)],
                                                option29=option29[random.randint(0, len(option29) - 1)],
                                                option41=option41[random.randint(0, len(option41) - 1)],
                                                option30=option30[random.randint(0, len(option30) - 1)],
                                                option42=option42[random.randint(0, len(option42) - 1)],
                                                option31=option31[random.randint(0, len(option31) - 1)],
                                                option43=option43[random.randint(0, len(option43) - 1)],
                                                option32=option32[random.randint(0, len(option32) - 1)],
                                                option44=option44[random.randint(0, len(option44) - 1)],
                                                option33=option33[random.randint(0, len(option33) - 1)],
                                                option45=option45[random.randint(0, len(option45) - 1)],
                                                option34=option34[random.randint(0, len(option34) - 1)],
                                                option46=option46[random.randint(0, len(option46) - 1)],
                                                option35=option35[random.randint(0, len(option35) - 1)],
                                                option47=option47[random.randint(0, len(option47) - 1)],
                                                sort_by=generate_random_sort(),
                                                option48=option48[random.randint(0, len(option48) - 1)],
                                                email=EMAIL, phone=PHONE)

        travels = port.getTravelsDestination()
        texts = generating_travels_destination(travels)

        for i in range(len(texts)):
            #travel = Travel.objects.get(pk=travels[list[i]])
            if i == 0:
                 dinamic_text.cruise_line1 = texts[i]
            elif i == 1:
                dinamic_text.cruise_line2 = texts[i]
            elif i == 2:
                dinamic_text.cruise_line3 = texts[i]
            elif i == 3:
                dinamic_text.cruise_line4 = texts[i]
            else:
                dinamic_text.cruise_line5 = texts[i]

        dinamic_text.save()


def generating_travels_destination(travels_id):
    travels_data = []
    try:
        num = random.randint(0, len(travels_id) - 1)
        travel = Travel.objects.get(pk=travels_id[num])
        text = '<a style="color: #fff" href="/cruiselines/{cruiseline_slug}/subregion/' \
               '{subregion}/">{name} ({destination})</a>'.format(cruiseline_slug=travel.ship.cruise_line.slug,
                                                subregion=travel.destination.slug, name=travel.ship.cruise_line.name,
                                                                destination=travel.destination.alias)
        travels_data.append(text)
        count = 1
        while (len(travels_data) <= 5 and count <= len(travels_id)):
            count += 1
            num = random.randint(0, len(travels_id) - 1)
            travel = Travel.objects.get(pk=travels_id[num])
            text = '<a style="color: #fff" href="/cruiselines/{cruiseline_slug}/subregion/' \
                   '{subregion}/">{name} ({destination})</a>'.format(cruiseline_slug=travel.ship.cruise_line.slug,
                                                    subregion=travel.destination.slug, name=travel.ship.cruise_line.name,
                                                                    destination=travel.destination.alias)
            if not text in travels_data:
                travels_data.append(text)
        return travels_data

    except ValueError:
        return travels_data


def generate_random_list(inicial,final):
    list = []
    try:
        list.append(random.randint(inicial,final-1))
        while (len(list) <= 5 and len(list) < final):
            num = random.randint(inicial,final-1)
            if not num in list:
                list.append(num)
        return list
    except ValueError:
        return list


def generate_random_sort():
    sort_by = ['date range','cruise line','length','ship','departure port','price']
    list = []
    list.append(random.randint(0,5))
    while (len(list) < 6):
        num = random.randint(0,5)
        if not num in list:
            list.append(num)
    return '{option1}, {option2}, {option3}, {option4}, {option5}, or {option6}'.format(option1=sort_by[list[0]], option2=sort_by[list[1]],
                                                                                       option3=sort_by[list[2]], option4=sort_by[list[3]],
                                                                                       option5=sort_by[list[4]], option6=sort_by[list[5]])


#@cronjobs.register
def departure_text():
    print 'generating departure_text'
    option1 = ['Searching for','Looking for','Seeking','Need a list of']
    option2 = ['Red Hot Cruises','RedHotCruises','RedHotCruises.com']
    option3 = ['offers','provides','has','includes','contains']
    option4 = ['a huge assortment','one of the largest selections','many types']
    option5 = ['rock bottom','super low','unbeatable','fantastic','great']
    option6 = ['rates','prices']
    option7 = ['arriving','coming','traveling','vacationing']
    option8 = ['out of town','somewhere else','another location','a different place','another place','a different location']
    option9 = ['close','local','in the area','in town']
    option10 = ['a perfect','a great','an ideal','an excellent','a popular']
    option11 = ['location','place','spot','choice']
    option12 = ['begin','head out on','start','embark on','set sail on','leave on']
    option13 = ['using','with','aboard','through','via']
    option14 = ['best','most beloved','most trusted','most popular','premier','most well-known','top','most well-respected']
    option15 = ['companies','lines','providers','professionals','hosts','leaders']
    option16 = ['including','like','featuring']

    text1 = """ {option1} cruises departing from {departure}? {option2} {option3} {option4} of cruises leaving from
    {departure} at {option5} {option6}. Whether you are {option7} from {option8} or already {option9}, {departure} is
    {option10} {option11} to {option12} a cruise {option13} some of the industry’s {option14} cruise {option15} {option16}: """

    option17 = ['View','Check out','Browse','See']
    option18 = ['complete','full','entire','detailed']
    option19 = ['list','lineup','schedule','listing','catalog','calendar']
    option20 = ['available','possible','upcoming','forthcoming']
    option21 = ['Once choosing','When you find','Once you see','When choosing','When selecting']
    option22 = ['perfect','ideal','best']
    option23 = ['view','check out','browse','see']
    option24 = ['more','additional','further']
    option25 = ['specifics','information']
    option26 = ['complete','full','entire','detailed']
    option27 = ['schedule','itinerary','agenda']
    option28 = ['ship','vessel','boat','yacht','sailing']
    option29 = ['a list of','the available','some featured']
    option30 = ['trips','excursions','adventures','ventures']
    option31 = ['Vacancy','Availability','Space']
    option32 = ['very','extremely','usually','often']
    option33 = ['purchase','book','buy','reserve','order']
    option34 = ['now','today','as soon as possible','quickly','without delay']
    option35 = ['ensure','lock in','grab']
    option36 = ['it’s too late','it sells out completely','they are gone','waiting too long']
    option37 = ['Give us a call','Call us','Get a hold of us','Contact us']
    option38 = ['speak','talk','consult']
    option39 = ['cruise','vacation']
    option40 = ['professional','specialist','expert','pro']
    option41 = ['guide you in the right direction','answer any questions you have','help you find what you’re looking for','find the perfect cruise for you']


    text2 = """ {option17} our {option18} {option19} of all {option20} cruises from {departure} below and sort by {sort_by}.
    {option21} the {option22} cruise from {departure}, you can {option23} {option24} {option25} and the {option26} cruise
    {option27} as well as the {option28} details and {option29} shore {option30}. {option31} is {option32} limited, so
    {option33} online with us {option34} to {option35} your {departure} cruise reservation before {option36}. {option37}
    at {phone} or {email} to {option38} with a {option39} {option40} and we will {option41}. """

    ports = Port.objects.all()
    for port in ports:
        print 'generating ports departures'
        try:
            dinamic_text = port.dinamic_text.get(tag='port_departure')
        except DinamicText.DoesNotExist:
            dinamic_text = DinamicText()
        except DinamicText.MultipleObjectsReturned:
            dinamic_text = port.dinamic_text.filter(tag='port_departure')[0]
        dinamic_text.tag = 'port_departure'
        dinamic_text.content_object = port
        dinamic_text.first_text = text1.format(departure=port.alias,
                                               option1=option1[random.randint(0, len(option1) - 1)],
                                               option2=option2[random.randint(0, len(option2) - 1)],
                                               option3=option3[random.randint(0, len(option3) - 1)],
                                               option4=option4[random.randint(0, len(option4) - 1)],
                                               option5=option5[random.randint(0, len(option5) - 1)],
                                               option6=option6[random.randint(0, len(option6) - 1)],
                                               option7=option7[random.randint(0, len(option7) - 1)],
                                               option8=option8[random.randint(0, len(option8) - 1)],
                                               option9=option9[random.randint(0, len(option9) - 1)],
                                               option10=option10[random.randint(0, len(option10) - 1)],
                                               option11=option11[random.randint(0, len(option11) - 1)],
                                               option12=option12[random.randint(0, len(option12) - 1)],
                                               option13=option13[random.randint(0, len(option13) - 1)],
                                               option14=option14[random.randint(0, len(option14) - 1)],
                                               option15=option15[random.randint(0, len(option15) - 1)],
                                               option16=option16[random.randint(0, len(option16) - 1)])
        dinamic_text.second_text = text2.format(departure=port.alias,
                                                option17=option17[random.randint(0, len(option17) - 1)],
                                                option18=option18[random.randint(0, len(option18) - 1)],
                                                option19=option19[random.randint(0, len(option19) - 1)],
                                                option20=option20[random.randint(0, len(option20) - 1)],
                                                option21=option21[random.randint(0, len(option21) - 1)],
                                                option22=option22[random.randint(0, len(option22) - 1)],
                                                option23=option23[random.randint(0, len(option23) - 1)],
                                                option24=option24[random.randint(0, len(option24) - 1)],
                                                option36=option36[random.randint(0, len(option36) - 1)],
                                                option25=option25[random.randint(0, len(option25) - 1)],
                                                option37=option37[random.randint(0, len(option37) - 1)],
                                                option26=option26[random.randint(0, len(option26) - 1)],
                                                option38=option38[random.randint(0, len(option38) - 1)],
                                                option27=option27[random.randint(0, len(option27) - 1)],
                                                option39=option39[random.randint(0, len(option39) - 1)],
                                                option28=option28[random.randint(0, len(option28) - 1)],
                                                option40=option40[random.randint(0, len(option40) - 1)],
                                                option29=option29[random.randint(0, len(option29) - 1)],
                                                option41=option41[random.randint(0, len(option41) - 1)],
                                                option30=option30[random.randint(0, len(option30) - 1)],
                                                option31=option31[random.randint(0, len(option31) - 1)],
                                                option32=option32[random.randint(0, len(option32) - 1)],
                                                option33=option33[random.randint(0, len(option33) - 1)],
                                                option34=option34[random.randint(0, len(option34) - 1)],
                                                option35=option35[random.randint(0, len(option35) - 1)],
                                                sort_by=generate_random_sort(),
                                                email=EMAIL, phone=PHONE)

        travels = port.getTravelsDeparture()
        texts = generating_travels_departure(travels, port)
        for i in range(len(texts)):
            if i == 0:
                 dinamic_text.cruise_line1 = texts[i]
            elif i == 1:
                dinamic_text.cruise_line2 = texts[i]
            elif i == 2:
                dinamic_text.cruise_line3 = texts[i]
            elif i == 3:
                dinamic_text.cruise_line4 = texts[i]
            else:
                dinamic_text.cruise_line5 = texts[i]

        dinamic_text.save()


def generating_travels_departure(travels_id, port):
    travels_data = []
    try:
        num = random.randint(0, len(travels_id) - 1)
        travel = Travel.objects.get(pk=travels_id[num])
        text = '<a style="color: #fff" href="/cruiselines/{cruiseline_slug}/departure/' \
               '{departure}/">{name}</a>'.format(cruiseline_slug=travel.ship.cruise_line.slug,
                                                departure=port.slug, name=travel.ship.cruise_line.name)
        travels_data.append(text)
        count = 1
        while (len(travels_data) <= 5 and count <= len(travels_id)):
            count += 1
            num = random.randint(0, len(travels_id) - 1)
            travel = Travel.objects.get(pk=travels_id[num])
            text = '<a style="color: #fff" href="/cruiselines/{cruiseline_slug}/departure/' \
                   '{departure}/">{name}</a>'.format(cruiseline_slug=travel.ship.cruise_line.slug,
                                                    departure=port.slug, name=travel.ship.cruise_line.name)
            if not text in travels_data:
                travels_data.append(text)
        return travels_data

    except ValueError:
        return travels_data


#@cronjobs.register
def cruiseline_text():
    print 'generating cruiseline_text'
    option1 = ['has become','has always been','has proven to be','has developed into','has made itself into','is known as','is definitely']
    option2 = ['best','most beloved','most trusted','most popular','premier','most well-known','top','most well-respected']
    option3 = ['companies','lines','providers','hosts','leaders']
    option4 = ['world','industry']
    option5 = ['Head out on','Embark on','Set sail on','Leave on']
    option6 = ['cruise','vacation','getaway']
    option7 = ['enjoy','expect','get','receive','find']
    option8 = ['a slew of','only the best in','tons of','many types of']
    option9 = ['luxury','extravagant','delightful','lavish','generous']
    option10 = ['terrific','helpful','pampering','catering','favoring','delightful']
    option11 = ['fantastic','extraordinary','uncommon','special','outstanding','incredible','impressive','stunning','spectacular']
    option12 = ['Red Hot Cruises','RedHotCruises','RedHotCruises.com']
    option13 = ['offers','provides','has','includes','contains']
    option14 = ['a huge assortment','one of the largest selections','many types']
    option15 = ['rock bottom','super low','unbeatable','fantastic','great']
    option16 = ['rates','prices']
    option17 = ['Here','Below','On this page']
    option18 = ['view','check out','browse','see']
    option19 = ['Vacancy','Availability','Space']
    option20 = ['very','extremely','usually','often']
    option21 = ['purchase','book','buy','reserve','order']
    option22 = ['now','today','as soon as possible','quickly','without delay']
    option23 = ['ensure','lock in','grab']
    option24 = ['it’s too late','it sells out completely','they are gone','waiting too long']
    option25 = ['Give us a call','Call us','Get a hold of us','Contact us']
    option26 = ['speak','talk','consult']
    option27 = ['cruise','vacation']
    option28 = ['professional','specialist','expert','pro']
    option29 = ['guide you in the right direction','answer any questions you have','help you find what you’re looking for','find the perfect cruise for you']

    text1 = """ {cruiseline} {option1} one of the {option2} cruise {option3} in the {option4}. {option5} a {option6} with
    {cruiseline} and {option7} {option8} {option9} on-board amenities, a {option10} service staff, and a truly {option11}
    fleet. {option12} {option13} {option14} of cruises with {cruiseline} at {option15} {option16}. {option17}, you can
    {option18} all destinations, departures and ships available with {cruiseline}. {option19} is {option20} limited, so
    {option21} online with us {option22} to {option23} your cruise with {cruiseline} before {option24}. {option25} at
    {phone} or {email} to {option26} with a {option27} {option28} and we will {option29}."""

    cruiselines = CruiseLine.objects.all()
    for cruiseline in cruiselines:
        print 'generating cruiselines'
        try:
            dinamic_text = cruiseline.dinamic_text.get(tag='cruiseline_lines')
        except DinamicText.DoesNotExist:
            dinamic_text = DinamicText()
        except DinamicText.MultipleObjectsReturned:
            dinamic_text = cruiseline.dinamic_text.filter(tag='cruiseline_lines')[0]
        dinamic_text.tag = 'cruiseline_lines'
        dinamic_text.content_object = cruiseline
        dinamic_text.first_text = text1.format(cruiseline=cruiseline.name,
                                               option1=option1[random.randint(0, len(option1) - 1)],
                                               option2=option2[random.randint(0, len(option2) - 1)],
                                               option3=option3[random.randint(0, len(option3) - 1)],
                                               option4=option4[random.randint(0, len(option4) - 1)],
                                               option5=option5[random.randint(0, len(option5) - 1)],
                                               option6=option6[random.randint(0, len(option6) - 1)],
                                               option7=option7[random.randint(0, len(option7) - 1)],
                                               option8=option8[random.randint(0, len(option8) - 1)],
                                               option9=option9[random.randint(0, len(option9) - 1)],
                                               option10=option10[random.randint(0, len(option10) - 1)],
                                               option11=option11[random.randint(0, len(option11) - 1)],
                                               option12=option12[random.randint(0, len(option12) - 1)],
                                               option13=option13[random.randint(0, len(option13) - 1)],
                                               option14=option14[random.randint(0, len(option14) - 1)],
                                               option15=option15[random.randint(0, len(option15) - 1)],
                                               option16=option16[random.randint(0, len(option16) - 1)],
                                               option17=option17[random.randint(0, len(option17) - 1)],
                                               option18=option18[random.randint(0, len(option18) - 1)],
                                               option19=option19[random.randint(0, len(option19) - 1)],
                                               option20=option20[random.randint(0, len(option20) - 1)],
                                               option21=option21[random.randint(0, len(option21) - 1)],
                                               option22=option22[random.randint(0, len(option22) - 1)],
                                               option23=option23[random.randint(0, len(option23) - 1)],
                                               option24=option24[random.randint(0, len(option24) - 1)],
                                               option25=option25[random.randint(0, len(option25) - 1)],
                                               option26=option26[random.randint(0, len(option26) - 1)],
                                               option27=option27[random.randint(0, len(option27) - 1)],
                                               option28=option28[random.randint(0, len(option28) - 1)],
                                               option29=option29[random.randint(0, len(option29) - 1)],
                                               email=EMAIL, phone=PHONE)


        dinamic_text.save()


#@cronjobs.register
def cruiseline_destination_text():
    print 'generating cruiseline_destination_text'
    option1 = ['has become','has always been','has proven to be','has developed into','has made itself into','is known as','is definitely']
    option2 = ['top','highly sought-after','popular','must-see','featured','beloved']
    option3 = ['cruise','vacation','getaway']
    option4 = ['Featuring','Complete with','Bursting with']
    option5 = ['beautiful','spectacular','magnificent','stunning','exquisite']
    option6 = ['scenery','landscapes','views','ambiance']
    option7 = ['interesting','fascinating','captivating','inviting']
    option8 = ['charismatic','charming','delightful','lovable','enchanting']
    option9 = ['offers','provides','has','includes','contains']
    option10 = ['heading out','embarking','traveling','going']
    option11 = ['visitors','guests','newcomers','vacationers']
    option12 = ['pick from','choose from','arrange','plan','schedule']
    option13 = ['lots','a wide variety','an assortment','a wide range']
    option14 = ['trips','excursions','adventures','ventures']
    option15 = ['better','fully','further']
    option16 = ['see','witness','experience','enjoy','take part in','appreciate']
    option17 = ['Red Hot Cruises','RedHotCruises','RedHotCruises.com']
    option18 = ['offer','provide you with','have','allow you to choose from']
    option19 = ['a huge assortment','one of the largest selections','many types']
    option20 = ['on','aboard','on board']
    option21 = ['fantastic','extraordinary','uncommon','special','outstanding','incredible','impressive','stunning','spectacular']
    option22 = ['including','like','featuring']
    
    text1 = """{destination} {option1} a {option2} destination for a {option3}, and {cruiseline} will take you there in style.
    {option4} {option5} {option6}, {option7} cultures and {option8} people, {destination} {option9} something for everyone. 
    When {option10} on a cruise with {cruiseline} to {destination}, {option11} can {option12} {option13} of activities and 
    shore {option14} to {option15} {option16} what {destination} has to offer. Here at {option17}, we {option18} {option19} 
    of cruises with {cruiseline} to {destination} {option20} some of their most {option21} ships {option22}: """

    option23 = ['View','Check out','Browse','See']
    option24 = ['complete','full','entire','detailed']
    option25 = ['list','lineup','schedule','listing','catalog','calendar']
    option26 = ['available','possible','upcoming','forthcoming']
    option27 = ['Once choosing','When you find','Once you see','When choosing','When selecting']
    option28 = ['perfect','ideal','best']
    option29 = ['view','check out','browse','see']
    option30 = ['more','additional','further']
    option31 = ['specifics','information']
    option32 = ['complete','full','entire','detailed']
    option33 = ['schedule','itinerary','agenda']
    option34 = ['ship','vessel','boat','yacht','sailing']
    option35 = ['a list of','the available','some featured']
    option36 = ['trips','excursions','adventures','ventures']
    option37 = ['Vacancy','Availability','Space']
    option38 = ['very','extremely','usually','often']
    option39 = ['purchase','book','buy','reserve','order']
    option40 = ['now','today','as soon as possible','quickly','without delay']
    option41 = ['ensure','lock in','grab']
    option42 = ['it’s too late','it sells out completely','they are gone','waiting too long']
    option43 = ['Give us a call','Call us','Get a hold of us','Contact us']
    option44 = ['speak','talk','consult']
    option45 = ['cruise','vacation']
    option46 = ['professional','specialist','expert','pro']
    option47 = ['guide you in the right direction','answer any questions you have','help you find what you’re looking for',''
                'find the perfect cruise for you']

    text2 = """{option23} our {option24} {option25} of all {option26} cruises with {cruiseline} to {destination} below
    and sort by {sort_by}. {option27} the {option28} cruise to {destination}, you can {option29} {option30} {option31}
    and the {option32} cruise {option33} as well as the {option34} details and {option35} shore {option36}. {option37}
    is {option38} limited, so {option39} online with us {option40} to {option41} your {destination} cruise reservation
    with {cruiseline} before {option42}. {option43} at {phone} or {email} to {option44} with a {option45} {option46} and
    we will {option47}. """

    cruiseslines = CruiseLine.objects.all()
    for cruiseline in cruiseslines:
        print 'generating cruiselines'
        travels = Travel.objects.values('destination_id').filter(ship__in=cruiseline.ship_set.all()).order_by('destination__name').annotate(count=Count('destination_id'))
        for travel in travels:
            print 'generating cruiselines destinations'
            subregion = SubRegion.objects.get(pk=travel['destination_id'])
            try:
                dinamic_text = subregion.dinamic_text.get(tag='subregion_cruiseline',slug=cruiseline.slug)
            except DinamicText.DoesNotExist:
                dinamic_text = DinamicText()
            except DinamicText.MultipleObjectsReturned:
                dinamic_text = subregion.dinamic_text.filter(tag='subregion_cruiseline').filter(slug=cruiseline.slug)[0]
            dinamic_text.tag = 'subregion_cruiseline'
            dinamic_text.slug = cruiseline.slug
            dinamic_text.content_object = subregion
            dinamic_text.first_text = text1.format(destination=subregion.alias,cruiseline=cruiseline.name,
                                                   option1=option1[random.randint(0, len(option1) - 1)],
                                                   option2=option2[random.randint(0, len(option2) - 1)],
                                                   option3=option3[random.randint(0, len(option3) - 1)],
                                                   option4=option4[random.randint(0, len(option4) - 1)],
                                                   option5=option5[random.randint(0, len(option5) - 1)],
                                                   option6=option6[random.randint(0, len(option6) - 1)],
                                                   option7=option7[random.randint(0, len(option7) - 1)],
                                                   option8=option8[random.randint(0, len(option8) - 1)],
                                                   option9=option9[random.randint(0, len(option9) - 1)],
                                                   option10=option10[random.randint(0, len(option10) - 1)],
                                                   option11=option11[random.randint(0, len(option11) - 1)],
                                                   option12=option12[random.randint(0, len(option12) - 1)],
                                                   option13=option13[random.randint(0, len(option13) - 1)],
                                                   option14=option14[random.randint(0, len(option14) - 1)],
                                                   option15=option15[random.randint(0, len(option15) - 1)],
                                                   option16=option16[random.randint(0, len(option16) - 1)],
                                                   option17=option17[random.randint(0, len(option17) - 1)],
                                                   option18=option18[random.randint(0, len(option18) - 1)],
                                                   option19=option19[random.randint(0, len(option19) - 1)],
                                                   option20=option20[random.randint(0, len(option20) - 1)],
                                                   option21=option21[random.randint(0, len(option21) - 1)],
                                                   option22=option22[random.randint(0, len(option22) - 1)],)

            dinamic_text.second_text = text2.format(destination=subregion.alias,cruiseline=cruiseline.name,
                                                    option23=option23[random.randint(0, len(option23) - 1)],
                                                    option24=option24[random.randint(0, len(option24) - 1)],
                                                    option36=option36[random.randint(0, len(option36) - 1)],
                                                    option25=option25[random.randint(0, len(option25) - 1)],
                                                    option37=option37[random.randint(0, len(option37) - 1)],
                                                    option26=option26[random.randint(0, len(option26) - 1)],
                                                    option38=option38[random.randint(0, len(option38) - 1)],
                                                    option27=option27[random.randint(0, len(option27) - 1)],
                                                    option39=option39[random.randint(0, len(option39) - 1)],
                                                    option28=option28[random.randint(0, len(option28) - 1)],
                                                    option40=option40[random.randint(0, len(option40) - 1)],
                                                    option29=option29[random.randint(0, len(option29) - 1)],
                                                    option41=option41[random.randint(0, len(option41) - 1)],
                                                    option30=option30[random.randint(0, len(option30) - 1)],
                                                    option42=option42[random.randint(0, len(option42) - 1)],
                                                    option31=option31[random.randint(0, len(option31) - 1)],
                                                    option43=option43[random.randint(0, len(option43) - 1)],
                                                    option32=option32[random.randint(0, len(option32) - 1)],
                                                    option44=option44[random.randint(0, len(option44) - 1)],
                                                    option33=option33[random.randint(0, len(option33) - 1)],
                                                    option45=option45[random.randint(0, len(option45) - 1)],
                                                    option34=option34[random.randint(0, len(option34) - 1)],
                                                    option46=option46[random.randint(0, len(option46) - 1)],
                                                    option35=option35[random.randint(0, len(option35) - 1)],
                                                    option47=option47[random.randint(0, len(option47) - 1)],
                                                    sort_by=generate_random_sort(),
                                                    email=EMAIL, phone=PHONE)

            travels = cruiseline.getTravelsByDestination(subregion.id)
            print 'travels {t}'.format(t=len(travels))
            texts = generating_travels_ship(travels)
            print 'texts {t}'.format(t=len(texts))
            for i in range(len(texts)):
                if i == 0:
                     dinamic_text.cruise_line1 = texts[i]
                elif i == 1:
                    dinamic_text.cruise_line2 = texts[i]
                elif i == 2:
                    dinamic_text.cruise_line3 = texts[i]
                elif i == 3:
                    dinamic_text.cruise_line4 = texts[i]
                else:
                    dinamic_text.cruise_line5 = texts[i]

            dinamic_text.save()


def generating_travels_ship(travels_id):
    travels_data = []
    try:
        num = random.randint(0, len(travels_id) - 1)
        travel = Travel.objects.get(pk=travels_id[num])
        text = '<a style="color: #fff" ' \
                        'href="/cruiselines/{cruiseline}/ship/{ship_slug}/">{ship}</a>'.format(
                        cruiseline=travel.ship.cruise_line.slug,ship_slug=travel.ship.slug,ship=travel.ship.name)
        travels_data.append(text)
        count = 1
        while (len(travels_data) <= 5 and count <= len(travels_id)):
            count += 1
            num = random.randint(0, len(travels_id) - 1)
            travel = Travel.objects.get(pk=travels_id[num])
            text = '<a style="color: #fff" ' \
                        'href="/cruiselines/{cruiseline}/ship/{ship_slug}/">{ship}</a>'.format(
                        cruiseline=travel.ship.cruise_line.slug,ship_slug=travel.ship.slug,ship=travel.ship.name)
            if not text in travels_data:
                travels_data.append(text)
        return travels_data

    except ValueError:
        return travels_data


#@cronjobs.register
def cruiseline_departure_text():
    option1 = ['Searching for','Looking for','Seeking','Need a list of']
    option2 = ['Red Hot Cruises','RedHotCruises','RedHotCruises.com']
    option3 = ['offers','provides','has','includes','contains']
    option4 = ['a huge assortment','one of the largest selections','many types']
    option5 = ['rock bottom','super low','unbeatable','fantastic','great']
    option6 = ['rates','prices']
    option7 = ['arriving','coming','traveling','vacationing']
    option8 = ['out of town','somewhere else','another location','a different place','another place','a different location']
    option9 = ['close','local','in the area','in town']
    option10 = ['a perfect','a great','an ideal','an excellent','a popular']
    option11 = ['location','place','spot','choice']
    option12 = ['begin','head out on','start','embark on','set sail on','leave on']
    option13 = ['on','aboard','on board']
    option14 = ['fantastic','extraordinary','uncommon','special','outstanding','incredible','impressive','stunning','spectacular']
    option15 = ['including','like','featuring']

    text1 = """{option1} cruises with {cruiseline} departing from {departure}? {option2} {option3} {option4} of cruises
    with {cruiseline} leaving from {departure} at {option5} {option6}. Whether you are {option7} from {option8} or already
    {option9}, {departure} is {option10} {option11} to {option12} a cruise with {cruiseline} {option13} some of their most
    {option14} ships {option15}:"""

    option16 = ['View','Check out','Browse','See']
    option17 = ['complete','full','entire','detailed']
    option18 = ['list','lineup','schedule','listing','catalog','calendar']
    option19 = ['available','possible','upcoming','forthcoming']
    option20 = ['Once choosing','When you find','Once you see','When choosing','When selecting']
    option21 = ['perfect','ideal','best']
    option22 = ['view','check out','browse','see']
    option23 = ['more','additional','further']
    option24 = ['specifics','information']
    option25 = ['complete','full','entire','detailed']
    option26 = ['schedule','itinerary','agenda']
    option27 = ['ship','vessel','boat','yacht','sailing']
    option28 = ['a list of','the available','some featured']
    option29 = ['trips','excursions','adventures','ventures']
    option30 = ['Vacancy','Availability','Space']
    option31 = ['very','extremely','usually','often']
    option32 = ['purchase','book','buy','reserve','order']
    option33 = ['now','today','as soon as possible','quickly','without delay']
    option34 = ['ensure','lock in','grab']
    option35 = ['it’s too late','it sells out completely','they are gone','waiting too long']
    option36 = ['Give us a call','Call us','Get a hold of us','Contact us']
    option37 = ['speak','talk','consult']
    option38 = ['cruise','vacation']
    option39 = ['professional','specialist','expert','pro']
    option40 = ['guide you in the right direction','answer any questions you have','help you find what you’re looking for','find the perfect cruise for you']

    text2 = """{option16} our {option17} {option18} of all {option19} cruises with {cruiseline} from {departure} below 
    and sort by {sort_by}. {option20} the {option21} cruise from {departure}, you can {option22} {option23} {option24} 
    and the {option25} cruise {option26} as well as the {option27} details and {option28} shore {option29}. {option30} 
    is {option31} limited, so {option32} online with us {option33} to {option34} your {departure} cruise reservation 
    with {cruiseline} before {option35}. {option36} at {phone} or {email} to {option37} with a {option38} {option39} and 
    we will {option40}."""

    cruiseslines = CruiseLine.objects.all()
    for cruiseline in cruiseslines:
        print 'generating cruiselines'
        travels = TravelDates.objects.values('departure_port').filter(travel__ship_id__in = cruiseline.ship_set.all()).order_by('departure_port').annotate(count = Count('departure_port'))
        for travel in travels:
            try:
                port = Port.objects.get_by_name(travel['departure_port'])
                print 'generating cruiselines departure'
                try:
                    dinamic_text = port.dinamic_text.get(tag='port_cruiseline',slug=cruiseline.slug)
                except DinamicText.DoesNotExist:
                    dinamic_text = DinamicText()
                except DinamicText.MultipleObjectsReturned:
                    dinamic_text = port.dinamic_text.filter(tag='port_cruiseline').filter(slug=cruiseline.slug)[0]
                dinamic_text.tag = 'port_cruiseline'
                dinamic_text.slug = cruiseline.slug
                dinamic_text.content_object = port
                dinamic_text.first_text = text1.format(departure=port.alias,cruiseline=cruiseline.name,
                                                       option1=option1[random.randint(0, len(option1) - 1)],
                                                       option2=option2[random.randint(0, len(option2) - 1)],
                                                       option3=option3[random.randint(0, len(option3) - 1)],
                                                       option4=option4[random.randint(0, len(option4) - 1)],
                                                       option5=option5[random.randint(0, len(option5) - 1)],
                                                       option6=option6[random.randint(0, len(option6) - 1)],
                                                       option7=option7[random.randint(0, len(option7) - 1)],
                                                       option8=option8[random.randint(0, len(option8) - 1)],
                                                       option9=option9[random.randint(0, len(option9) - 1)],
                                                       option10=option10[random.randint(0, len(option10) - 1)],
                                                       option11=option11[random.randint(0, len(option11) - 1)],
                                                       option12=option12[random.randint(0, len(option12) - 1)],
                                                       option13=option13[random.randint(0, len(option13) - 1)],
                                                       option14=option14[random.randint(0, len(option14) - 1)],
                                                       option15=option15[random.randint(0, len(option15) - 1)])

                dinamic_text.second_text = text2.format(departure=port.alias,cruiseline=cruiseline.name,
                                                        option16=option16[random.randint(0, len(option16) - 1)],
                                                        option17=option17[random.randint(0, len(option17) - 1)],
                                                        option18=option18[random.randint(0, len(option18) - 1)],
                                                        option19=option19[random.randint(0, len(option19) - 1)],
                                                        option20=option20[random.randint(0, len(option20) - 1)],
                                                        option21=option21[random.randint(0, len(option21) - 1)],
                                                        option22=option22[random.randint(0, len(option22) - 1)],
                                                        option23=option23[random.randint(0, len(option23) - 1)],
                                                        option24=option24[random.randint(0, len(option24) - 1)],
                                                        option36=option36[random.randint(0, len(option36) - 1)],
                                                        option25=option25[random.randint(0, len(option25) - 1)],
                                                        option37=option37[random.randint(0, len(option37) - 1)],
                                                        option26=option26[random.randint(0, len(option26) - 1)],
                                                        option38=option38[random.randint(0, len(option38) - 1)],
                                                        option27=option27[random.randint(0, len(option27) - 1)],
                                                        option39=option39[random.randint(0, len(option39) - 1)],
                                                        option28=option28[random.randint(0, len(option28) - 1)],
                                                        option40=option40[random.randint(0, len(option40) - 1)],
                                                        option29=option29[random.randint(0, len(option29) - 1)],
                                                        option30=option30[random.randint(0, len(option30) - 1)],
                                                        option31=option31[random.randint(0, len(option31) - 1)],
                                                        option32=option32[random.randint(0, len(option32) - 1)],
                                                        option33=option33[random.randint(0, len(option33) - 1)],
                                                        option34=option34[random.randint(0, len(option34) - 1)],
                                                        option35=option35[random.randint(0, len(option35) - 1)],
                                                        sort_by=generate_random_sort(),
                                                        email=EMAIL, phone=PHONE)


                travels = cruiseline.getTravelsByDeparture(port.name)
                print 'travels {t}'.format(t=len(travels))
                texts = generating_travels_ship(travels)
                print 'texts {t}'.format(t=len(texts))
                for i in range(len(texts)):
                    if i == 0:
                         dinamic_text.cruise_line1 = texts[i]
                    elif i == 1:
                        dinamic_text.cruise_line2 = texts[i]
                    elif i == 2:
                        dinamic_text.cruise_line3 = texts[i]
                    elif i == 3:
                        dinamic_text.cruise_line4 = texts[i]
                    else:
                        dinamic_text.cruise_line5 = texts[i]

                dinamic_text.save()

                dinamic_text.save()
            except Port.DoesNotExist:
                pass


#@cronjobs.register
def cruiseline_ship_text():
    option1 = ['has become','has always been','has proven to be','has developed into','has made itself into','is known as','is definitely']
    option2 = ['best','most beloved','most trusted','most popular','premier','most well-known','top','most well-respected']
    option3 = ['companies','lines','providers','hosts','leaders']
    option4 = ['world','industry']
    option5 = ['Head out on','Embark on','Set sail on','Leave on']
    option6 = ['cruise','vacation','getaway']
    option7 = ['enjoy','expect','get','receive','find']
    option8 = ['a slew of','only the best in','tons of','many types of']
    option9 = ['luxury','extravagant','delightful','lavish','generous']
    option10 = ['terrific','helpful','pampering','catering','favoring','delightful']
    option11 = ['fantastic','extraordinary','uncommon','special','outstanding','incredible','impressive','stunning','spectacular']
    option12 = ['Red Hot Cruises','RedHotCruises','RedHotCruises.com']
    option13 = ['offers','provides','has','includes','contains']
    option14 = ['a huge assortment','one of the largest selections','many types']
    option15 = ['rock bottom','super low','unbeatable','fantastic','great']
    option16 = ['rates','prices']
    option17 = ['View','Check out','Browse','See']
    option18 = ['complete','full','entire','detailed']
    option19 = ['list','lineup','schedule','listing','catalog','calendar']
    option20 = ['available','possible','upcoming','forthcoming']
    option21 = ['Once choosing','When you find','Once you see','When choosing','When selecting']
    option22 = ['perfect','ideal','best']
    option23 = ['view','check out','browse','see']
    option24 = ['more','additional','further']
    option25 = ['specifics','information']
    option26 = ['complete','full','entire','detailed']
    option27 = ['schedule','itinerary','agenda']
    option28 = ['ship','vessel','boat','yacht','sailing']
    option29 = ['a list of','the available','some featured']
    option30 = ['trips','excursions','adventures','ventures']
    option31 = ['Vacancy','Availability','Space']
    option32 = ['very','extremely','usually','often']
    option33 = ['purchase','book','buy','reserve','order']
    option34 = ['now','today','as soon as possible','quickly','without delay']
    option35 = ['ensure','lock in','grab']
    option36 = ['it’s too late','it sells out completely','they are gone','waiting too long']
    option37 = ['Give us a call','Call us','Get a hold of us','Contact us']
    option38 = ['speak','talk','consult']
    option39 = ['cruise','vacation']
    option40 = ['professional','specialist','expert','pro']
    option41 = ['guide you in the right direction','answer any questions you have','help you find what you’re looking for','find the perfect cruise for you']


    text1 = """{cruiseline} {option1} one of the {option2} cruise {option3} in the {option4}, and what better way to
    cruise aboard the {ship}. {option5} a {option6} with {cruiseline} and {option7} {option8} {option9} on-board
    amenities and a {option10} service staff aboard the truly {option11} {ship}. {option12} {option13} {option14} of
    {ship} cruises at {option15} {option16}. {option17} our {option18} {option19} of all {option20} {ship} cruises below
    and sort by {sort_by}. {option21} the {option22} cruise aboard the {ship}, you can {option23} {option24} {option25}
    and the {option26} cruise {option27} as well as the {option28} details and {option29} shore {option30}. {option31}
    is {option32} limited, so {option33} online with us {option34} to {option35} your {ship} cruise reservation before
    {option36}. {option37} at {phone} or {email} to {option38} with a {option39} {option40} and we will {option41}."""

    ships = Ship.objects.all()
    for ship in ships:
        print 'generating ships'
        try:
            dinamic_text = ship.dinamic_text.get(tag='ship_cruiseline')
        except DinamicText.DoesNotExist:
            dinamic_text = DinamicText()
        except DinamicText.MultipleObjectsReturned:
            dinamic_text = ship.dinamic_text.filter(tag='ship_cruiseline')[0]
        dinamic_text.tag = 'ship_cruiseline'
        dinamic_text.content_object = ship
        dinamic_text.first_text = text1.format(cruiseline=ship.cruise_line.name, ship=ship.name,
                                               option1=option1[random.randint(0, len(option1) - 1)],
                                               option2=option2[random.randint(0, len(option2) - 1)],
                                               option3=option3[random.randint(0, len(option3) - 1)],
                                               option4=option4[random.randint(0, len(option4) - 1)],
                                               option5=option5[random.randint(0, len(option5) - 1)],
                                               option6=option6[random.randint(0, len(option6) - 1)],
                                               option7=option7[random.randint(0, len(option7) - 1)],
                                               option8=option8[random.randint(0, len(option8) - 1)],
                                               option9=option9[random.randint(0, len(option9) - 1)],
                                               option10=option10[random.randint(0, len(option10) - 1)],
                                               option11=option11[random.randint(0, len(option11) - 1)],
                                               option12=option12[random.randint(0, len(option12) - 1)],
                                               option13=option13[random.randint(0, len(option13) - 1)],
                                               option14=option14[random.randint(0, len(option14) - 1)],
                                               option15=option15[random.randint(0, len(option15) - 1)],
                                               option16=option16[random.randint(0, len(option16) - 1)],
                                               option17=option17[random.randint(0, len(option17) - 1)],
                                               option18=option18[random.randint(0, len(option18) - 1)],
                                               option19=option19[random.randint(0, len(option19) - 1)],
                                               option20=option20[random.randint(0, len(option20) - 1)],
                                               option21=option21[random.randint(0, len(option21) - 1)],
                                               option22=option22[random.randint(0, len(option22) - 1)],
                                               option23=option23[random.randint(0, len(option23) - 1)],
                                               option24=option24[random.randint(0, len(option24) - 1)],
                                               option36=option36[random.randint(0, len(option36) - 1)],
                                               option25=option25[random.randint(0, len(option25) - 1)],
                                               option37=option37[random.randint(0, len(option37) - 1)],
                                               option26=option26[random.randint(0, len(option26) - 1)],
                                               option38=option38[random.randint(0, len(option38) - 1)],
                                               option27=option27[random.randint(0, len(option27) - 1)],
                                               option39=option39[random.randint(0, len(option39) - 1)],
                                               option28=option28[random.randint(0, len(option28) - 1)],
                                               option40=option40[random.randint(0, len(option40) - 1)],
                                               option29=option29[random.randint(0, len(option29) - 1)],
                                               option41=option41[random.randint(0, len(option41) - 1)],
                                               option30=option30[random.randint(0, len(option30) - 1)],
                                               option31=option31[random.randint(0, len(option31) - 1)],
                                               option32=option32[random.randint(0, len(option32) - 1)],
                                               option33=option33[random.randint(0, len(option33) - 1)],
                                               option34=option34[random.randint(0, len(option34) - 1)],
                                               option35=option35[random.randint(0, len(option35) - 1)],
                                               sort_by=generate_random_sort(),
                                               email=EMAIL, phone=PHONE)


        dinamic_text.save()


#@cronjobs.register
def generate_all():
    destination_text()
    departure_text()
    cruiseline_text()
    cruiseline_destination_text()
    cruiseline_departure_text()
    cruiseline_ship_text()


#@cronjobs.register
def generate_alias():
    subregions = SubRegion.objects.all()
    for subregion in subregions:
        subregion.alias = subregion.name
        print subregion.alias
        subregion.save()

    ports = Port.objects.all()
    for port in ports:
        port.alias = port.name
        print port.alias
        port.save()


#@cronjobs.register
def update_images():
    ships = Ship.objects.filter(big_logo_src__istartswith = 'http://www.cruiseplanners.com').exclude(big_logo_src__istartswith = 'https://cruiseplannersnet.com')
    #ships = Ship.objects.all()
    for s in ships:
        s.big_logo_src = s.big_logo_src.replace('http://www.cruiseplanners.com', 'https://cruiseplannersnet.com')
        print s.name
        s.save()


# Added functions to fix some bugs 10-08-2016

def generate_subregion_text(subregion):
    option1 = ['has become', 'has always been', 'has proven to be', 'has developed into', 'has made itself into',
               'is known as', 'is definitely']
    option2 = ['top', 'highly sought-after', 'popular', 'must-see', 'featured', 'beloved']
    option3 = ['heading out', 'embarking', 'traveling', 'going']
    option4 = ['cruise', 'vacation', 'getaway']
    option5 = ['Featuring', 'Complete with', 'Bursting with']
    option6 = ['beautiful', 'spectacular', 'magnificent', 'stunning', 'exquisite']
    option7 = ['scenery', 'landscapes', 'views', 'ambiance']
    option8 = ['interesting', 'fascinating', 'captivating', 'inviting']
    option9 = ['charismatic', 'charming', 'delightful', 'lovable', 'enchanting']
    option10 = ['offers', 'provides', 'has', 'includes', 'contains']
    option11 = ['visitors', 'guests', 'newcomers', 'vacationers']
    option12 = ['pick from', 'choose from', 'arrange', 'plan', 'schedule']
    option13 = ['lots', 'a wide variety', 'an assortment', 'a wide range']
    option14 = ['trips', 'excursions', 'adventures', 'ventures']
    option15 = ['better', 'fully', 'further']
    option16 = ['see', 'witness', 'experience', 'enjoy', 'take part in', 'appreciate']
    option17 = ['Red Hot Cruises', 'RedHotCruises', 'RedHotCruises.com']
    option18 = ['offer', 'provide you with', 'have', 'allow you to choose from']
    option19 = ['a huge assortment', 'one of the largest selections', 'many types']
    option20 = ['aboard', 'with', 'through', 'via']
    option21 = ['world’s', 'industry’s']
    option22 = ['best', 'most beloved', 'most trusted', 'most popular', 'premier', 'most well-known', 'top',
                'most well-respected']
    option23 = ['companies', 'lines', 'providers', 'hosts', 'leaders']
    option49 = ['including', 'like', 'featuring']

    text1 = """
    {destination} {option1} a {option2} destination when {option3} on a {option4}. {option5} {option6} {option7}, {option8}
    cultures and {option9} people, {destination} {option10} something for everyone. With all cruises to {destination}, {option11}
    can {option12} {option13} of activities and shore {option14} to {option15} {option16} what {destination} has to offer.
    Here at {option17}, we {option18} {option19} of cruises to {destination} {option20} some of the {option21} {option22}
    cruise {option23} {option49}: """

    option24 = ['View','Check out','Browse','See']
    option48 = ['complete','full','entire','detailed']
    option25 = ['list','lineup','schedule','listing','catalog','calendar']
    option26 = ['available','possible','upcoming','forthcoming']
    #sort_by = ['date range,cruise line,length,ship,departure port']
    option27 = ['Once choosing','When you find','Once you see','When choosing','When selecting']
    option28 = ['perfect','ideal','best']
    option29 = ['view','check out','browse','see']
    option30 = ['more','additional','further']
    option31 = ['specifics','information']
    option32 = ['complete','full','entire','detailed']
    option33 = ['schedule','itinerary','agenda']
    option34 = ['ship','vessel','boat','yacht','sailing']
    option35 = ['a list of','the available','some featured']
    option36 = ['trips','excursions','adventures','ventures']
    option37 = ['Vacancy','Availability','Space']
    option38 = ['very','extremely','usually','often']
    option39 = ['purchase','book','buy','reserve','order']
    option40 = ['now','today','as soon as possible','quickly','without delay']
    option41 = ['ensure','lock in','grab']
    option42 = ['it’s too late','it sells out completely','they are gone','waiting too long']
    option43 = ['Give us a call','Call us','Get a hold of us','Contact us']
    option44 = ['speak','talk','consult']
    option45 = ['cruise','vacation']
    option46 = ['professional','specialist','expert','pro']
    option47 = ['guide you in the right direction','answer any questions you have','help you find what you’re looking for','find the perfect cruise for you']

    text2 = """
    {option24} our {option48} {option25} of all {option26} cruises to {destination} and sort by {sort_by}.
    {option27} the {option28} cruise, you can {option29} {option30} {option31} and the {option32} cruise {option33} as well
    as the {option34} details and {option35} shore {option36} in {destination}. {option37} is {option38} limited, so
    {option39} online with us {option40} to {option41} your {destination} cruise reservation before {option42}. {option43} at
    {phone} or {email} to {option44} with a {option45} {option46} and we will {option47}."""

    try:
        dinamic_text = subregion.dinamic_text.get(tag='subregion_destination')
    except DinamicText.DoesNotExist:
        dinamic_text = DinamicText()
    except DinamicText.MultipleObjectsReturned:
        dinamic_text = subregion.dinamic_text.filter(tag='subregion_destination')[0]
    dinamic_text.tag = 'subregion_destination'
    dinamic_text.content_object = subregion
    dinamic_text.first_text = text1.format(destination=subregion.alias,
                                           option1=option1[random.randint(0, len(option1) - 1)],
                                           option2=option2[random.randint(0, len(option2) - 1)],
                                           option3=option3[random.randint(0, len(option3) - 1)],
                                           option4=option4[random.randint(0, len(option4) - 1)],
                                           option5=option5[random.randint(0, len(option5) - 1)],
                                           option6=option6[random.randint(0, len(option6) - 1)],
                                           option7=option7[random.randint(0, len(option7) - 1)],
                                           option8=option8[random.randint(0, len(option8) - 1)],
                                           option9=option9[random.randint(0, len(option9) - 1)],
                                           option10=option10[random.randint(0, len(option10) - 1)],
                                           option11=option11[random.randint(0, len(option11) - 1)],
                                           option12=option12[random.randint(0, len(option12) - 1)],
                                           option13=option13[random.randint(0, len(option13) - 1)],
                                           option14=option14[random.randint(0, len(option14) - 1)],
                                           option15=option15[random.randint(0, len(option15) - 1)],
                                           option16=option16[random.randint(0, len(option16) - 1)],
                                           option17=option17[random.randint(0, len(option17) - 1)],
                                           option18=option18[random.randint(0, len(option18) - 1)],
                                           option19=option19[random.randint(0, len(option19) - 1)],
                                           option20=option20[random.randint(0, len(option20) - 1)],
                                           option21=option21[random.randint(0, len(option21) - 1)],
                                           option22=option22[random.randint(0, len(option22) - 1)],
                                           option23=option23[random.randint(0, len(option23) - 1)],
                                           option49=option49[random.randint(0, len(option49) - 1)])
    dinamic_text.second_text = text2.format(destination=subregion.alias,
                                            option24=option24[random.randint(0, len(option24) - 1)],
                                            option36=option36[random.randint(0, len(option36) - 1)],
                                            option25=option25[random.randint(0, len(option25) - 1)],
                                            option37=option37[random.randint(0, len(option37) - 1)],
                                            option26=option26[random.randint(0, len(option26) - 1)],
                                            option38=option38[random.randint(0, len(option38) - 1)],
                                            option27=option27[random.randint(0, len(option27) - 1)],
                                            option39=option39[random.randint(0, len(option39) - 1)],
                                            option28=option28[random.randint(0, len(option28) - 1)],
                                            option40=option40[random.randint(0, len(option40) - 1)],
                                            option29=option29[random.randint(0, len(option29) - 1)],
                                            option41=option41[random.randint(0, len(option41) - 1)],
                                            option30=option30[random.randint(0, len(option30) - 1)],
                                            option42=option42[random.randint(0, len(option42) - 1)],
                                            option31=option31[random.randint(0, len(option31) - 1)],
                                            option43=option43[random.randint(0, len(option43) - 1)],
                                            option32=option32[random.randint(0, len(option32) - 1)],
                                            option44=option44[random.randint(0, len(option44) - 1)],
                                            option33=option33[random.randint(0, len(option33) - 1)],
                                            option45=option45[random.randint(0, len(option45) - 1)],
                                            option34=option34[random.randint(0, len(option34) - 1)],
                                            option46=option46[random.randint(0, len(option46) - 1)],
                                            option35=option35[random.randint(0, len(option35) - 1)],
                                            option47=option47[random.randint(0, len(option47) - 1)],
                                            sort_by=generate_random_sort(),
                                            option48=option48[random.randint(0, len(option48) - 1)],
                                            email=EMAIL, phone=PHONE)

    cruiselines = subregion.getCruisesLines()
    list = generate_random_list(0, len(cruiselines))
    count = len(list) if (len(cruiselines) < 5) else 5
    for i in range(count):
        cruise_line = CruiseLine.objects.get(pk=cruiselines[list[i]])
        if i == 0:
            dinamic_text.cruise_line1 = '<a style="color: #fff" href="/cruiselines/{cruiseline_slug}/subregion/' \
                                        '{subregion}/">{name}</a>'.format(cruiseline_slug=cruise_line.slug,
                                                                          subregion=subregion.slug,
                                                                          name=cruise_line.name)
        elif i == 1:
            dinamic_text.cruise_line2 = '<a style="color: #fff" href="/cruiselines/{cruiseline_slug}/subregion/' \
                                        '{subregion}/">{name}</a>'.format(cruiseline_slug=cruise_line.slug,
                                                                          subregion=subregion.slug,
                                                                          name=cruise_line.name)
        elif i == 2:
            dinamic_text.cruise_line3 = '<a style="color: #fff" href="/cruiselines/{cruiseline_slug}/subregion/' \
                                        '{subregion}/">{name}</a>'.format(cruiseline_slug=cruise_line.slug,
                                                                          subregion=subregion.slug,
                                                                          name=cruise_line.name)
        elif i == 3:
            dinamic_text.cruise_line4 = '<a style="color: #fff" href="/cruiselines/{cruiseline_slug}/subregion/' \
                                        '{subregion}/">{name}</a>'.format(cruiseline_slug=cruise_line.slug,
                                                                          subregion=subregion.slug,
                                                                          name=cruise_line.name)
        else:
            dinamic_text.cruise_line5 = '<a style="color: #fff" href="/cruiselines/{cruiseline_slug}/subregion/' \
                                        '{subregion}/">{name}</a>'.format(cruiseline_slug=cruise_line.slug,
                                                                          subregion=subregion.slug,
                                                                          name=cruise_line.name)

    dinamic_text.save()

def generate_port_text_destination(port):
    option1 = ['has become', 'has always been', 'has proven to be', 'has developed into', 'has made itself into',
               'is known as', 'is definitely']
    option2 = ['top', 'highly sought-after', 'popular', 'must-see', 'featured', 'beloved']
    option3 = ['heading out', 'embarking', 'traveling', 'going']
    option4 = ['cruise', 'vacation', 'getaway']
    option5 = ['Featuring', 'Complete with', 'Bursting with']
    option6 = ['beautiful', 'spectacular', 'magnificent', 'stunning', 'exquisite']
    option7 = ['scenery', 'landscapes', 'views', 'ambiance']
    option8 = ['interesting', 'fascinating', 'captivating', 'inviting']
    option9 = ['charismatic', 'charming', 'delightful', 'lovable', 'enchanting']
    option10 = ['offers', 'provides', 'has', 'includes', 'contains']
    option11 = ['visitors', 'guests', 'newcomers', 'vacationers']
    option12 = ['pick from', 'choose from', 'arrange', 'plan', 'schedule']
    option13 = ['lots', 'a wide variety', 'an assortment', 'a wide range']
    option14 = ['trips', 'excursions', 'adventures', 'ventures']
    option15 = ['better', 'fully', 'further']
    option16 = ['see', 'witness', 'experience', 'enjoy', 'take part in', 'appreciate']
    option17 = ['Red Hot Cruises', 'RedHotCruises', 'RedHotCruises.com']
    option18 = ['offer', 'provide you with', 'have', 'allow you to choose from']
    option19 = ['a huge assortment', 'one of the largest selections', 'many types']
    option20 = ['aboard', 'with', 'through', 'via']
    option21 = ['world’s', 'industry’s']
    option22 = ['best', 'most beloved', 'most trusted', 'most popular', 'premier', 'most well-known', 'top',
                'most well-respected']
    option23 = ['companies', 'lines', 'providers', 'hosts', 'leaders']
    option49 = ['including', 'like', 'featuring']

    text1 = """
    {destination} {option1} a {option2} destination when {option3} on a {option4}. {option5} {option6} {option7}, {option8}
    cultures and {option9} people, {destination} {option10} something for everyone. With all cruises to {destination}, {option11}
    can {option12} {option13} of activities and shore {option14} to {option15} {option16} what {destination} has to offer.
    Here at {option17}, we {option18} {option19} of cruises to {destination} {option20} some of the {option21} {option22}
    cruise {option23} {option49}: """

    option24 = ['View','Check out','Browse','See']
    option48 = ['complete','full','entire','detailed']
    option25 = ['list','lineup','schedule','listing','catalog','calendar']
    option26 = ['available','possible','upcoming','forthcoming']
    #sort_by = ['date range,cruise line,length,ship,departure port']
    option27 = ['Once choosing','When you find','Once you see','When choosing','When selecting']
    option28 = ['perfect','ideal','best']
    option29 = ['view','check out','browse','see']
    option30 = ['more','additional','further']
    option31 = ['specifics','information']
    option32 = ['complete','full','entire','detailed']
    option33 = ['schedule','itinerary','agenda']
    option34 = ['ship','vessel','boat','yacht','sailing']
    option35 = ['a list of','the available','some featured']
    option36 = ['trips','excursions','adventures','ventures']
    option37 = ['Vacancy','Availability','Space']
    option38 = ['very','extremely','usually','often']
    option39 = ['purchase','book','buy','reserve','order']
    option40 = ['now','today','as soon as possible','quickly','without delay']
    option41 = ['ensure','lock in','grab']
    option42 = ['it’s too late','it sells out completely','they are gone','waiting too long']
    option43 = ['Give us a call','Call us','Get a hold of us','Contact us']
    option44 = ['speak','talk','consult']
    option45 = ['cruise','vacation']
    option46 = ['professional','specialist','expert','pro']
    option47 = ['guide you in the right direction','answer any questions you have','help you find what you’re looking for','find the perfect cruise for you']

    text2 = """
    {option24} our {option48} {option25} of all {option26} cruises to {destination} and sort by {sort_by}.
    {option27} the {option28} cruise, you can {option29} {option30} {option31} and the {option32} cruise {option33} as well
    as the {option34} details and {option35} shore {option36} in {destination}. {option37} is {option38} limited, so
    {option39} online with us {option40} to {option41} your {destination} cruise reservation before {option42}. {option43} at
    {phone} or {email} to {option44} with a {option45} {option46} and we will {option47}."""

    try:
        dinamic_text = port.dinamic_text.get(tag='port_destination')
    except DinamicText.DoesNotExist:
        dinamic_text = DinamicText()
    except DinamicText.MultipleObjectsReturned:
        dinamic_text = port.dinamic_text.filter(tag='port_destination')[0]
    dinamic_text.tag = 'port_destination'
    dinamic_text.content_object = port
    dinamic_text.first_text = text1.format(destination=port.alias,
                                           option1=option1[random.randint(0, len(option1) - 1)],
                                           option2=option2[random.randint(0, len(option2) - 1)],
                                           option3=option3[random.randint(0, len(option3) - 1)],
                                           option4=option4[random.randint(0, len(option4) - 1)],
                                           option5=option5[random.randint(0, len(option5) - 1)],
                                           option6=option6[random.randint(0, len(option6) - 1)],
                                           option7=option7[random.randint(0, len(option7) - 1)],
                                           option8=option8[random.randint(0, len(option8) - 1)],
                                           option9=option9[random.randint(0, len(option9) - 1)],
                                           option10=option10[random.randint(0, len(option10) - 1)],
                                           option11=option11[random.randint(0, len(option11) - 1)],
                                           option12=option12[random.randint(0, len(option12) - 1)],
                                           option13=option13[random.randint(0, len(option13) - 1)],
                                           option14=option14[random.randint(0, len(option14) - 1)],
                                           option15=option15[random.randint(0, len(option15) - 1)],
                                           option16=option16[random.randint(0, len(option16) - 1)],
                                           option17=option17[random.randint(0, len(option17) - 1)],
                                           option18=option18[random.randint(0, len(option18) - 1)],
                                           option19=option19[random.randint(0, len(option19) - 1)],
                                           option20=option20[random.randint(0, len(option20) - 1)],
                                           option21=option21[random.randint(0, len(option21) - 1)],
                                           option22=option22[random.randint(0, len(option22) - 1)],
                                           option23=option23[random.randint(0, len(option23) - 1)],
                                           option49=option49[random.randint(0, len(option49) - 1)])
    dinamic_text.second_text = text2.format(destination=port.alias,
                                            option24=option24[random.randint(0, len(option24) - 1)],
                                            option36=option36[random.randint(0, len(option36) - 1)],
                                            option25=option25[random.randint(0, len(option25) - 1)],
                                            option37=option37[random.randint(0, len(option37) - 1)],
                                            option26=option26[random.randint(0, len(option26) - 1)],
                                            option38=option38[random.randint(0, len(option38) - 1)],
                                            option27=option27[random.randint(0, len(option27) - 1)],
                                            option39=option39[random.randint(0, len(option39) - 1)],
                                            option28=option28[random.randint(0, len(option28) - 1)],
                                            option40=option40[random.randint(0, len(option40) - 1)],
                                            option29=option29[random.randint(0, len(option29) - 1)],
                                            option41=option41[random.randint(0, len(option41) - 1)],
                                            option30=option30[random.randint(0, len(option30) - 1)],
                                            option42=option42[random.randint(0, len(option42) - 1)],
                                            option31=option31[random.randint(0, len(option31) - 1)],
                                            option43=option43[random.randint(0, len(option43) - 1)],
                                            option32=option32[random.randint(0, len(option32) - 1)],
                                            option44=option44[random.randint(0, len(option44) - 1)],
                                            option33=option33[random.randint(0, len(option33) - 1)],
                                            option45=option45[random.randint(0, len(option45) - 1)],
                                            option34=option34[random.randint(0, len(option34) - 1)],
                                            option46=option46[random.randint(0, len(option46) - 1)],
                                            option35=option35[random.randint(0, len(option35) - 1)],
                                            option47=option47[random.randint(0, len(option47) - 1)],
                                            sort_by=generate_random_sort(),
                                            option48=option48[random.randint(0, len(option48) - 1)],
                                            email=EMAIL, phone=PHONE)

    travels = port.getTravelsDestination()
    texts = generating_travels_destination(travels)

    for i in range(len(texts)):
        # travel = Travel.objects.get(pk=travels[list[i]])
        if i == 0:
            dinamic_text.cruise_line1 = texts[i]
        elif i == 1:
            dinamic_text.cruise_line2 = texts[i]
        elif i == 2:
            dinamic_text.cruise_line3 = texts[i]
        elif i == 3:
            dinamic_text.cruise_line4 = texts[i]
        else:
            dinamic_text.cruise_line5 = texts[i]

    dinamic_text.save()

def generate_port_text_departure(port):
    option1 = ['Searching for','Looking for','Seeking','Need a list of']
    option2 = ['Red Hot Cruises','RedHotCruises','RedHotCruises.com']
    option3 = ['offers','provides','has','includes','contains']
    option4 = ['a huge assortment','one of the largest selections','many types']
    option5 = ['rock bottom','super low','unbeatable','fantastic','great']
    option6 = ['rates','prices']
    option7 = ['arriving','coming','traveling','vacationing']
    option8 = ['out of town','somewhere else','another location','a different place','another place','a different location']
    option9 = ['close','local','in the area','in town']
    option10 = ['a perfect','a great','an ideal','an excellent','a popular']
    option11 = ['location','place','spot','choice']
    option12 = ['begin','head out on','start','embark on','set sail on','leave on']
    option13 = ['using','with','aboard','through','via']
    option14 = ['best','most beloved','most trusted','most popular','premier','most well-known','top','most well-respected']
    option15 = ['companies','lines','providers','professionals','hosts','leaders']
    option16 = ['including','like','featuring']

    text1 = """ {option1} cruises departing from {departure}? {option2} {option3} {option4} of cruises leaving from
    {departure} at {option5} {option6}. Whether you are {option7} from {option8} or already {option9}, {departure} is
    {option10} {option11} to {option12} a cruise {option13} some of the industry’s {option14} cruise {option15} {option16}: """

    option17 = ['View','Check out','Browse','See']
    option18 = ['complete','full','entire','detailed']
    option19 = ['list','lineup','schedule','listing','catalog','calendar']
    option20 = ['available','possible','upcoming','forthcoming']
    option21 = ['Once choosing','When you find','Once you see','When choosing','When selecting']
    option22 = ['perfect','ideal','best']
    option23 = ['view','check out','browse','see']
    option24 = ['more','additional','further']
    option25 = ['specifics','information']
    option26 = ['complete','full','entire','detailed']
    option27 = ['schedule','itinerary','agenda']
    option28 = ['ship','vessel','boat','yacht','sailing']
    option29 = ['a list of','the available','some featured']
    option30 = ['trips','excursions','adventures','ventures']
    option31 = ['Vacancy','Availability','Space']
    option32 = ['very','extremely','usually','often']
    option33 = ['purchase','book','buy','reserve','order']
    option34 = ['now','today','as soon as possible','quickly','without delay']
    option35 = ['ensure','lock in','grab']
    option36 = ['it’s too late','it sells out completely','they are gone','waiting too long']
    option37 = ['Give us a call','Call us','Get a hold of us','Contact us']
    option38 = ['speak','talk','consult']
    option39 = ['cruise','vacation']
    option40 = ['professional','specialist','expert','pro']
    option41 = ['guide you in the right direction','answer any questions you have','help you find what you’re looking for','find the perfect cruise for you']


    text2 = """ {option17} our {option18} {option19} of all {option20} cruises from {departure} below and sort by {sort_by}.
    {option21} the {option22} cruise from {departure}, you can {option23} {option24} {option25} and the {option26} cruise
    {option27} as well as the {option28} details and {option29} shore {option30}. {option31} is {option32} limited, so
    {option33} online with us {option34} to {option35} your {departure} cruise reservation before {option36}. {option37}
    at {phone} or {email} to {option38} with a {option39} {option40} and we will {option41}. """

    try:
        dinamic_text = port.dinamic_text.get(tag='port_departure')
    except DinamicText.DoesNotExist:
        dinamic_text = DinamicText()
    except DinamicText.MultipleObjectsReturned:
        dinamic_text = port.dinamic_text.filter(tag='port_departure')[0]
    dinamic_text.tag = 'port_departure'
    dinamic_text.content_object = port
    dinamic_text.first_text = text1.format(departure=port.alias,
                                           option1=option1[random.randint(0, len(option1) - 1)],
                                           option2=option2[random.randint(0, len(option2) - 1)],
                                           option3=option3[random.randint(0, len(option3) - 1)],
                                           option4=option4[random.randint(0, len(option4) - 1)],
                                           option5=option5[random.randint(0, len(option5) - 1)],
                                           option6=option6[random.randint(0, len(option6) - 1)],
                                           option7=option7[random.randint(0, len(option7) - 1)],
                                           option8=option8[random.randint(0, len(option8) - 1)],
                                           option9=option9[random.randint(0, len(option9) - 1)],
                                           option10=option10[random.randint(0, len(option10) - 1)],
                                           option11=option11[random.randint(0, len(option11) - 1)],
                                           option12=option12[random.randint(0, len(option12) - 1)],
                                           option13=option13[random.randint(0, len(option13) - 1)],
                                           option14=option14[random.randint(0, len(option14) - 1)],
                                           option15=option15[random.randint(0, len(option15) - 1)],
                                           option16=option16[random.randint(0, len(option16) - 1)])
    dinamic_text.second_text = text2.format(departure=port.alias,
                                            option17=option17[random.randint(0, len(option17) - 1)],
                                            option18=option18[random.randint(0, len(option18) - 1)],
                                            option19=option19[random.randint(0, len(option19) - 1)],
                                            option20=option20[random.randint(0, len(option20) - 1)],
                                            option21=option21[random.randint(0, len(option21) - 1)],
                                            option22=option22[random.randint(0, len(option22) - 1)],
                                            option23=option23[random.randint(0, len(option23) - 1)],
                                            option24=option24[random.randint(0, len(option24) - 1)],
                                            option36=option36[random.randint(0, len(option36) - 1)],
                                            option25=option25[random.randint(0, len(option25) - 1)],
                                            option37=option37[random.randint(0, len(option37) - 1)],
                                            option26=option26[random.randint(0, len(option26) - 1)],
                                            option38=option38[random.randint(0, len(option38) - 1)],
                                            option27=option27[random.randint(0, len(option27) - 1)],
                                            option39=option39[random.randint(0, len(option39) - 1)],
                                            option28=option28[random.randint(0, len(option28) - 1)],
                                            option40=option40[random.randint(0, len(option40) - 1)],
                                            option29=option29[random.randint(0, len(option29) - 1)],
                                            option41=option41[random.randint(0, len(option41) - 1)],
                                            option30=option30[random.randint(0, len(option30) - 1)],
                                            option31=option31[random.randint(0, len(option31) - 1)],
                                            option32=option32[random.randint(0, len(option32) - 1)],
                                            option33=option33[random.randint(0, len(option33) - 1)],
                                            option34=option34[random.randint(0, len(option34) - 1)],
                                            option35=option35[random.randint(0, len(option35) - 1)],
                                            sort_by=generate_random_sort(),
                                            email=EMAIL, phone=PHONE)

    travels = port.getTravelsDeparture()
    texts = generating_travels_departure(travels, port)
    for i in range(len(texts)):
        if i == 0:
            dinamic_text.cruise_line1 = texts[i]
        elif i == 1:
            dinamic_text.cruise_line2 = texts[i]
        elif i == 2:
            dinamic_text.cruise_line3 = texts[i]
        elif i == 3:
            dinamic_text.cruise_line4 = texts[i]
        else:
            dinamic_text.cruise_line5 = texts[i]

    dinamic_text.save()

def generate_cruiseline_text(cruiseline):
    option1 = ['has become','has always been','has proven to be','has developed into','has made itself into','is known as','is definitely']
    option2 = ['best','most beloved','most trusted','most popular','premier','most well-known','top','most well-respected']
    option3 = ['companies','lines','providers','hosts','leaders']
    option4 = ['world','industry']
    option5 = ['Head out on','Embark on','Set sail on','Leave on']
    option6 = ['cruise','vacation','getaway']
    option7 = ['enjoy','expect','get','receive','find']
    option8 = ['a slew of','only the best in','tons of','many types of']
    option9 = ['luxury','extravagant','delightful','lavish','generous']
    option10 = ['terrific','helpful','pampering','catering','favoring','delightful']
    option11 = ['fantastic','extraordinary','uncommon','special','outstanding','incredible','impressive','stunning','spectacular']
    option12 = ['Red Hot Cruises','RedHotCruises','RedHotCruises.com']
    option13 = ['offers','provides','has','includes','contains']
    option14 = ['a huge assortment','one of the largest selections','many types']
    option15 = ['rock bottom','super low','unbeatable','fantastic','great']
    option16 = ['rates','prices']
    option17 = ['Here','Below','On this page']
    option18 = ['view','check out','browse','see']
    option19 = ['Vacancy','Availability','Space']
    option20 = ['very','extremely','usually','often']
    option21 = ['purchase','book','buy','reserve','order']
    option22 = ['now','today','as soon as possible','quickly','without delay']
    option23 = ['ensure','lock in','grab']
    option24 = ['it’s too late','it sells out completely','they are gone','waiting too long']
    option25 = ['Give us a call','Call us','Get a hold of us','Contact us']
    option26 = ['speak','talk','consult']
    option27 = ['cruise','vacation']
    option28 = ['professional','specialist','expert','pro']
    option29 = ['guide you in the right direction','answer any questions you have','help you find what you’re looking for','find the perfect cruise for you']

    text1 = """ {cruiseline} {option1} one of the {option2} cruise {option3} in the {option4}. {option5} a {option6} with
    {cruiseline} and {option7} {option8} {option9} on-board amenities, a {option10} service staff, and a truly {option11}
    fleet. {option12} {option13} {option14} of cruises with {cruiseline} at {option15} {option16}. {option17}, you can
    {option18} all destinations, departures and ships available with {cruiseline}. {option19} is {option20} limited, so
    {option21} online with us {option22} to {option23} your cruise with {cruiseline} before {option24}. {option25} at
    {phone} or {email} to {option26} with a {option27} {option28} and we will {option29}."""

    try:
        dinamic_text = cruiseline.dinamic_text.get(tag='cruiseline_lines')
    except DinamicText.DoesNotExist:
        dinamic_text = DinamicText()
    except DinamicText.MultipleObjectsReturned:
        dinamic_text = cruiseline.dinamic_text.filter(tag='cruiseline_lines')[0]
    dinamic_text.tag = 'cruiseline_lines'
    dinamic_text.content_object = cruiseline
    dinamic_text.first_text = text1.format(cruiseline=cruiseline.name,
                                           option1=option1[random.randint(0, len(option1) - 1)],
                                           option2=option2[random.randint(0, len(option2) - 1)],
                                           option3=option3[random.randint(0, len(option3) - 1)],
                                           option4=option4[random.randint(0, len(option4) - 1)],
                                           option5=option5[random.randint(0, len(option5) - 1)],
                                           option6=option6[random.randint(0, len(option6) - 1)],
                                           option7=option7[random.randint(0, len(option7) - 1)],
                                           option8=option8[random.randint(0, len(option8) - 1)],
                                           option9=option9[random.randint(0, len(option9) - 1)],
                                           option10=option10[random.randint(0, len(option10) - 1)],
                                           option11=option11[random.randint(0, len(option11) - 1)],
                                           option12=option12[random.randint(0, len(option12) - 1)],
                                           option13=option13[random.randint(0, len(option13) - 1)],
                                           option14=option14[random.randint(0, len(option14) - 1)],
                                           option15=option15[random.randint(0, len(option15) - 1)],
                                           option16=option16[random.randint(0, len(option16) - 1)],
                                           option17=option17[random.randint(0, len(option17) - 1)],
                                           option18=option18[random.randint(0, len(option18) - 1)],
                                           option19=option19[random.randint(0, len(option19) - 1)],
                                           option20=option20[random.randint(0, len(option20) - 1)],
                                           option21=option21[random.randint(0, len(option21) - 1)],
                                           option22=option22[random.randint(0, len(option22) - 1)],
                                           option23=option23[random.randint(0, len(option23) - 1)],
                                           option24=option24[random.randint(0, len(option24) - 1)],
                                           option25=option25[random.randint(0, len(option25) - 1)],
                                           option26=option26[random.randint(0, len(option26) - 1)],
                                           option27=option27[random.randint(0, len(option27) - 1)],
                                           option28=option28[random.randint(0, len(option28) - 1)],
                                           option29=option29[random.randint(0, len(option29) - 1)],
                                           email=EMAIL, phone=PHONE)

    dinamic_text.save()

def generate_ship_text(ship):
    option1 = ['has become','has always been','has proven to be','has developed into','has made itself into','is known as','is definitely']
    option2 = ['best','most beloved','most trusted','most popular','premier','most well-known','top','most well-respected']
    option3 = ['companies','lines','providers','hosts','leaders']
    option4 = ['world','industry']
    option5 = ['Head out on','Embark on','Set sail on','Leave on']
    option6 = ['cruise','vacation','getaway']
    option7 = ['enjoy','expect','get','receive','find']
    option8 = ['a slew of','only the best in','tons of','many types of']
    option9 = ['luxury','extravagant','delightful','lavish','generous']
    option10 = ['terrific','helpful','pampering','catering','favoring','delightful']
    option11 = ['fantastic','extraordinary','uncommon','special','outstanding','incredible','impressive','stunning','spectacular']
    option12 = ['Red Hot Cruises','RedHotCruises','RedHotCruises.com']
    option13 = ['offers','provides','has','includes','contains']
    option14 = ['a huge assortment','one of the largest selections','many types']
    option15 = ['rock bottom','super low','unbeatable','fantastic','great']
    option16 = ['rates','prices']
    option17 = ['View','Check out','Browse','See']
    option18 = ['complete','full','entire','detailed']
    option19 = ['list','lineup','schedule','listing','catalog','calendar']
    option20 = ['available','possible','upcoming','forthcoming']
    option21 = ['Once choosing','When you find','Once you see','When choosing','When selecting']
    option22 = ['perfect','ideal','best']
    option23 = ['view','check out','browse','see']
    option24 = ['more','additional','further']
    option25 = ['specifics','information']
    option26 = ['complete','full','entire','detailed']
    option27 = ['schedule','itinerary','agenda']
    option28 = ['ship','vessel','boat','yacht','sailing']
    option29 = ['a list of','the available','some featured']
    option30 = ['trips','excursions','adventures','ventures']
    option31 = ['Vacancy','Availability','Space']
    option32 = ['very','extremely','usually','often']
    option33 = ['purchase','book','buy','reserve','order']
    option34 = ['now','today','as soon as possible','quickly','without delay']
    option35 = ['ensure','lock in','grab']
    option36 = ['it’s too late','it sells out completely','they are gone','waiting too long']
    option37 = ['Give us a call','Call us','Get a hold of us','Contact us']
    option38 = ['speak','talk','consult']
    option39 = ['cruise','vacation']
    option40 = ['professional','specialist','expert','pro']
    option41 = ['guide you in the right direction','answer any questions you have','help you find what you’re looking for','find the perfect cruise for you']


    text1 = """{cruiseline} {option1} one of the {option2} cruise {option3} in the {option4}, and what better way to
    cruise aboard the {ship}. {option5} a {option6} with {cruiseline} and {option7} {option8} {option9} on-board
    amenities and a {option10} service staff aboard the truly {option11} {ship}. {option12} {option13} {option14} of
    {ship} cruises at {option15} {option16}. {option17} our {option18} {option19} of all {option20} {ship} cruises below
    and sort by {sort_by}. {option21} the {option22} cruise aboard the {ship}, you can {option23} {option24} {option25}
    and the {option26} cruise {option27} as well as the {option28} details and {option29} shore {option30}. {option31}
    is {option32} limited, so {option33} online with us {option34} to {option35} your {ship} cruise reservation before
    {option36}. {option37} at {phone} or {email} to {option38} with a {option39} {option40} and we will {option41}."""

    try:
        dinamic_text = ship.dinamic_text.get(tag='ship_cruiseline')
    except DinamicText.DoesNotExist:
        dinamic_text = DinamicText()
    except DinamicText.MultipleObjectsReturned:
        dinamic_text = ship.dinamic_text.filter(tag='ship_cruiseline')[0]
    dinamic_text.tag = 'ship_cruiseline'
    dinamic_text.content_object = ship
    dinamic_text.first_text = text1.format(cruiseline=ship.cruise_line.name, ship=ship.name,
                                           option1=option1[random.randint(0, len(option1) - 1)],
                                           option2=option2[random.randint(0, len(option2) - 1)],
                                           option3=option3[random.randint(0, len(option3) - 1)],
                                           option4=option4[random.randint(0, len(option4) - 1)],
                                           option5=option5[random.randint(0, len(option5) - 1)],
                                           option6=option6[random.randint(0, len(option6) - 1)],
                                           option7=option7[random.randint(0, len(option7) - 1)],
                                           option8=option8[random.randint(0, len(option8) - 1)],
                                           option9=option9[random.randint(0, len(option9) - 1)],
                                           option10=option10[random.randint(0, len(option10) - 1)],
                                           option11=option11[random.randint(0, len(option11) - 1)],
                                           option12=option12[random.randint(0, len(option12) - 1)],
                                           option13=option13[random.randint(0, len(option13) - 1)],
                                           option14=option14[random.randint(0, len(option14) - 1)],
                                           option15=option15[random.randint(0, len(option15) - 1)],
                                           option16=option16[random.randint(0, len(option16) - 1)],
                                           option17=option17[random.randint(0, len(option17) - 1)],
                                           option18=option18[random.randint(0, len(option18) - 1)],
                                           option19=option19[random.randint(0, len(option19) - 1)],
                                           option20=option20[random.randint(0, len(option20) - 1)],
                                           option21=option21[random.randint(0, len(option21) - 1)],
                                           option22=option22[random.randint(0, len(option22) - 1)],
                                           option23=option23[random.randint(0, len(option23) - 1)],
                                           option24=option24[random.randint(0, len(option24) - 1)],
                                           option36=option36[random.randint(0, len(option36) - 1)],
                                           option25=option25[random.randint(0, len(option25) - 1)],
                                           option37=option37[random.randint(0, len(option37) - 1)],
                                           option26=option26[random.randint(0, len(option26) - 1)],
                                           option38=option38[random.randint(0, len(option38) - 1)],
                                           option27=option27[random.randint(0, len(option27) - 1)],
                                           option39=option39[random.randint(0, len(option39) - 1)],
                                           option28=option28[random.randint(0, len(option28) - 1)],
                                           option40=option40[random.randint(0, len(option40) - 1)],
                                           option29=option29[random.randint(0, len(option29) - 1)],
                                           option41=option41[random.randint(0, len(option41) - 1)],
                                           option30=option30[random.randint(0, len(option30) - 1)],
                                           option31=option31[random.randint(0, len(option31) - 1)],
                                           option32=option32[random.randint(0, len(option32) - 1)],
                                           option33=option33[random.randint(0, len(option33) - 1)],
                                           option34=option34[random.randint(0, len(option34) - 1)],
                                           option35=option35[random.randint(0, len(option35) - 1)],
                                           sort_by=generate_random_sort(),
                                           email=EMAIL, phone=PHONE)

    dinamic_text.save()

# Added crons to fix some bugs 10-08-2016

#@cronjobs.register
def generate_empty_alias():
    subregions = SubRegion.objects.all()
    for subregion in subregions:
        if subregion.alias is None:
            subregion.alias = subregion.name
            print subregion.alias
            subregion.save()

    ports = Port.objects.all()
    for port in ports:
        if port.alias is None:
            port.alias = port.name
            print port.alias
            port.save()

#@cronjobs.register
def destination_empty_text():
    print 'generating destination_text'
    option1 = ['has become', 'has always been', 'has proven to be', 'has developed into', 'has made itself into',
               'is known as', 'is definitely']
    option2 = ['top', 'highly sought-after', 'popular', 'must-see', 'featured', 'beloved']
    option3 = ['heading out', 'embarking', 'traveling', 'going']
    option4 = ['cruise', 'vacation', 'getaway']
    option5 = ['Featuring', 'Complete with', 'Bursting with']
    option6 = ['beautiful', 'spectacular', 'magnificent', 'stunning', 'exquisite']
    option7 = ['scenery', 'landscapes', 'views', 'ambiance']
    option8 = ['interesting', 'fascinating', 'captivating', 'inviting']
    option9 = ['charismatic', 'charming', 'delightful', 'lovable', 'enchanting']
    option10 = ['offers', 'provides', 'has', 'includes', 'contains']
    option11 = ['visitors', 'guests', 'newcomers', 'vacationers']
    option12 = ['pick from', 'choose from', 'arrange', 'plan', 'schedule']
    option13 = ['lots', 'a wide variety', 'an assortment', 'a wide range']
    option14 = ['trips', 'excursions', 'adventures', 'ventures']
    option15 = ['better', 'fully', 'further']
    option16 = ['see', 'witness', 'experience', 'enjoy', 'take part in', 'appreciate']
    option17 = ['Red Hot Cruises', 'RedHotCruises', 'RedHotCruises.com']
    option18 = ['offer', 'provide you with', 'have', 'allow you to choose from']
    option19 = ['a huge assortment', 'one of the largest selections', 'many types']
    option20 = ['aboard', 'with', 'through', 'via']
    option21 = ['world’s', 'industry’s']
    option22 = ['best', 'most beloved', 'most trusted', 'most popular', 'premier', 'most well-known', 'top',
                'most well-respected']
    option23 = ['companies', 'lines', 'providers', 'hosts', 'leaders']
    option49 = ['including', 'like', 'featuring']

    text1 = """
    {destination} {option1} a {option2} destination when {option3} on a {option4}. {option5} {option6} {option7}, {option8}
    cultures and {option9} people, {destination} {option10} something for everyone. With all cruises to {destination}, {option11}
    can {option12} {option13} of activities and shore {option14} to {option15} {option16} what {destination} has to offer.
    Here at {option17}, we {option18} {option19} of cruises to {destination} {option20} some of the {option21} {option22}
    cruise {option23} {option49}: """

    option24 = ['View','Check out','Browse','See']
    option48 = ['complete','full','entire','detailed']
    option25 = ['list','lineup','schedule','listing','catalog','calendar']
    option26 = ['available','possible','upcoming','forthcoming']
    #sort_by = ['date range,cruise line,length,ship,departure port']
    option27 = ['Once choosing','When you find','Once you see','When choosing','When selecting']
    option28 = ['perfect','ideal','best']
    option29 = ['view','check out','browse','see']
    option30 = ['more','additional','further']
    option31 = ['specifics','information']
    option32 = ['complete','full','entire','detailed']
    option33 = ['schedule','itinerary','agenda']
    option34 = ['ship','vessel','boat','yacht','sailing']
    option35 = ['a list of','the available','some featured']
    option36 = ['trips','excursions','adventures','ventures']
    option37 = ['Vacancy','Availability','Space']
    option38 = ['very','extremely','usually','often']
    option39 = ['purchase','book','buy','reserve','order']
    option40 = ['now','today','as soon as possible','quickly','without delay']
    option41 = ['ensure','lock in','grab']
    option42 = ['it’s too late','it sells out completely','they are gone','waiting too long']
    option43 = ['Give us a call','Call us','Get a hold of us','Contact us']
    option44 = ['speak','talk','consult']
    option45 = ['cruise','vacation']
    option46 = ['professional','specialist','expert','pro']
    option47 = ['guide you in the right direction','answer any questions you have','help you find what you’re looking for','find the perfect cruise for you']

    text2 = """
    {option24} our {option48} {option25} of all {option26} cruises to {destination} and sort by {sort_by}.
    {option27} the {option28} cruise, you can {option29} {option30} {option31} and the {option32} cruise {option33} as well
    as the {option34} details and {option35} shore {option36} in {destination}. {option37} is {option38} limited, so
    {option39} online with us {option40} to {option41} your {destination} cruise reservation before {option42}. {option43} at
    {phone} or {email} to {option44} with a {option45} {option46} and we will {option47}."""

    subregions = SubRegion.objects.all()
    for subregion in subregions:
        print 'generating subregions'
        try:
            dinamic_text = subregion.dinamic_text.get(tag='subregion_destination')
        except DinamicText.DoesNotExist:
            dinamic_text = DinamicText()
            print subregion.name
            dinamic_text.tag = 'subregion_destination'
            dinamic_text.content_object = subregion
            dinamic_text.first_text = text1.format(destination=subregion.alias,
                                                        option1=option1[random.randint(0,len(option1)-1)],option2=option2[random.randint(0,len(option2)-1)],
                                                        option3=option3[random.randint(0,len(option3)-1)],option4=option4[random.randint(0,len(option4)-1)],
                                                        option5=option5[random.randint(0,len(option5)-1)],option6=option6[random.randint(0,len(option6)-1)],
                                                        option7=option7[random.randint(0,len(option7)-1)],option8=option8[random.randint(0,len(option8)-1)],
                                                        option9=option9[random.randint(0,len(option9)-1)],option10=option10[random.randint(0,len(option10)-1)],
                                                        option11=option11[random.randint(0,len(option11)-1)],option12=option12[random.randint(0,len(option12)-1)],
                                                        option13=option13[random.randint(0,len(option13)-1)],option14=option14[random.randint(0,len(option14)-1)],
                                                        option15=option15[random.randint(0,len(option15)-1)],option16=option16[random.randint(0,len(option16)-1)],
                                                        option17=option17[random.randint(0,len(option17)-1)],option18=option18[random.randint(0,len(option18)-1)],
                                                        option19=option19[random.randint(0,len(option19)-1)],option20=option20[random.randint(0,len(option20)-1)],
                                                        option21=option21[random.randint(0,len(option21)-1)],option22=option22[random.randint(0,len(option22)-1)],
                                                        option23=option23[random.randint(0,len(option23)-1)],option49=option49[random.randint(0,len(option49)-1)])
            dinamic_text.second_text = text2.format(destination=subregion.alias,
                                                    option24=option24[random.randint(0,len(option24)-1)],option36=option36[random.randint(0,len(option36)-1)],
                                                    option25=option25[random.randint(0,len(option25)-1)],option37=option37[random.randint(0,len(option37)-1)],
                                                    option26=option26[random.randint(0,len(option26)-1)],option38=option38[random.randint(0,len(option38)-1)],
                                                    option27=option27[random.randint(0,len(option27)-1)],option39=option39[random.randint(0,len(option39)-1)],
                                                    option28=option28[random.randint(0,len(option28)-1)],option40=option40[random.randint(0,len(option40)-1)],
                                                    option29=option29[random.randint(0,len(option29)-1)],option41=option41[random.randint(0,len(option41)-1)],
                                                    option30=option30[random.randint(0,len(option30)-1)],option42=option42[random.randint(0,len(option42)-1)],
                                                    option31=option31[random.randint(0,len(option31)-1)],option43=option43[random.randint(0,len(option43)-1)],
                                                    option32=option32[random.randint(0,len(option32)-1)],option44=option44[random.randint(0,len(option44)-1)],
                                                    option33=option33[random.randint(0,len(option33)-1)],option45=option45[random.randint(0,len(option45)-1)],
                                                    option34=option34[random.randint(0,len(option34)-1)],option46=option46[random.randint(0,len(option46)-1)],
                                                    option35=option35[random.randint(0,len(option35)-1)],option47=option47[random.randint(0,len(option47)-1)],
                                                    sort_by=generate_random_sort(),option48=option48[random.randint(0,len(option48)-1)],
                                                    email=EMAIL, phone=PHONE)

            cruiselines = subregion.getCruisesLines()
            list = generate_random_list(0, len(cruiselines))
            count = len(list) if (len(cruiselines) < 5) else 5
            for i in range(count):
                cruise_line = CruiseLine.objects.get(pk=cruiselines[list[i]])
                if i == 0:
                     dinamic_text.cruise_line1 = '<a style="color: #fff" href="/cruiselines/{cruiseline_slug}/subregion/' \
                     '{subregion}/">{name}</a>'.format(cruiseline_slug=cruise_line.slug,
                                            subregion=subregion.slug,name=cruise_line.name)
                elif i == 1:
                    dinamic_text.cruise_line2 = '<a style="color: #fff" href="/cruiselines/{cruiseline_slug}/subregion/' \
                     '{subregion}/">{name}</a>'.format(cruiseline_slug=cruise_line.slug,
                                            subregion=subregion.slug,name=cruise_line.name)
                elif i == 2:
                    dinamic_text.cruise_line3 = '<a style="color: #fff" href="/cruiselines/{cruiseline_slug}/subregion/' \
                     '{subregion}/">{name}</a>'.format(cruiseline_slug=cruise_line.slug,
                                            subregion=subregion.slug,name=cruise_line.name)
                elif i == 3:
                    dinamic_text.cruise_line4 = '<a style="color: #fff" href="/cruiselines/{cruiseline_slug}/subregion/' \
                     '{subregion}/">{name}</a>'.format(cruiseline_slug=cruise_line.slug,
                                            subregion=subregion.slug,name=cruise_line.name)
                else:
                    dinamic_text.cruise_line5 = '<a style="color: #fff" href="/cruiselines/{cruiseline_slug}/subregion/' \
                     '{subregion}/">{name}</a>'.format(cruiseline_slug=cruise_line.slug,
                                            subregion=subregion.slug,name=cruise_line.name)

            dinamic_text.save()

    ports = Port.objects.all()
    for port in ports:
        print 'generating ports destinations'
        try:
            dinamic_text = port.dinamic_text.get(tag='port_destination')
        except DinamicText.DoesNotExist:
            dinamic_text = DinamicText()
            print port.name
            dinamic_text.tag = 'port_destination'
            dinamic_text.content_object = port
            dinamic_text.first_text = text1.format(destination=port.alias,
                                                   option1=option1[random.randint(0, len(option1) - 1)],
                                                   option2=option2[random.randint(0, len(option2) - 1)],
                                                   option3=option3[random.randint(0, len(option3) - 1)],
                                                   option4=option4[random.randint(0, len(option4) - 1)],
                                                   option5=option5[random.randint(0, len(option5) - 1)],
                                                   option6=option6[random.randint(0, len(option6) - 1)],
                                                   option7=option7[random.randint(0, len(option7) - 1)],
                                                   option8=option8[random.randint(0, len(option8) - 1)],
                                                   option9=option9[random.randint(0, len(option9) - 1)],
                                                   option10=option10[random.randint(0, len(option10) - 1)],
                                                   option11=option11[random.randint(0, len(option11) - 1)],
                                                   option12=option12[random.randint(0, len(option12) - 1)],
                                                   option13=option13[random.randint(0, len(option13) - 1)],
                                                   option14=option14[random.randint(0, len(option14) - 1)],
                                                   option15=option15[random.randint(0, len(option15) - 1)],
                                                   option16=option16[random.randint(0, len(option16) - 1)],
                                                   option17=option17[random.randint(0, len(option17) - 1)],
                                                   option18=option18[random.randint(0, len(option18) - 1)],
                                                   option19=option19[random.randint(0, len(option19) - 1)],
                                                   option20=option20[random.randint(0, len(option20) - 1)],
                                                   option21=option21[random.randint(0, len(option21) - 1)],
                                                   option22=option22[random.randint(0, len(option22) - 1)],
                                                   option23=option23[random.randint(0, len(option23) - 1)],
                                                   option49=option49[random.randint(0, len(option49) - 1)])
            dinamic_text.second_text = text2.format(destination=port.alias,
                                                    option24=option24[random.randint(0, len(option24) - 1)],
                                                    option36=option36[random.randint(0, len(option36) - 1)],
                                                    option25=option25[random.randint(0, len(option25) - 1)],
                                                    option37=option37[random.randint(0, len(option37) - 1)],
                                                    option26=option26[random.randint(0, len(option26) - 1)],
                                                    option38=option38[random.randint(0, len(option38) - 1)],
                                                    option27=option27[random.randint(0, len(option27) - 1)],
                                                    option39=option39[random.randint(0, len(option39) - 1)],
                                                    option28=option28[random.randint(0, len(option28) - 1)],
                                                    option40=option40[random.randint(0, len(option40) - 1)],
                                                    option29=option29[random.randint(0, len(option29) - 1)],
                                                    option41=option41[random.randint(0, len(option41) - 1)],
                                                    option30=option30[random.randint(0, len(option30) - 1)],
                                                    option42=option42[random.randint(0, len(option42) - 1)],
                                                    option31=option31[random.randint(0, len(option31) - 1)],
                                                    option43=option43[random.randint(0, len(option43) - 1)],
                                                    option32=option32[random.randint(0, len(option32) - 1)],
                                                    option44=option44[random.randint(0, len(option44) - 1)],
                                                    option33=option33[random.randint(0, len(option33) - 1)],
                                                    option45=option45[random.randint(0, len(option45) - 1)],
                                                    option34=option34[random.randint(0, len(option34) - 1)],
                                                    option46=option46[random.randint(0, len(option46) - 1)],
                                                    option35=option35[random.randint(0, len(option35) - 1)],
                                                    option47=option47[random.randint(0, len(option47) - 1)],
                                                    sort_by=generate_random_sort(),
                                                    option48=option48[random.randint(0, len(option48) - 1)],
                                                    email=EMAIL, phone=PHONE)

            travels = port.getTravelsDestination()
            texts = generating_travels_destination(travels)

            for i in range(len(texts)):
                #travel = Travel.objects.get(pk=travels[list[i]])
                if i == 0:
                     dinamic_text.cruise_line1 = texts[i]
                elif i == 1:
                    dinamic_text.cruise_line2 = texts[i]
                elif i == 2:
                    dinamic_text.cruise_line3 = texts[i]
                elif i == 3:
                    dinamic_text.cruise_line4 = texts[i]
                else:
                    dinamic_text.cruise_line5 = texts[i]

            dinamic_text.save()

#@cronjobs.register
def departure_empty_text():
    print 'generating departure_text'
    option1 = ['Searching for','Looking for','Seeking','Need a list of']
    option2 = ['Red Hot Cruises','RedHotCruises','RedHotCruises.com']
    option3 = ['offers','provides','has','includes','contains']
    option4 = ['a huge assortment','one of the largest selections','many types']
    option5 = ['rock bottom','super low','unbeatable','fantastic','great']
    option6 = ['rates','prices']
    option7 = ['arriving','coming','traveling','vacationing']
    option8 = ['out of town','somewhere else','another location','a different place','another place','a different location']
    option9 = ['close','local','in the area','in town']
    option10 = ['a perfect','a great','an ideal','an excellent','a popular']
    option11 = ['location','place','spot','choice']
    option12 = ['begin','head out on','start','embark on','set sail on','leave on']
    option13 = ['using','with','aboard','through','via']
    option14 = ['best','most beloved','most trusted','most popular','premier','most well-known','top','most well-respected']
    option15 = ['companies','lines','providers','professionals','hosts','leaders']
    option16 = ['including','like','featuring']

    text1 = """ {option1} cruises departing from {departure}? {option2} {option3} {option4} of cruises leaving from
    {departure} at {option5} {option6}. Whether you are {option7} from {option8} or already {option9}, {departure} is
    {option10} {option11} to {option12} a cruise {option13} some of the industry’s {option14} cruise {option15} {option16}: """

    option17 = ['View','Check out','Browse','See']
    option18 = ['complete','full','entire','detailed']
    option19 = ['list','lineup','schedule','listing','catalog','calendar']
    option20 = ['available','possible','upcoming','forthcoming']
    option21 = ['Once choosing','When you find','Once you see','When choosing','When selecting']
    option22 = ['perfect','ideal','best']
    option23 = ['view','check out','browse','see']
    option24 = ['more','additional','further']
    option25 = ['specifics','information']
    option26 = ['complete','full','entire','detailed']
    option27 = ['schedule','itinerary','agenda']
    option28 = ['ship','vessel','boat','yacht','sailing']
    option29 = ['a list of','the available','some featured']
    option30 = ['trips','excursions','adventures','ventures']
    option31 = ['Vacancy','Availability','Space']
    option32 = ['very','extremely','usually','often']
    option33 = ['purchase','book','buy','reserve','order']
    option34 = ['now','today','as soon as possible','quickly','without delay']
    option35 = ['ensure','lock in','grab']
    option36 = ['it’s too late','it sells out completely','they are gone','waiting too long']
    option37 = ['Give us a call','Call us','Get a hold of us','Contact us']
    option38 = ['speak','talk','consult']
    option39 = ['cruise','vacation']
    option40 = ['professional','specialist','expert','pro']
    option41 = ['guide you in the right direction','answer any questions you have','help you find what you’re looking for','find the perfect cruise for you']

    text2 = """ {option17} our {option18} {option19} of all {option20} cruises from {departure} below and sort by {sort_by}.
    {option21} the {option22} cruise from {departure}, you can {option23} {option24} {option25} and the {option26} cruise
    {option27} as well as the {option28} details and {option29} shore {option30}. {option31} is {option32} limited, so
    {option33} online with us {option34} to {option35} your {departure} cruise reservation before {option36}. {option37}
    at {phone} or {email} to {option38} with a {option39} {option40} and we will {option41}. """

    ports = Port.objects.all()
    for port in ports:
        try:
            dinamic_text = port.dinamic_text.get(tag='port_departure')
        except DinamicText.DoesNotExist:
            dinamic_text = DinamicText()
            print 'generating ports departures' + ' ' + port.name
            dinamic_text.tag = 'port_departure'
            dinamic_text.content_object = port
            dinamic_text.first_text = text1.format(departure=port.alias,
                                                   option1=option1[random.randint(0, len(option1) - 1)],
                                                   option2=option2[random.randint(0, len(option2) - 1)],
                                                   option3=option3[random.randint(0, len(option3) - 1)],
                                                   option4=option4[random.randint(0, len(option4) - 1)],
                                                   option5=option5[random.randint(0, len(option5) - 1)],
                                                   option6=option6[random.randint(0, len(option6) - 1)],
                                                   option7=option7[random.randint(0, len(option7) - 1)],
                                                   option8=option8[random.randint(0, len(option8) - 1)],
                                                   option9=option9[random.randint(0, len(option9) - 1)],
                                                   option10=option10[random.randint(0, len(option10) - 1)],
                                                   option11=option11[random.randint(0, len(option11) - 1)],
                                                   option12=option12[random.randint(0, len(option12) - 1)],
                                                   option13=option13[random.randint(0, len(option13) - 1)],
                                                   option14=option14[random.randint(0, len(option14) - 1)],
                                                   option15=option15[random.randint(0, len(option15) - 1)],
                                                   option16=option16[random.randint(0, len(option16) - 1)])
            dinamic_text.second_text = text2.format(departure=port.alias,
                                                    option17=option17[random.randint(0, len(option17) - 1)],
                                                    option18=option18[random.randint(0, len(option18) - 1)],
                                                    option19=option19[random.randint(0, len(option19) - 1)],
                                                    option20=option20[random.randint(0, len(option20) - 1)],
                                                    option21=option21[random.randint(0, len(option21) - 1)],
                                                    option22=option22[random.randint(0, len(option22) - 1)],
                                                    option23=option23[random.randint(0, len(option23) - 1)],
                                                    option24=option24[random.randint(0, len(option24) - 1)],
                                                    option36=option36[random.randint(0, len(option36) - 1)],
                                                    option25=option25[random.randint(0, len(option25) - 1)],
                                                    option37=option37[random.randint(0, len(option37) - 1)],
                                                    option26=option26[random.randint(0, len(option26) - 1)],
                                                    option38=option38[random.randint(0, len(option38) - 1)],
                                                    option27=option27[random.randint(0, len(option27) - 1)],
                                                    option39=option39[random.randint(0, len(option39) - 1)],
                                                    option28=option28[random.randint(0, len(option28) - 1)],
                                                    option40=option40[random.randint(0, len(option40) - 1)],
                                                    option29=option29[random.randint(0, len(option29) - 1)],
                                                    option41=option41[random.randint(0, len(option41) - 1)],
                                                    option30=option30[random.randint(0, len(option30) - 1)],
                                                    option31=option31[random.randint(0, len(option31) - 1)],
                                                    option32=option32[random.randint(0, len(option32) - 1)],
                                                    option33=option33[random.randint(0, len(option33) - 1)],
                                                    option34=option34[random.randint(0, len(option34) - 1)],
                                                    option35=option35[random.randint(0, len(option35) - 1)],
                                                    sort_by=generate_random_sort(),
                                                    email=EMAIL, phone=PHONE)

            travels = port.getTravelsDeparture()
            texts = generating_travels_departure(travels, port)
            for i in range(len(texts)):
                if i == 0:
                     dinamic_text.cruise_line1 = texts[i]
                elif i == 1:
                    dinamic_text.cruise_line2 = texts[i]
                elif i == 2:
                    dinamic_text.cruise_line3 = texts[i]
                elif i == 3:
                    dinamic_text.cruise_line4 = texts[i]
                else:
                    dinamic_text.cruise_line5 = texts[i]

            dinamic_text.save()

#@cronjobs.register
def cruiseline_empty_text():
    print 'generating cruiseline_text'
    option1 = ['has become','has always been','has proven to be','has developed into','has made itself into','is known as','is definitely']
    option2 = ['best','most beloved','most trusted','most popular','premier','most well-known','top','most well-respected']
    option3 = ['companies','lines','providers','hosts','leaders']
    option4 = ['world','industry']
    option5 = ['Head out on','Embark on','Set sail on','Leave on']
    option6 = ['cruise','vacation','getaway']
    option7 = ['enjoy','expect','get','receive','find']
    option8 = ['a slew of','only the best in','tons of','many types of']
    option9 = ['luxury','extravagant','delightful','lavish','generous']
    option10 = ['terrific','helpful','pampering','catering','favoring','delightful']
    option11 = ['fantastic','extraordinary','uncommon','special','outstanding','incredible','impressive','stunning','spectacular']
    option12 = ['Red Hot Cruises','RedHotCruises','RedHotCruises.com']
    option13 = ['offers','provides','has','includes','contains']
    option14 = ['a huge assortment','one of the largest selections','many types']
    option15 = ['rock bottom','super low','unbeatable','fantastic','great']
    option16 = ['rates','prices']
    option17 = ['Here','Below','On this page']
    option18 = ['view','check out','browse','see']
    option19 = ['Vacancy','Availability','Space']
    option20 = ['very','extremely','usually','often']
    option21 = ['purchase','book','buy','reserve','order']
    option22 = ['now','today','as soon as possible','quickly','without delay']
    option23 = ['ensure','lock in','grab']
    option24 = ['it’s too late','it sells out completely','they are gone','waiting too long']
    option25 = ['Give us a call','Call us','Get a hold of us','Contact us']
    option26 = ['speak','talk','consult']
    option27 = ['cruise','vacation']
    option28 = ['professional','specialist','expert','pro']
    option29 = ['guide you in the right direction','answer any questions you have','help you find what you’re looking for','find the perfect cruise for you']

    text1 = """ {cruiseline} {option1} one of the {option2} cruise {option3} in the {option4}. {option5} a {option6} with
    {cruiseline} and {option7} {option8} {option9} on-board amenities, a {option10} service staff, and a truly {option11}
    fleet. {option12} {option13} {option14} of cruises with {cruiseline} at {option15} {option16}. {option17}, you can
    {option18} all destinations, departures and ships available with {cruiseline}. {option19} is {option20} limited, so
    {option21} online with us {option22} to {option23} your cruise with {cruiseline} before {option24}. {option25} at
    {phone} or {email} to {option26} with a {option27} {option28} and we will {option29}."""

    cruiselines = CruiseLine.objects.all()
    for cruiseline in cruiselines:
        try:
            dinamic_text = cruiseline.dinamic_text.get(tag='cruiseline_lines')
        except DinamicText.DoesNotExist:
            dinamic_text = DinamicText()
            print 'generating cruiselines' + ' ' + cruiseline.name
            dinamic_text.tag = 'cruiseline_lines'
            dinamic_text.content_object = cruiseline
            dinamic_text.first_text = text1.format(cruiseline=cruiseline.name,
                                                   option1=option1[random.randint(0, len(option1) - 1)],
                                                   option2=option2[random.randint(0, len(option2) - 1)],
                                                   option3=option3[random.randint(0, len(option3) - 1)],
                                                   option4=option4[random.randint(0, len(option4) - 1)],
                                                   option5=option5[random.randint(0, len(option5) - 1)],
                                                   option6=option6[random.randint(0, len(option6) - 1)],
                                                   option7=option7[random.randint(0, len(option7) - 1)],
                                                   option8=option8[random.randint(0, len(option8) - 1)],
                                                   option9=option9[random.randint(0, len(option9) - 1)],
                                                   option10=option10[random.randint(0, len(option10) - 1)],
                                                   option11=option11[random.randint(0, len(option11) - 1)],
                                                   option12=option12[random.randint(0, len(option12) - 1)],
                                                   option13=option13[random.randint(0, len(option13) - 1)],
                                                   option14=option14[random.randint(0, len(option14) - 1)],
                                                   option15=option15[random.randint(0, len(option15) - 1)],
                                                   option16=option16[random.randint(0, len(option16) - 1)],
                                                   option17=option17[random.randint(0, len(option17) - 1)],
                                                   option18=option18[random.randint(0, len(option18) - 1)],
                                                   option19=option19[random.randint(0, len(option19) - 1)],
                                                   option20=option20[random.randint(0, len(option20) - 1)],
                                                   option21=option21[random.randint(0, len(option21) - 1)],
                                                   option22=option22[random.randint(0, len(option22) - 1)],
                                                   option23=option23[random.randint(0, len(option23) - 1)],
                                                   option24=option24[random.randint(0, len(option24) - 1)],
                                                   option25=option25[random.randint(0, len(option25) - 1)],
                                                   option26=option26[random.randint(0, len(option26) - 1)],
                                                   option27=option27[random.randint(0, len(option27) - 1)],
                                                   option28=option28[random.randint(0, len(option28) - 1)],
                                                   option29=option29[random.randint(0, len(option29) - 1)],
                                                   email=EMAIL, phone=PHONE)

            dinamic_text.save()

#@cronjobs.register
def cruiseline_destination_empty_text():
    print 'generating cruiseline_destination_text'
    option1 = ['has become','has always been','has proven to be','has developed into','has made itself into','is known as','is definitely']
    option2 = ['top','highly sought-after','popular','must-see','featured','beloved']
    option3 = ['cruise','vacation','getaway']
    option4 = ['Featuring','Complete with','Bursting with']
    option5 = ['beautiful','spectacular','magnificent','stunning','exquisite']
    option6 = ['scenery','landscapes','views','ambiance']
    option7 = ['interesting','fascinating','captivating','inviting']
    option8 = ['charismatic','charming','delightful','lovable','enchanting']
    option9 = ['offers','provides','has','includes','contains']
    option10 = ['heading out','embarking','traveling','going']
    option11 = ['visitors','guests','newcomers','vacationers']
    option12 = ['pick from','choose from','arrange','plan','schedule']
    option13 = ['lots','a wide variety','an assortment','a wide range']
    option14 = ['trips','excursions','adventures','ventures']
    option15 = ['better','fully','further']
    option16 = ['see','witness','experience','enjoy','take part in','appreciate']
    option17 = ['Red Hot Cruises','RedHotCruises','RedHotCruises.com']
    option18 = ['offer','provide you with','have','allow you to choose from']
    option19 = ['a huge assortment','one of the largest selections','many types']
    option20 = ['on','aboard','on board']
    option21 = ['fantastic','extraordinary','uncommon','special','outstanding','incredible','impressive','stunning','spectacular']
    option22 = ['including','like','featuring']

    text1 = """{destination} {option1} a {option2} destination for a {option3}, and {cruiseline} will take you there in style.
    {option4} {option5} {option6}, {option7} cultures and {option8} people, {destination} {option9} something for everyone.
    When {option10} on a cruise with {cruiseline} to {destination}, {option11} can {option12} {option13} of activities and
    shore {option14} to {option15} {option16} what {destination} has to offer. Here at {option17}, we {option18} {option19}
    of cruises with {cruiseline} to {destination} {option20} some of their most {option21} ships {option22}: """

    option23 = ['View','Check out','Browse','See']
    option24 = ['complete','full','entire','detailed']
    option25 = ['list','lineup','schedule','listing','catalog','calendar']
    option26 = ['available','possible','upcoming','forthcoming']
    option27 = ['Once choosing','When you find','Once you see','When choosing','When selecting']
    option28 = ['perfect','ideal','best']
    option29 = ['view','check out','browse','see']
    option30 = ['more','additional','further']
    option31 = ['specifics','information']
    option32 = ['complete','full','entire','detailed']
    option33 = ['schedule','itinerary','agenda']
    option34 = ['ship','vessel','boat','yacht','sailing']
    option35 = ['a list of','the available','some featured']
    option36 = ['trips','excursions','adventures','ventures']
    option37 = ['Vacancy','Availability','Space']
    option38 = ['very','extremely','usually','often']
    option39 = ['purchase','book','buy','reserve','order']
    option40 = ['now','today','as soon as possible','quickly','without delay']
    option41 = ['ensure','lock in','grab']
    option42 = ['it’s too late','it sells out completely','they are gone','waiting too long']
    option43 = ['Give us a call','Call us','Get a hold of us','Contact us']
    option44 = ['speak','talk','consult']
    option45 = ['cruise','vacation']
    option46 = ['professional','specialist','expert','pro']
    option47 = ['guide you in the right direction','answer any questions you have','help you find what you’re looking for',''
                'find the perfect cruise for you']

    text2 = """{option23} our {option24} {option25} of all {option26} cruises with {cruiseline} to {destination} below
    and sort by {sort_by}. {option27} the {option28} cruise to {destination}, you can {option29} {option30} {option31}
    and the {option32} cruise {option33} as well as the {option34} details and {option35} shore {option36}. {option37}
    is {option38} limited, so {option39} online with us {option40} to {option41} your {destination} cruise reservation
    with {cruiseline} before {option42}. {option43} at {phone} or {email} to {option44} with a {option45} {option46} and
    we will {option47}. """

    cruiseslines = CruiseLine.objects.all()
    for cruiseline in cruiseslines:
        travels = Travel.objects.values('destination_id').filter(ship__in=cruiseline.ship_set.all()).order_by('destination__name').annotate(count=Count('destination_id'))
        for travel in travels:
            subregion = SubRegion.objects.get(pk=travel['destination_id'])
            try:
                dinamic_text = subregion.dinamic_text.get(tag='subregion_cruiseline',slug=cruiseline.slug)
            except DinamicText.DoesNotExist:
                dinamic_text = DinamicText()
                print cruiseline.name + ' ' + subregion.name
                dinamic_text.tag = 'subregion_cruiseline'
                dinamic_text.slug = cruiseline.slug
                dinamic_text.content_object = subregion
                dinamic_text.first_text = text1.format(destination=subregion.alias,cruiseline=cruiseline.name,
                                                       option1=option1[random.randint(0, len(option1) - 1)],
                                                       option2=option2[random.randint(0, len(option2) - 1)],
                                                       option3=option3[random.randint(0, len(option3) - 1)],
                                                       option4=option4[random.randint(0, len(option4) - 1)],
                                                       option5=option5[random.randint(0, len(option5) - 1)],
                                                       option6=option6[random.randint(0, len(option6) - 1)],
                                                       option7=option7[random.randint(0, len(option7) - 1)],
                                                       option8=option8[random.randint(0, len(option8) - 1)],
                                                       option9=option9[random.randint(0, len(option9) - 1)],
                                                       option10=option10[random.randint(0, len(option10) - 1)],
                                                       option11=option11[random.randint(0, len(option11) - 1)],
                                                       option12=option12[random.randint(0, len(option12) - 1)],
                                                       option13=option13[random.randint(0, len(option13) - 1)],
                                                       option14=option14[random.randint(0, len(option14) - 1)],
                                                       option15=option15[random.randint(0, len(option15) - 1)],
                                                       option16=option16[random.randint(0, len(option16) - 1)],
                                                       option17=option17[random.randint(0, len(option17) - 1)],
                                                       option18=option18[random.randint(0, len(option18) - 1)],
                                                       option19=option19[random.randint(0, len(option19) - 1)],
                                                       option20=option20[random.randint(0, len(option20) - 1)],
                                                       option21=option21[random.randint(0, len(option21) - 1)],
                                                       option22=option22[random.randint(0, len(option22) - 1)],)

                dinamic_text.second_text = text2.format(destination=subregion.alias,cruiseline=cruiseline.name,
                                                        option23=option23[random.randint(0, len(option23) - 1)],
                                                        option24=option24[random.randint(0, len(option24) - 1)],
                                                        option36=option36[random.randint(0, len(option36) - 1)],
                                                        option25=option25[random.randint(0, len(option25) - 1)],
                                                        option37=option37[random.randint(0, len(option37) - 1)],
                                                        option26=option26[random.randint(0, len(option26) - 1)],
                                                        option38=option38[random.randint(0, len(option38) - 1)],
                                                        option27=option27[random.randint(0, len(option27) - 1)],
                                                        option39=option39[random.randint(0, len(option39) - 1)],
                                                        option28=option28[random.randint(0, len(option28) - 1)],
                                                        option40=option40[random.randint(0, len(option40) - 1)],
                                                        option29=option29[random.randint(0, len(option29) - 1)],
                                                        option41=option41[random.randint(0, len(option41) - 1)],
                                                        option30=option30[random.randint(0, len(option30) - 1)],
                                                        option42=option42[random.randint(0, len(option42) - 1)],
                                                        option31=option31[random.randint(0, len(option31) - 1)],
                                                        option43=option43[random.randint(0, len(option43) - 1)],
                                                        option32=option32[random.randint(0, len(option32) - 1)],
                                                        option44=option44[random.randint(0, len(option44) - 1)],
                                                        option33=option33[random.randint(0, len(option33) - 1)],
                                                        option45=option45[random.randint(0, len(option45) - 1)],
                                                        option34=option34[random.randint(0, len(option34) - 1)],
                                                        option46=option46[random.randint(0, len(option46) - 1)],
                                                        option35=option35[random.randint(0, len(option35) - 1)],
                                                        option47=option47[random.randint(0, len(option47) - 1)],
                                                        sort_by=generate_random_sort(),
                                                        email=EMAIL, phone=PHONE)

                travels = cruiseline.getTravelsByDestination(subregion.id)
                texts = generating_travels_ship(travels)
                for i in range(len(texts)):
                    if i == 0:
                         dinamic_text.cruise_line1 = texts[i]
                    elif i == 1:
                        dinamic_text.cruise_line2 = texts[i]
                    elif i == 2:
                        dinamic_text.cruise_line3 = texts[i]
                    elif i == 3:
                        dinamic_text.cruise_line4 = texts[i]
                    else:
                        dinamic_text.cruise_line5 = texts[i]

                dinamic_text.save()

#@cronjobs.register
def cruiseline_departure_empty_text():
    option1 = ['Searching for','Looking for','Seeking','Need a list of']
    option2 = ['Red Hot Cruises','RedHotCruises','RedHotCruises.com']
    option3 = ['offers','provides','has','includes','contains']
    option4 = ['a huge assortment','one of the largest selections','many types']
    option5 = ['rock bottom','super low','unbeatable','fantastic','great']
    option6 = ['rates','prices']
    option7 = ['arriving','coming','traveling','vacationing']
    option8 = ['out of town','somewhere else','another location','a different place','another place','a different location']
    option9 = ['close','local','in the area','in town']
    option10 = ['a perfect','a great','an ideal','an excellent','a popular']
    option11 = ['location','place','spot','choice']
    option12 = ['begin','head out on','start','embark on','set sail on','leave on']
    option13 = ['on','aboard','on board']
    option14 = ['fantastic','extraordinary','uncommon','special','outstanding','incredible','impressive','stunning','spectacular']
    option15 = ['including','like','featuring']

    text1 = """{option1} cruises with {cruiseline} departing from {departure}? {option2} {option3} {option4} of cruises
    with {cruiseline} leaving from {departure} at {option5} {option6}. Whether you are {option7} from {option8} or already
    {option9}, {departure} is {option10} {option11} to {option12} a cruise with {cruiseline} {option13} some of their most
    {option14} ships {option15}:"""

    option16 = ['View','Check out','Browse','See']
    option17 = ['complete','full','entire','detailed']
    option18 = ['list','lineup','schedule','listing','catalog','calendar']
    option19 = ['available','possible','upcoming','forthcoming']
    option20 = ['Once choosing','When you find','Once you see','When choosing','When selecting']
    option21 = ['perfect','ideal','best']
    option22 = ['view','check out','browse','see']
    option23 = ['more','additional','further']
    option24 = ['specifics','information']
    option25 = ['complete','full','entire','detailed']
    option26 = ['schedule','itinerary','agenda']
    option27 = ['ship','vessel','boat','yacht','sailing']
    option28 = ['a list of','the available','some featured']
    option29 = ['trips','excursions','adventures','ventures']
    option30 = ['Vacancy','Availability','Space']
    option31 = ['very','extremely','usually','often']
    option32 = ['purchase','book','buy','reserve','order']
    option33 = ['now','today','as soon as possible','quickly','without delay']
    option34 = ['ensure','lock in','grab']
    option35 = ['it’s too late','it sells out completely','they are gone','waiting too long']
    option36 = ['Give us a call','Call us','Get a hold of us','Contact us']
    option37 = ['speak','talk','consult']
    option38 = ['cruise','vacation']
    option39 = ['professional','specialist','expert','pro']
    option40 = ['guide you in the right direction','answer any questions you have','help you find what you’re looking for','find the perfect cruise for you']

    text2 = """{option16} our {option17} {option18} of all {option19} cruises with {cruiseline} from {departure} below
    and sort by {sort_by}. {option20} the {option21} cruise from {departure}, you can {option22} {option23} {option24}
    and the {option25} cruise {option26} as well as the {option27} details and {option28} shore {option29}. {option30}
    is {option31} limited, so {option32} online with us {option33} to {option34} your {departure} cruise reservation
    with {cruiseline} before {option35}. {option36} at {phone} or {email} to {option37} with a {option38} {option39} and
    we will {option40}."""

    cruiseslines = CruiseLine.objects.all()
    for cruiseline in cruiseslines:
        travels = TravelDates.objects.values('departure_port').filter(travel__ship_id__in = cruiseline.ship_set.all()).order_by('departure_port').annotate(count = Count('departure_port'))
        for travel in travels:
            try:
                port = Port.objects.get_by_name(travel['departure_port'])
                try:
                    dinamic_text = port.dinamic_text.get(tag='port_cruiseline',slug=cruiseline.slug)
                except DinamicText.DoesNotExist:
                    dinamic_text = DinamicText()
                    print cruiseline.name + ' ' + port.name
                    dinamic_text.tag = 'port_cruiseline'
                    dinamic_text.slug = cruiseline.slug
                    dinamic_text.content_object = port
                    dinamic_text.first_text = text1.format(departure=port.alias,cruiseline=cruiseline.name,
                                                           option1=option1[random.randint(0, len(option1) - 1)],
                                                           option2=option2[random.randint(0, len(option2) - 1)],
                                                           option3=option3[random.randint(0, len(option3) - 1)],
                                                           option4=option4[random.randint(0, len(option4) - 1)],
                                                           option5=option5[random.randint(0, len(option5) - 1)],
                                                           option6=option6[random.randint(0, len(option6) - 1)],
                                                           option7=option7[random.randint(0, len(option7) - 1)],
                                                           option8=option8[random.randint(0, len(option8) - 1)],
                                                           option9=option9[random.randint(0, len(option9) - 1)],
                                                           option10=option10[random.randint(0, len(option10) - 1)],
                                                           option11=option11[random.randint(0, len(option11) - 1)],
                                                           option12=option12[random.randint(0, len(option12) - 1)],
                                                           option13=option13[random.randint(0, len(option13) - 1)],
                                                           option14=option14[random.randint(0, len(option14) - 1)],
                                                           option15=option15[random.randint(0, len(option15) - 1)])

                    dinamic_text.second_text = text2.format(departure=port.alias,cruiseline=cruiseline.name,
                                                            option16=option16[random.randint(0, len(option16) - 1)],
                                                            option17=option17[random.randint(0, len(option17) - 1)],
                                                            option18=option18[random.randint(0, len(option18) - 1)],
                                                            option19=option19[random.randint(0, len(option19) - 1)],
                                                            option20=option20[random.randint(0, len(option20) - 1)],
                                                            option21=option21[random.randint(0, len(option21) - 1)],
                                                            option22=option22[random.randint(0, len(option22) - 1)],
                                                            option23=option23[random.randint(0, len(option23) - 1)],
                                                            option24=option24[random.randint(0, len(option24) - 1)],
                                                            option36=option36[random.randint(0, len(option36) - 1)],
                                                            option25=option25[random.randint(0, len(option25) - 1)],
                                                            option37=option37[random.randint(0, len(option37) - 1)],
                                                            option26=option26[random.randint(0, len(option26) - 1)],
                                                            option38=option38[random.randint(0, len(option38) - 1)],
                                                            option27=option27[random.randint(0, len(option27) - 1)],
                                                            option39=option39[random.randint(0, len(option39) - 1)],
                                                            option28=option28[random.randint(0, len(option28) - 1)],
                                                            option40=option40[random.randint(0, len(option40) - 1)],
                                                            option29=option29[random.randint(0, len(option29) - 1)],
                                                            option30=option30[random.randint(0, len(option30) - 1)],
                                                            option31=option31[random.randint(0, len(option31) - 1)],
                                                            option32=option32[random.randint(0, len(option32) - 1)],
                                                            option33=option33[random.randint(0, len(option33) - 1)],
                                                            option34=option34[random.randint(0, len(option34) - 1)],
                                                            option35=option35[random.randint(0, len(option35) - 1)],
                                                            sort_by=generate_random_sort(),
                                                            email=EMAIL, phone=PHONE)


                    travels = cruiseline.getTravelsByDeparture(port.name)
                    texts = generating_travels_ship(travels)
                    for i in range(len(texts)):
                        if i == 0:
                             dinamic_text.cruise_line1 = texts[i]
                        elif i == 1:
                            dinamic_text.cruise_line2 = texts[i]
                        elif i == 2:
                            dinamic_text.cruise_line3 = texts[i]
                        elif i == 3:
                            dinamic_text.cruise_line4 = texts[i]
                        else:
                            dinamic_text.cruise_line5 = texts[i]

                    dinamic_text.save()

            except Port.DoesNotExist:
                pass

#@cronjobs.register
def cruiseline_ship_empty_text():
    option1 = ['has become','has always been','has proven to be','has developed into','has made itself into','is known as','is definitely']
    option2 = ['best','most beloved','most trusted','most popular','premier','most well-known','top','most well-respected']
    option3 = ['companies','lines','providers','hosts','leaders']
    option4 = ['world','industry']
    option5 = ['Head out on','Embark on','Set sail on','Leave on']
    option6 = ['cruise','vacation','getaway']
    option7 = ['enjoy','expect','get','receive','find']
    option8 = ['a slew of','only the best in','tons of','many types of']
    option9 = ['luxury','extravagant','delightful','lavish','generous']
    option10 = ['terrific','helpful','pampering','catering','favoring','delightful']
    option11 = ['fantastic','extraordinary','uncommon','special','outstanding','incredible','impressive','stunning','spectacular']
    option12 = ['Red Hot Cruises','RedHotCruises','RedHotCruises.com']
    option13 = ['offers','provides','has','includes','contains']
    option14 = ['a huge assortment','one of the largest selections','many types']
    option15 = ['rock bottom','super low','unbeatable','fantastic','great']
    option16 = ['rates','prices']
    option17 = ['View','Check out','Browse','See']
    option18 = ['complete','full','entire','detailed']
    option19 = ['list','lineup','schedule','listing','catalog','calendar']
    option20 = ['available','possible','upcoming','forthcoming']
    option21 = ['Once choosing','When you find','Once you see','When choosing','When selecting']
    option22 = ['perfect','ideal','best']
    option23 = ['view','check out','browse','see']
    option24 = ['more','additional','further']
    option25 = ['specifics','information']
    option26 = ['complete','full','entire','detailed']
    option27 = ['schedule','itinerary','agenda']
    option28 = ['ship','vessel','boat','yacht','sailing']
    option29 = ['a list of','the available','some featured']
    option30 = ['trips','excursions','adventures','ventures']
    option31 = ['Vacancy','Availability','Space']
    option32 = ['very','extremely','usually','often']
    option33 = ['purchase','book','buy','reserve','order']
    option34 = ['now','today','as soon as possible','quickly','without delay']
    option35 = ['ensure','lock in','grab']
    option36 = ['it’s too late','it sells out completely','they are gone','waiting too long']
    option37 = ['Give us a call','Call us','Get a hold of us','Contact us']
    option38 = ['speak','talk','consult']
    option39 = ['cruise','vacation']
    option40 = ['professional','specialist','expert','pro']
    option41 = ['guide you in the right direction','answer any questions you have','help you find what you’re looking for','find the perfect cruise for you']

    text1 = """{cruiseline} {option1} one of the {option2} cruise {option3} in the {option4}, and what better way to
    cruise aboard the {ship}. {option5} a {option6} with {cruiseline} and {option7} {option8} {option9} on-board
    amenities and a {option10} service staff aboard the truly {option11} {ship}. {option12} {option13} {option14} of
    {ship} cruises at {option15} {option16}. {option17} our {option18} {option19} of all {option20} {ship} cruises below
    and sort by {sort_by}. {option21} the {option22} cruise aboard the {ship}, you can {option23} {option24} {option25}
    and the {option26} cruise {option27} as well as the {option28} details and {option29} shore {option30}. {option31}
    is {option32} limited, so {option33} online with us {option34} to {option35} your {ship} cruise reservation before
    {option36}. {option37} at {phone} or {email} to {option38} with a {option39} {option40} and we will {option41}."""

    ships = Ship.objects.all()
    for ship in ships:
        try:
            dinamic_text = ship.dinamic_text.get(tag='ship_cruiseline')
        except DinamicText.DoesNotExist:
            dinamic_text = DinamicText()
            print ship.name
            dinamic_text.tag = 'ship_cruiseline'
            dinamic_text.content_object = ship
            dinamic_text.first_text = text1.format(cruiseline=ship.cruise_line.name, ship=ship.name,
                                                   option1=option1[random.randint(0, len(option1) - 1)],
                                                   option2=option2[random.randint(0, len(option2) - 1)],
                                                   option3=option3[random.randint(0, len(option3) - 1)],
                                                   option4=option4[random.randint(0, len(option4) - 1)],
                                                   option5=option5[random.randint(0, len(option5) - 1)],
                                                   option6=option6[random.randint(0, len(option6) - 1)],
                                                   option7=option7[random.randint(0, len(option7) - 1)],
                                                   option8=option8[random.randint(0, len(option8) - 1)],
                                                   option9=option9[random.randint(0, len(option9) - 1)],
                                                   option10=option10[random.randint(0, len(option10) - 1)],
                                                   option11=option11[random.randint(0, len(option11) - 1)],
                                                   option12=option12[random.randint(0, len(option12) - 1)],
                                                   option13=option13[random.randint(0, len(option13) - 1)],
                                                   option14=option14[random.randint(0, len(option14) - 1)],
                                                   option15=option15[random.randint(0, len(option15) - 1)],
                                                   option16=option16[random.randint(0, len(option16) - 1)],
                                                   option17=option17[random.randint(0, len(option17) - 1)],
                                                   option18=option18[random.randint(0, len(option18) - 1)],
                                                   option19=option19[random.randint(0, len(option19) - 1)],
                                                   option20=option20[random.randint(0, len(option20) - 1)],
                                                   option21=option21[random.randint(0, len(option21) - 1)],
                                                   option22=option22[random.randint(0, len(option22) - 1)],
                                                   option23=option23[random.randint(0, len(option23) - 1)],
                                                   option24=option24[random.randint(0, len(option24) - 1)],
                                                   option36=option36[random.randint(0, len(option36) - 1)],
                                                   option25=option25[random.randint(0, len(option25) - 1)],
                                                   option37=option37[random.randint(0, len(option37) - 1)],
                                                   option26=option26[random.randint(0, len(option26) - 1)],
                                                   option38=option38[random.randint(0, len(option38) - 1)],
                                                   option27=option27[random.randint(0, len(option27) - 1)],
                                                   option39=option39[random.randint(0, len(option39) - 1)],
                                                   option28=option28[random.randint(0, len(option28) - 1)],
                                                   option40=option40[random.randint(0, len(option40) - 1)],
                                                   option29=option29[random.randint(0, len(option29) - 1)],
                                                   option41=option41[random.randint(0, len(option41) - 1)],
                                                   option30=option30[random.randint(0, len(option30) - 1)],
                                                   option31=option31[random.randint(0, len(option31) - 1)],
                                                   option32=option32[random.randint(0, len(option32) - 1)],
                                                   option33=option33[random.randint(0, len(option33) - 1)],
                                                   option34=option34[random.randint(0, len(option34) - 1)],
                                                   option35=option35[random.randint(0, len(option35) - 1)],
                                                   sort_by=generate_random_sort(),
                                                   email=EMAIL, phone=PHONE)

            dinamic_text.save()

@cronjobs.register
def remove_empty_urls():
    travels_dates = TravelDates.objects.all()
    count = 0
    for travel_date in travels_dates:
        import re
        match = re.search(r'\d+\%\d+\%\d+%3B\w+', travel_date.url)
        if match is not None:
            print travel_date.url
            travel_date.delete()
            count += 1
    print 'Total ' + str(count)
