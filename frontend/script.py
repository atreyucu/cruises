# -*- coding: latin-1 -*-
import random
from backend.models import SubRegion

option1 = ['has become','has always been','has proven to be','has developed into','has made itself into','is known as','is definitely']
option2 = ['top','highly sought-after','popular','must-see','featured','beloved']
option3 = ['heading out','embarking','traveling','going']
option4 = ['cruise','vacation','getaway']
option5 = ['Featuring','Complete with','Bursting with']
option6 = ['beautiful','spectacular','magnificent','stunning','exquisite']
option7 = ['scenery','landscapes','views','ambiance']
option8 = ['interesting','fascinating','captivating','inviting']
option9 = ['charismatic','charming','delightful','lovable','enchanting']
option10 = ['offers','provides','has','includes','contains']
option11 = ['visitors','guests','newcomers','vacationers']
option12 = ['pick from','choose from','arrange','plan','schedule']
option13 = ['lots','a wide variety','an assortment','a wide range']
option14 = ['trips','excursions','adventures','ventures']
option15 = ['better','fully','further']
option16 = ['see','witness','experience','enjoy','take part in','appreciate']
option17 = ['Red Hot Cruises','RedHotCruises','RedHotCruises.com']
option18 = ['offer','provide you with','have','allow you to choose from']
option19 = ['a huge assortment','one of the largest selections','many types']
option20 = ['aboard','with','through','via']
option21 = ['world’s','industry’s']
option22 = ['best','most beloved','most trusted','most popular','premier','most well-known','top','most well-respected']
option23 = ['companies','lines','providers','hosts','leaders> <including','like','featuring']


destination_text = """
<h1>Cruises to {destination}</h1>
{destination} {option1} a {option2} destination when {option3} on a {option4}. {option5} {option6} {option7}, {option8}
cultures and {option9} people, {destination} {option10} something for everyone. With all cruises to {destination}, {option11}
can {option12} {option13} of activities and shore {option14} to {option15} {option16} what {destination} has to offer.
Here at {option17}, we {option18} {option19} of cruises to {destination} {option20} some of the {option21} {option22} 
cruise {option23}: """

subregions = SubRegion.objects.all()
for subregion in subregions:
    subregion.description = destination_text.format(destination=subregion.name,option1=option1[random.randint(0,len(option1))],
                                                    option2=option2[random.randint(0,len(option2))],
                                                    option3=option3[random.randint(0,len(option3))],option4=option4[random.randint(0,len(option4))],
                                                    option5=option5[random.randint(0,len(option5))],option6=option6[random.randint(0,len(option6))],
                                                    option7=option7[random.randint(0,len(option7))],option8=option8[random.randint(0,len(option8))],
                                                    option9=option9[random.randint(0,len(option9))],option10=option10[random.randint(0,len(option10))],
                                                    option11=option11[random.randint(0,len(option11))],option12=option12[random.randint(0,len(option12))],
                                                    option13=option13[random.randint(0,len(option13))],option14=option14[random.randint(0,len(option14))],
                                                    option15=option15[random.randint(0,len(option15))],option16=option16[random.randint(0,len(option16))],
                                                    option17=option17[random.randint(0,len(option17))],option18=option18[random.randint(0,len(option18))],
                                                    option19=option19[random.randint(0,len(option19))],option20=option20[random.randint(0,len(option20))],
                                                    option21=option21[random.randint(0,len(option21))],option22=option22[random.randint(0,len(option22))],
                                                    option23=option23[random.randint(0,len(option23))])